from pipe import Pipe
import os
import logging
import sys
import traceback
import signal
import fcntl

class Process(Pipe):

	_log = logging.getLogger(__name__)
	program = None
	args = None

	def __init__(self, reactor, program, args):
		Pipe.__init__(self, reactor)

		self.program = program
		self.args = args

		try:
			pid = os.fork()
			if not pid:
				try:
					# Prevent this child's child from raising SIGCHLD on exit to main thread
					signal.set_wakeup_fd(-1)

					pid2 = os.fork()
					if not pid2:
						try:
							sys.stdin.close()
							sys.stdout.close()
							sys.stderr.close()

							null = os.open("/dev/null", os.O_RDONLY)
							os.dup2(null, 0)
							os.close(null)
							os.dup2(self.pipe_write, 1)
							os.dup2(self.pipe_write, 2)

							os.execv(self.program, self.args)

							self._log.error("Unable to execv() %s!" % self.program)
						except:
							traceback.print_exc()
							self._log.error("Fork #2 died")

						os._exit(1)
					else:
						pid, status = os.waitpid(pid2, 0)
						status = os.WEXITSTATUS(status)

						# This write must succeed
						flags = fcntl.fcntl(self.pipe_write, fcntl.F_GETFL, 0)
						flags = flags & ~os.O_NONBLOCK
						flags = fcntl.fcntl(self.pipe_write, fcntl.F_SETFL, flags)

						os.write(self.pipe_write, "\0")

						os._exit(status)
				except:
					traceback.print_exc()
					self._log.error("Fork #1 died")
					os._exit(1)
		except OSError, ex:
			self._log.error("Unable to fork for process: %s" % str(ex))
			self.Shutdown()

	def OnRead(self, line):
		self.OnProcessRead(line)

		if '\0' in line:
			self.OnProcessEnd()
			self.Shutdown()
	
	def OnProcessRead(self, line):
		pass
	
	def OnProcessEnd(self):
		pass

