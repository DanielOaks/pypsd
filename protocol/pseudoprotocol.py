#!/usr/bin/python
# A pseudo protocol for PyPseudoserver
# doesnt work on its own, you should extend this module with your own protocol

from untwisted.istring import istring
import MySQLdb as db
#import MySQLdb as db
import time
import sys, os, re, commands as pycmd
import networkx as nx
import string
from __main__ import *
import logging
from untwisted import task

class PseudoProtocol():
	"""A PseudoServer based on the IRCD protocol
	This should be extended out for your own protocol if you write one.
	"""
	hostname = istring(config.get('server', 'name'))
	logchan = istring(config.get('control', 'channel'))
	
	# pycmd.getoutput("git describe --tags") or ? allows this to continue to load on machines where 
	# the hg executable is not in the local directory of the system search path
	version = r"PyPseudoServer-3.0+%s(%s)."
	try:
		version = version % ("%s", pycmd.getoutput("git describe --tags"))
	except:
		version = version % ("%s","0")
	masters = config.get('control', 'master').split(",")
	shutting_down = False
	flood_timer = None

	def __init__(self):
		"""Initialize the system"""
		self.modules = {}
		self.hooks = {}
		self.commands = {}
		self.first_burst = True
		self.flood = False
		self.last_count = 0
		self.ready_to_start = False
		self.start_ts = int(time.time())
	
		self.dbp = None #persistent datastore
		
		self.log = logging.getLogger(__name__)
	
		"""
		The server_t class variable contains information about the servers
		in the network.  It's a dict of dicts, to allow for some flexibility
		when developing modules and protocols.
		KEYS: TS6 networks are keyed by SID, otherwise key by 'host'
		Standard subkeys are as follows:
		'host' = server hostname (irc.mycool.net)
		'desc' = server description (Cold as ICE, baby)
		'uplink' = server that connects to this server (hub.mycool.net)
		'hops' = how many hops to that server (2)
		"""
		self.server_t = {} # server table
		self.server_tree = None #server link tree (used with graphing and networkx)
	
		"""
		The user_t class variable contains information about users
		on the network and "my" private users from modules and control users, respectively.
		KEYS: TS6 networks are keyed by UID, otherwise key by 'Nick'
		Standard subkeys are as follows:
		'nick' = user nickname (g0d)
		'user' = user field of the mask (~mirc32)
		'host' = host field of the mask (MyCoolNET-D3FDS3A.21.221.myisp.net) [rdns or spoof allowed]
		'gecos' = gecos of the user (I am awesome)
		'hops' = hops to the user (4)
		'modes' = umodes set on the user (+iwx)
		'ip' = ip address of the user (55.23.21.221)
		'rdns' = reverse dns of the ip (dialup-1.55.23.21.221.myisp.net)
		'server' = server host (must match 'host' in server_t) that the user is on
		'version' = client version response, default is ""
		ADDITIONALLY, my_user_t must contain:
		'nspass' = string/password to identify to NickServ with
		"""
		self.user_t = {} # user table
	
		"""
		The channel_t class variable contains channel related information for the network,
		including full user->channels and channel->users maps.
		KEYS: keyed by name (#channel)
		Standard subkeys are as follows:
		'modes' = modes set on the channel (+stnl)
		'limit' = if +l is set, this is the channel limit
		'key' = if +k is set, this is the channel key
		'topic' = current topic of the channel
		'members' = dict of users: uid->prefix"""
		self.channel_t = {} # channel table
		
		PseudoProtocol.shutting_down = False
		
		try:
			self.dbx = db.connect(
				host=config.get('database', 'host'),
				user=config.get('database', 'user'),
				passwd=config.get('database', 'passwd'),
				db=config.get('database', 'db'),
				unix_socket=config.get('database','sock')
			)
			self.dbx.autocommit(True) #no need to have transactions
			self.dbp = self.dbx.cursor()
		except Exception, err:
			log.exception("Error initializing database (%s), exiting..." % err)
			raise
		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS record (name VARCHAR(255), num INT(10), UNIQUE KEY (name));")
		except Exception, err:
			log.exception("Error creating record table: %s" % err)
			raise
		
		try:
			self.flood_check_usermark = config.getint('control', 'flood_usermark')
		except Exception, err:
			log.exception("Error getting config settings: %s" % err)
			raises
		try:
			self.server_tree = nx.Graph()
		except Exception, err:
			log.exception("Error instantiating server tree graph (%s), exiting..." % err)
			raise

	""" Somewhat taken from twisted """
	def sendMessage(self, command, *parameter_list, **prefix):
		if not command:
			raise ValueError, "No command"

		if ' ' in command or command[0] == ':':
			raise ValueError, "Invalid command"

		i_param_list = [ istring(command) ]
		for i in list(parameter_list):
			if isinstance(i, unicode):
				i_param_list.append(istring(i.encode('utf-8', errors='ignore')))
			else:
				i_param_list.append(istring(i))

		line = istring(" ").join(i_param_list)
		if prefix.has_key('prefix'):
			line = ":%s %s" % (prefix['prefix'], line)

		self.log.debug(" -> " + line)
		self.socket.Write(line)

	def keepAlive(self):
		"""Nudge MySQL connection to keep alive"""
		self.dbx.ping(True)
		self.log.debug("Parent DB Pinged")

	def reload(self):
		"""Re-read the configuration file and reload some values
		"""
		
		try:
			config.read("config.ini")
			self.masters = config.get('control', 'master').split(",")
			
			tmplogger = logging.getLogger('')
			loglevel = getattr(logging, config.get('logging', 'level').upper())
			tmplogger.setLevel(loglevel)
			
			self.flood_check_usermark = config.getint('control', 'flood_usermark')
			return True
		except Exception, err:
			self.log.error("Error reloading config.ini (%s)" % err)
			return False
		
	def shutdown(self):
		"""Exit the system"""
		PseudoProtocol.shutting_down = True
		
		self.unloadModules()
		self.closeDbConnection()
		#logging.shutdown() ????
		
	def unloadModules(self):
		"""Unloads all modules"""
		
		for name in self.modules.copy():
			try:
				self.log.info("Shutting down module %s" % name)
				self.unloadModule(name)
			except Exception, err:
				self.log.error("Error shutting down module %s (%s)" % (name, err))
	
	def closeDbConnection(self):
		"""Close the db connection"""
		
		try:
			self.keepalive_timer.stop()
			self.dbx.close()
			self.log.info("DB Connection closed")
		except:
			self.log.error("Error closing DB connection")
	
	def loadModule(self, name):
		"""Load the specified module into the runtime code.
		Also register any hooks and commands it has.
		Note, this function changed from accepting a list to accepting a single string.
		To duplicate old functionality, loop your call.
		"""
		
		if name in self.modules:
			self.log.error("Error loading module %s, exists in global table already" % name)
			return False
		
		self.log.info("Loading: %s" % name)
		
		try:
			mnproper = "psm_" + name
			mod = __import__("modules." + mnproper)
			mod = eval("mod." + mnproper)
			obj = getattr(mod, "PSModule_" + name)(self, config)
		except ImportError, err:
			self.log.error("Error importing: %s (%s)" % (name, err))
			return False
		except Exception, err:
			self.log.error("Error instantiating(a) module %s (%s)" % (name, err))
			return False
			
		self.modules[name] = obj
		
		try:
			for hook, cb in obj.getHooks():
				self.registerHook(hook, cb)
			for cmd, args in obj.getCommands():
				self.registerCommand(name, cmd, args)
			if not self.first_burst:
				obj.startup()
			return obj.getVersion() 
		except Exception, err:
			import traceback
			traceback.print_exc()
			self.log.error("Error instantiating(b) %s (%s)" % (name, err))
			self.unloadModule(name)
			return False
	
	def unloadModule(self, name):
		"""Unload the module specified from runtime code
		and the global module list.
		TODO make this throw exceptions instead of return False
		"""
		
		if name not in self.modules:
			self.log.error("Module %s failed to unload: not in global table" % name)
			return False

		obj = self.modules[name]

		try:
			for hook,cb in obj.getHooks():
				try:
					self.log.debug("Unregistering hook %s for %s" % (hook,cb))
					self.hooks[hook].remove(cb)
				except Exception, err:
					self.log.error("Error unloading hook %s for module %s (%s)" % (hook,name,err))
		except AttributeError, err:
			self.log.error("Error unloading hooks: %s" % err)

		try:
			for cmd,args in obj.getCommands():
				cmd = name + "." + cmd
				try:
					self.log.debug("Unregistering command %s" % (cmd))
					del self.commands[cmd]
				except KeyError, err:
					self.log.error("Error unloading command %s for module %s (%s)" % (cmd,name,err))
		except AttributeError, err:
			self.log.error("Error unloading commands: %s" % err)

		try:
			obj.shutdown()
		except Exception, err:
			self.log.error("Error shutting down module %s (%s)" % (name,err))
			return False
	
		#sometimes the module is imported, but we need this here to unload
		# if something bad happens
		try:
			sys.modules.pop("modules.psm_%s" % name)
		except Exception, err:
			self.log.error("Error popping module %s off the import stack" % name)
		
		del self.modules[name]
		return True
			
	def startModules(self):
		"""Execute the startup() method of all loaded modules.
		Modules should check that they've been started already
		prior to complying.
		"""
		
		for name,obj in self.modules.copy().iteritems():
			try:
				if not obj.startup():
					self.log.error("Module %s was already started" % name)
			except Exception, err:
				self.log.error("Error starting module %s (%s), unloading..." % (name,err))
				self.unloadModule(name)

	def registerHook(self, hook_name, callback):
		"""Register a hook from a module
		"""
		
		self.log.debug("Registered hook %s for %s" % (hook_name, callback))
		if not self.hooks.has_key(hook_name):
			self.hooks.update({hook_name:[]})
			
		self.hooks[hook_name].append(callback)
	
	def registerCommand(self, host_module, command_name, command_dict):
		"""Register a PRIVMSG command
		For main and modules
		
		As pypsd as grown, command addition has grown; namespaces are pretty much needed now
		"""
		
		command_name = host_module + '.' + command_name
		
		self.log.debug("Registered command %s" % (command_name))
		if self.commands.has_key(command_name):
			self.log.warning("Command '%s' has already been registered, overwriting." % command_name)
		self.commands.update({command_name:command_dict})
	
	def callHooks(self, hook_name, prefix, params):
		"""Called in the main thread to execute module hooks
		"""
		
		if not hook_name in self.hooks or not self.ready_to_start: return
		
		for hooks in self.hooks[hook_name]:
			self.log.debug("Trying to call hook %s with %s %s" % (hooks, prefix, params))
			try:
				hooks(prefix, params)
			except Exception, err:
				import traceback
				traceback.print_exc()
				self.log.error("Error calling hook: %s|%s|%s|%s" % (hooks, prefix, params, err))

	def connectionMade(self):
		"""Called when a connection is established to the uplink"""
		
		self.log.info('Connected to uplink')
		
		if not self.flood_timer:
			self.flood_timer = task.LoopingCall(self.check_flood)
			self.flood_timer.start(1, True)
	
	def connectionLost(self, reason):
		"""Called when a connection is lost to the uplink"""

		if self.flood_timer:
			self.flood_timer.stop()
			self.flood_timer = None
		
#### override parent method
	def sendLine(self, line):
		"""Overrides standard line sender
		Modification allows optional logging of outgoing data
		Also converts to utf-8 to avoid line errors.
		"""
		
		text = line.encode('utf-8')
		self.log.debug('>>> %s' % text)
		IRC.sendLine(self, text)
		
	def handleCommand(self, command, prefix, params):
		"""Overrides standard command handler (twisted.irc)
		Modification allows optional logging of input data
		"""
		
		try:
			command = self.c2u(command)
			prefix = self.c2u(prefix)
			params = map(lambda x: self.c2u(x), params)
		except:
			self.log.error("ERROR: handleCommand() unicode decoding")
			raise
		
		self.log.debug('<<< [%s](%s): %s' % (command, prefix, '|'.join(params)))
		IRC.handleCommand(self, command, prefix, params)
	
	def irc_unknown(self, prefix, command, params):
		"""Overrides handler for methods that don't exist
		Default raises exceptions, this does not.
		"""
		
		#away falls here	
		# call a handler, maybe a module implements it
		self.callHooks(command, prefix, params)
		
		pass
### Begin IRC hooks
	def irc_PING(self, prefix, params):
		"""Handle pings, signal end of burst by receiving a ping"""
		
		self.sendMessage("PONG", self.hostname, params[0], prefix=self.hostname)

	def irc_VERSION(self, prefix, params):
		"""Respond to VERSION requests sanely"""
		
		self.sendMessage("351", "%s %s %s :feat=%s" %
			(prefix, self.version, self.hostname, "+".join(self.modules)))
### End IRC hooks
	def check_flood(self):
		"""Compare user count changes and signal if flood is detected"""
		
		# don't check during a burst, duh.
		for k,v in self.server_t.iteritems():
			if v['burst']:
				return # XXX
		
		current_count = len(self.user_t)
		
		if self.last_count:
			if (current_count-self.last_count) >= self.flood_check_usermark:
				self.flood = True
				self.log.warning("FLOOD DETECTED: %d users in the last %d second(s)!" %
					(current_count-self.last_count, 1))
			else:
				self.flood = False
				
#		self.log.debug("Floodlog: Flood?: %s, Last: %d, Current: %d" % (
#			self.flood, self.last_count, current_count))

		self.last_count = current_count
