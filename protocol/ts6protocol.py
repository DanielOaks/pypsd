#!/usr/bin/python
# TS6 Pseudoserver plugin for PyPseudoserver
#todo:
#re-design server/user hashes
#export command prefix to config file

from untwisted import task
from untwisted.istring import istring
from pseudoprotocol import PseudoProtocol
import time, datetime, re, os
import networkx as nx
from __main__ import *

import logging

class TS6Protocol(PseudoProtocol):
	"""A PseudoServer based on the TS6 IRCD protocol
	
	Extensions to PseudoProtocol:
	[my_]user_t has the following extra fields:
	KEY = UID
	'ts' = timestamp of the user's signon
	'sid' = TS6 SID of the server the user is on
	'flags' = ENCAP AUTHFLAGS (authflags) [default=""]
	'su' = ENCAP SU (services user) [default=""]
	'certfp' = ENCAP CERTFP (certificate fingerprint) [default=""]
	
	server_t has the following extra fields:
	KEY = SID
	'uplink_sid' = TS6 SID of the uplink of the server
	"""
	
	version = PseudoProtocol.version % ("TS6",)

	keepalive_timer = None

	def __init__(self):
		"""Initialize various components"""
		PseudoProtocol.__init__(self)

		self.sid = istring(config.get('server', 'sid'))
		self.uplink_sid = ""
		self.uid = ""
		self.my_client_count = 0 # lifetime connections
		self.use_uid = True
		self.flood_noticed = False
		self.silence = False

		self.prefix = istring(config.get('control', 'prefix'))
		
		self.server_tree.add_node(self.sid)

		self.log = logging.getLogger(__name__)
		
		self.keepalive_counter = 0
		
### Begin helper methods	
	def base36encode(self, input):
		"""Takes an integer and returns it as base36 encoded
		"""
		
		CLIST="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		rv = ""
		while input != 0:
			rv = CLIST[input % 36] + rv
			input /= 36
		rv = "%6s" % rv
		return rv.replace(" ", "A")
	
	def get_user(self, ident):
		"""Designed to superceed common use of the old name_to_*id/*id_to_name
		functions.  This gets a user dict regardless of it being
		a uid or name supplied.

		Optimized for speed, as it is commonly used
		Assumes most calls are with UID
		"""
		
		ident = istring(ident)
		try:
			return (ident, self.user_t[ident])
		except KeyError:
			for k,v in self.user_t.iteritems():
				if v['nick'] == ident: return (k, v)
			return (None, None)

	def get_server(self, ident):
		"""Designed to superceed common use of the old name_to_*id/*id_to_name
		functions.  This gets a server dict regardless of it being
		a sid or name supplied.

		Optimized for speed
		Assumes most calls are with SID
		"""
		
		try:
			return (ident, self.server_t[ident])
		except KeyError:		
			for k,v in self.server_t.iteritems():
				if v['host'] == ident: return (k, v)
			return (None, None)

		
	def get_channel(self, ident, skip_loop=False):
		"""Returns a pointer to a channel descriptor given its name
		"""
	
		ident = istring(ident)
		try:
			return (ident, self.channel_t[ident])
		except KeyError:
			return (None, None)
		
	def join_channel(self, u, c, **kwargs):
		"""
		Join a user to a channel, create channel if not exists
		
		Documentation note: callHooks at bottom does follow convention of prefix, params
			but is structured to always return a UID vs potentially a username when called
			via other internal methods.
		TODO: research why callHooks should be here vs irc_JOIN -- catching pseudoserver users,
			but this does not follow convention.
		"""
		
		# This dict keeps track of the Channel User Status that users are to get.
		# Since we can only determine the channel name and loop over users a bit
		# later, we'll have to keep track of things until we can work them down.
		# uid => prefixes [e.g., "5RVAAAAAA" => "~@"]
		cus = {}
		use_mass = True # More than one user in SJOIN
		prefixre = re.compile(u'[~&@%+]{1,5}')

		if not isinstance(u, list):
			# Find first number, since SIDs must begin with numbers; everything
			# before that must be a prefix.
			# Hell knows if using a regex is faster than looping over 0..9 and
			# using u.find().
			u = istring(u)
			match = prefixre.match(u)
			if match:
				cus[u] = u[match.span()[0]:match.span()[1]]
				u = u[match.span()[1]:]
			else:
				cus[u] = ""
			self.log.debug("Set CUS for %s in %s to %s" % (u, c, cus[u]))
			uid, user = self.get_user(u)
			if not user: return
			use_mass = False
		else:
			# userid is a throwaway var so we can iterate over u
			for i, userid in enumerate(u):
				match = prefixre.match(userid)
				if match:
					status = u[i][match.span()[0]:match.span()[1]]
					uid = istring(u[i][match.span()[1]:])
					cus[uid] = status
					u[i] = uid
					self.log.debug("Set CUS for %s in %s to %s" % (uid, c, cus[uid]))
				else:
					cus[istring(u[i])] = ""

		cname, chan = self.get_channel(c)

		ts = int(kwargs.get('ts', self.mkts()))
		modes = kwargs.get('modes', "")
		limit = kwargs.get('limit', 0)
		key = kwargs.get('key', "")

		if not chan:
			members = {}
			if use_mass:
				for userid in u:
					members[istring(userid)] = cus[istring(userid)]
			else:
				members = {istring(uid): cus[istring(uid)]}

			self.channel_t.update({istring(c):{'modes':modes, 'limit':limit,
				'key':key, 'topic':"", 'members':members, 'ts':ts}})
		else:
			if chan['ts'] < ts:
				modes = ""
				limit = 0
				key = ""
			elif chan['ts'] > ts:
				self.log.debug("Resetting TS for %s to %s" % (c, ts))

				chan.update({
					'modes': modes,
					'limit': limit,
					'key': key,
					'ts' : ts
				})

				for tempuser in chan['members']:
					chan['members'][tempuser] = ""
			else:
				self.changeCmodes(c, modes)
				if 'k' in modes:
					chan['key'] = key
				if 'l' in modes:
					chan['limit'] = limit

			if use_mass:
				for userid in u:
					chan['members'][istring(userid)] = cus[istring(userid)]
			else:
				chan['members'][istring(uid)] = cus[istring(uid)]

			c = cname

		if use_mass:
			#speed hack
			user_t = self.user_t
			for uid in u:
				user_t[istring(uid)]['channels'].add(istring(c))
				self.callHooks("join", uid, c)
		else:
			user['channels'].add(istring(c))
			self.callHooks("join", u, c)
	
	def part_channel(self, u, c):
		"""Part a user from a channel, remove channel if last user"""
		
		uid, user = self.get_user(u)
		if not user: return
		cname, chan = self.get_channel(c)
		if not chan: return
		
		try:
			user['channels'].remove(cname)
		except Exception, err:
			self.log.error("Error, user wasn't on parted channel: %s" % err)
		try:
			del chan['members'][uid]
		except Exception, err:
			self.log.error("Error, channel list desynch detected (%s/%s): %s" % (uid,cname,err,))
		
		if len(chan['members']) == 0 and 'z' not in chan['modes']:
			try:
				del self.channel_t[istring(cname)]
			except Exception, err:
				self.log.error("Error, channel disappeared during delete: %s" % err)
	
	def quit_user(self, ident, params=[''], call_hook=False):
		"""Quits a user
		Remove all channels
		Remove from user_t
		p0 = quit message
		"""
		
		uid, user = self.get_user(ident)
		if not user: return
		
		for c in user['channels'].copy():
			self.part_channel(uid,c)
			params.append(c)
		
		if call_hook:
			self.callHooks('quit', uid, params) #params[0] = quit msg, params[1,2..] = channels the user was in
		
		del self.user_t[istring(uid)]
			
	def is_id(self, id):
		"""Check if the supplied argument is a valid SID or UID"""
		
		if re.match("^[0-9][0-9A-Z]{2,8}$", id):
			return True
		else: return False
			
	def is_channel(self, name):
		"""Check if the supplied argument is a valid channel name"""
		
		if name.startswith("#"): return True
		else: return False
		
	def is_master(self, prefix):
		"""Check if the given user is in the master's list"""
		
		uid, user = self.get_user(prefix)
		if not user: return False

		if user['nick'] in self.masters and (
			config.getint('control', 'debug') or self.has_umode(uid, "r")):
			return True
		else: return False

			
	def has_umode(self, user, modes):
		"""Checks if a given user has usermode(s) set"""
		
		uid, user = self.get_user(user)
		if not user: return False

		for mode in modes:
			try:
				#TODO: would `if mode in user['modes']` work?
				if user['modes'].find(mode) == -1:
					return False
			except KeyError:
				return False
		return True
	
	def verify_acl(self, source, target, acl_dict):
		"""Check if a user has permission to execute a command per specified ACL
		"""
		
		if self.is_master(source):
			return True

		uid, user = self.get_user(source)

		perm = acl_dict["permission"]
		if perm == None:
			return True
		elif not perm:
			return self.has_umode(uid, 'o')

		if user['acl'] == "" and self.has_umode(uid, 'o') and user['su'] != "":
			# load the ACL
			self.dbp.execute("SELECT flags FROM acl WHERE nick = %s", user['su'])
			try:
				user['acl'] = self.dbp.fetchone()[0]
				self.log.debug("ACL for %s/%s set to %s" % (user['nick'], user['su'], user['acl']))
			except:
				pass
		if user and user['acl'].find(perm) != -1:
			return True

		return False
	
	def mkts(self):
		"""Return a unix timestamp as of now"""

		return int(time.time())

	def createFakeUser(self, nick, user, host, gecos, modes, nspass,
		flags = u"$=^_><|", version = u"PyPsd User", ip=u"0", away=True, *args, **kwargs):
		"""Introduce a new user into the network sourced from this server
		"""
		
		# convert ALL INPUT to unicode
		nick = istring(nick)
		user = istring(user)
		host = istring(host)
		gecos = istring(gecos)
		modes = istring(modes)
		nspass = istring(nspass)
		flags = istring(flags)
		version = istring(version)
		ip = istring(ip)
		
		# if a user with that nickname exists, return the array for it.
		# this has the unintended feature of allowing multiple modules to share
		#  the same user if they try to add it.  failure happens however if one
		#  module unloads (and thus kills off the user), so this should not be
		#  attempted.
		xuid, xuser = self.get_user(nick)
		if xuser:
			self.introduceUser(xuser)
			return (xuid, xuser)

		uid = istring("%s%s" % (self.sid, self.base36encode(self.my_client_count)))
		self.my_client_count += 1
		join_chans = set(kwargs.get('join_chans', set()))
		join_chans.add(self.logchan)
		
		nu = {
			'nick':nick, 'user':user, 'host':host,
			'gecos':gecos, 'modes':modes, 'nspass':nspass,
			'version':version, 'ip':ip, 'rdns':host, 'server':self.hostname,
			'sid':self.sid, 'ts':u"%d" % self.mkts(),
			'flags':flags, 'su':None, 'certfp':None, 'channels':set(),
			'channels':join_chans, 'uid':uid, 'introduced':False, 'acl':''
		}
		if away:
			nu['away'] = 'I am a bot. email %s with questions' % config.get('control', 'master_email')
		
		self.user_t.update({istring(uid):nu})

		self.introduceUser(nu)

		return (uid, nu)
		
	def introduceUser(self, user):
		if user['introduced']:
			return
		user['introduced'] = True
		self.sendMessage("UID", user['nick'], "1", user['ts'],
			user['modes'], user['user'], user['host'], user['ip'], user['uid'],
			"0", user['host'], ":%s" % user['gecos'])
		self.sendMessage("ENCAP", "*", "AUTHFLAGS", user['nick'],
			user['flags'], prefix=self.hostname)
		self.privMsg('NickServ', "IDENTIFY %s" % user['nspass'], sender=user['uid'])
		for cname in user['channels']:
			chan = self.get_channel(cname)[1]

			modes = "+nt"
			if chan and chan['modes']:
				modes = chan['modes']

			self.sendMessage("SJOIN", user['ts'], cname,
				modes, "@%s" % user['uid'], prefix=self.sid)
			self.join_channel(user['uid'], cname)
			if not chan:
				self.changeCmodes(cname, modes)

		self.privMsg(self.logchan, "%s online (%s)" % (user['nick'], self.version))
		if user['away']:
			self.sendMessage("AWAY", ":%s" % user['away'], prefix=user['uid'])
		self.log.debug("NEWUSER: %s" % user)
	
	def quitFakeUser(self, uid):
		"""Quit off a user sourced from this network
		Could have been loaded from module, etc
		"""
		
		if uid[:3] == self.sid and uid in self.user_t:	
			self.quit_user(uid)
			self.sendMessage("QUIT", ":Module Unloaded", prefix=uid)
			self.log.debug("QUITUSER: %s" % uid)
			return True
		return False

	def keepAlive(self):
		"""PING uplink"""
		PseudoProtocol.keepAlive(self)
		try:
			self.sendMessage("PING", self.sid, self.uplink_sid)
		except:
			pass # We my not be connected
		self.log.debug("Uplink Pinged")
		
		self.keepalive_counter += 1
		
		"""Handle "ping timeout"."""
		#if self.keepalive_counter > 3:
		#	self.log.warning("Uplink hasn't responded to PING lately, dropping connection and reconnecting")
		#	try:
		#		self.sendMessage("SQUIT", self.sid, ":Ping Timeout")
		#	except:
		#		pass # We my be disconnected here, too

	def killUser(self, target, sender, message):
		"""Issue a kill message for TARGET"""
		#TODO: can we do server kills?
		
		self.sendMessage("KILL", target, ": (%s)" % message, prefix=sender)
	
	def keyCUS(self, c):
		"""Returns a key for Python's list.sort function so we can automatically
		sort the prefix list. This will result in an order starting with highest
		rank."""
		import string
		return c.encode('ascii').translate(string.maketrans('~&@%+', 'abcde'))

	def changeCUS(self, nick, cname, add, prefix):
		"""Changes the channel user status of a user, think "change Channel
		User Status for Nick in channel: add/remove prefix." Prefix must be
		one of qaohv or ~&@%+ (Yes, we're implying that this is running
		plexus and not just any TS6 IRCd out there. A bunch of other code
		is probably implying the same thing elsewhere, so we'll get away
		with it). This function will only take a single prefix character
		for simplicity. It will only keep track of the channel user status,
		but not send any messages to upstream to change the user's status
		in a particular channel. Pass the nick to this function, even on TS6!
		This function will do the lookup of the UID!
		
		Inputs: nick of the user, the channel name, True/False whether to
		        add/remove the prefix, respectively, and the prefix itself
		Outputs: The new prefix string
		Side-effects: Sets channel['members'][uid] to the new prefix string"""
		uid, user = self.get_user(nick)
		if not user: return
		cname, chan = self.get_channel(cname)
		if not chan: return

		if uid not in chan['members']:
			# What the fuck. Why aren't you in the member list?! Bail and return
			self.log.debug('Nick %s (UID %s) does not exist for channel %s'
					% (nick, uid, cname))
			return

		prefix = prefix.encode('ascii') # Fuck unicode and Python2
		# When we get the mode passed on, translate to prefixes
		if prefix in 'qaohv':
			import string
			table = string.maketrans('qaohv', '~&@%+')
			prefix = prefix.translate(table)

		prefixes = list(chan['members'][uid])
		if add:
			prefixes.append(prefix)
			prefixes.sort(key=self.keyCUS)
		else:
			for i, c in enumerate(prefixes):
				if c == prefix:
					prefixes.pop(i) # ignoring return value for the heck of it
					break
			else:
				self.log.debug('Prefix %s not found to remove, ignoring change'
						% prefix)

		newprefixes = ''.join(prefixes)
		chan['members'][uid] = newprefixes
		self.log.debug('Set CUS for %s in %s to %s' % (uid, cname, newprefixes))
		return newprefixes

	def changeCmodes(self, cname, modes, *args):
		"""Changes modes of a channel in the local hash table
		target = channel name
		modes = (example, +rd-x)
		
		channel user modes can happen here.
		"""
		
		cname, chan = self.get_channel(cname)
		if not chan: return
			
		switchmode = None
		newmodes = chan['modes']
		ct = 0
		for mode in modes:
			if mode == "+" or mode == "-":
				switchmode = mode
			else:
				if switchmode == "+":
					if mode == "l":
						try:
							chan['limit'] = int(args[ct])
						except:
							chan['limit'] = 0
						ct += 1
					elif mode == "k":
						try:
							chan['key'] = args[ct]
						except:
							pass
						ct += 1
					elif mode in "beI":
						ct += 1
						continue
					elif mode in "qaohv":
						self.changeCUS(args[ct], cname, True, mode)
						ct += 1
						continue
					if newmodes.count(mode) == 0:
						newmodes += mode
				elif switchmode == "-":
					if mode == "l":
						chan['limit'] = 0
					elif mode == "k":
						chan['key'] = ""
						ct += 1
					elif mode in "beI":
						ct += 1
						continue
					elif mode in "qaohv":
						self.changeCUS(args[ct], cname, False, mode)
						ct += 1
						continue
					elif mode == "z" and len(chan['members']) == 0:
						# Channel was empty and expired in the meantime. Plexus is will delete it,
						# and so must we.
						try:
							del self.channel_t[istring(cname)]
						except Exception, err:
							self.log.error("Error, channel disappeared during delete: %s" % err)
						return
					newmodes = newmodes.replace(mode, "")
		chan['modes'] = newmodes
		
	def changeUmodes(self, user, modes):
		"""Changes modes of a user in the local hash table
		target = UID or Nick
		modes = (example, +rd-x)
		"""
		
		uid, user = self.get_user(user)
		if not user: return
			
		switchmode = None
		newmodes = user['modes']
		for mode in modes:
			if mode == "+" or mode == "-":
				switchmode = mode
			else:
				if switchmode == "+" and newmodes.count(mode) == 0:
					newmodes += mode
				elif switchmode == "-":
					newmodes = newmodes.replace(mode, "")
		user['modes'] = newmodes
		
	def privMsg(self, target, message, sender=None):
		"""Send a message to a user or channel
		target can be a tuple or a list, in which case it
		issues the message to each in the list
		"""
		
		#if silence mode is enabled, don't print automatic ban notifications
		if self.silence and "Banned" in message: return

		if not sender: sender = self.uid

		if type(target).__name__ == "str":
			target = tuple((istring(target),))
		elif type(target).__name__ == "unicode":
			target = tuple((target,))
		else: target = tuple((target,))

		j = 0
		for i in range(len(message)):
			if message[i] == '\0':
				return

			if message[i] == '\r' or message[i] == '\n' or i + 1 == len(message):
				for dest in target:
					self.sendMessage("PRIVMSG", dest,
						":%s" % message[j:i+1].strip(), prefix=sender)
				j = i

	def notice(self, target, message, sender=None):
		"""Send a notice to a user or channel"""

		if not sender: sender = self.uid
		self.sendMessage("NOTICE", target,
		":%s" % message, prefix=sender)

### End helper methods

### Begin command hooks
	def cmd_help(self, source, target, pieces):
		"""
		Prints a list of commands and syntax a user is allowed to use
		TODO: change output to be more segregated:
			`help` should list modules
			`help <module>` should list help for that module
			`help all` should list all commands
		"""
		
		if len(pieces) == 0 or len(pieces) > 1:
			self.notice(source, "Help is available for the following modules:")
			self.notice(source, "help all")
			for k,v in self.modules.iteritems():
				self.notice(source, "help %s" % k)
		elif len(pieces) == 1:
			if pieces[0] == "all":
				filter = None
			else:
				filter = pieces[0]
				
			cmds = self.commands.keys()
			cmds.sort(cmp=lambda x,y: cmp(self.commands[x]['permission'], self.commands[y]['permission']))
			for k in cmds:
				if self.verify_acl(source, target, self.commands[k]):
					perm = self.commands[k]['permission']
					if perm == None:
						perm = " "
					if (filter and filter == k.split('.')[0]) or not filter:
						self.notice(source, "[%s] %s%s %s" % (perm, self.prefix, k, self.commands[k]['usage']))

		self.notice(source, "End of Help")
		return True

	def cmd_lookup(self, source, target, pieces):
		"""Prints basic data about a user"""
		
		if not pieces: return False
		uid, mu = self.get_user(pieces[0])

		if not mu:
			self.privMsg(target, "User %s not found" % pieces[0])
			return True
		
		self.privMsg(target,
			"User (SU:%s) %s!%s@%s (%s), UID %s, Modes %s, Flags: %s, Server %s, CERTFP %s, ACL: %s" %
			(mu['su'], mu['nick'], mu['user'], mu['host'], mu['gecos'],
			uid, mu['modes'], mu['flags'], mu['server'], bool(mu['certfp']), mu['acl']))
		return True
	
	def cmd_chan_lookup(self, source, target, pieces):
		"""Prints basic data about a channel"""
		if not pieces: return False
		name, chan = self.get_channel(pieces[0])
		
		if not chan:
			self.privMsg(target, "Channel %s not found" % pieces[0])
			return True
		
		self.privMsg(target, 'Channel {name} [{modes}]{key}{limit}. Users: {users}'.format(
			name=name,
			modes=chan['modes'],
			key=', key %s' % chan['key'] if chan['key'] else '',
			limit=', limit %d' % chan['limit'] if chan['limit'] else '',
			users=len(chan['members'])))
		
		if len(pieces) > 1 and pieces[1] == 'all':
			out = []
			members = chan['members']
			for user in members:
				out.append('%s%s' % (chan['members'][user], user))
				if len(out) == 30:
					self.privMsg(target, 'Userlist: %s' % ', '.join(out))
					out = []
			if out:
				self.privMsg(target, 'Userlist: %s' % ', '.join(out))
		return True

	def cmd_modload(self, source, target, pieces):
		"""Loads a specified module"""
		
		if not pieces: return False
			
		version = self.loadModule(pieces[0])
		if version:
			self.privMsg(target,
				"Module %s v%d loaded successfully" % (pieces[0], version))
		else: self.privMsg(target,
				"Module %s already loaded or failed to load" % pieces[0])
		return True
	
	def cmd_modunload(self, source, target, pieces):
		"""Unloads a specified module"""
		
		if not pieces: return False

		if self.unloadModule(pieces[0]):
			self.privMsg(target,
				"Module %s unloaded" % pieces[0])
		else:
			self.privMsg(target,
				"Module %s not loaded or failed shutdown()" % pieces[0])
		return True

	def cmd_modreload(self, source, target, pieces):
		"""Reloads a specified module"""

		if not pieces: return False

		if not self.reload(): self.privMsg(target, "Error reading config file for reloading")

		if self.cmd_modunload(source,target,pieces):
			self.cmd_modload(source,target,pieces)
		return True
	
	def cmd_reload(self, source, target, pieces):
		"""Re-read config file"""
		
		if self.reload():
			self.privMsg(target, "Reload successful")
		else:
			self.privMsg(target, "Reload failed, check your config file")
		return True
		
	def cmd_silence(self, source, target, pieces):
		"""Disable all automatic privmsg activity in logchan
		"""
		
		self.silence = not self.silence
		
		self.privMsg(target, "Silence enabled: %s" % self.silence)
		
		return True		

	def cmd_shutdown(self, source, target, pieces):
		"""Shuts down the system"""
		
		self.privMsg(target, "Shutting down...")
		self.shutdown()
		if len(pieces) > 0:
			message = " ".join(pieces)
		else: message = "shutting down"
		self.sendMessage("SQUIT", self.sid, ":%s" % message)
		return True
	
	def cmd_status(self, source, target, pieces):
		"""Displays runtime technical information"""

		try:
			las = [str(x)[:4] for x in os.getloadavg()]
		except:
			las = ["unk", "unk", "unk"]

		self.privMsg(target, "Load: [%s %s %s]; Up %.2f hours" %
			(las[0], las[1], las[2], (time.time()-self.start_ts)/60/60))
		self.privMsg(target, "Modules loaded: %s" %
			(", ".join(self.modules)))
		return True
		
## End Command hooks

### Begin IRC hooks		
	def connectionMade(self):
		"""Start the uplink negotiation
		A 439 signifies the "go ahead" for negotiation. Plexus4, we need to send it on connect, so just do it this way
		"""
		
		self.log.info('Handshaking with uplink')
		self.sendMessage("PASS",
			config.get('uplink', 'pass'), "TS", "6", ":%s" % self.sid)
		self.sendMessage("CAPAB",
			"TBURST TB UNKLN KLN GLN QS HUB ENCAP EOB EX IE CLUSTER CHW KNOCK")
			#topic burst x2(?), unkline, kline, gline, quitstorm, is a hub, encap, end of burst message, ex?, ie?, cluster?, chw?, knock?
		self.sendMessage("SERVER",
			self.hostname, "1", ":(H) %s" % self.version)
		self.sendMessage("SVINFO",
			"6", "6", "%d" % self.mkts())

	def irc_242(self, prefix, params):
		"""Receive a STATS U response"""
		
		self.log.debug('<<< 242/UPTIME: %s: %s' % (prefix, ' '.join(params)))
		self.callHooks('242', prefix, params)
			
	def irc_BMASK(self, prefix, params):
		"""Called on burst, has data on:
		+b (bans)
		+e (ban exemptions)
		+I (invite exemptions)
		
		prefix = introducing server
		p0 = ts (of mode set)?
		p1 = chan name
		p2 = mode (e/I/b) (no +, not multiple)
		p3... = masks
		
		unimplemented currently
		"""
		
		pass
		
			
	def irc_ENCAP(self, prefix, params):
		"""Handle ENCAP messages
		We can ignore SVSJOIN/SVSPART since they trigger SJOIN/PART messages
		"""
		
		if params[1] == "SVSMODE":
			self.changeUmodes(params[2], params[4])
		elif params[1] == "AUTHFLAGS":
			uid, user = self.get_user(params[2])
			if user: user['flags'] = params[3]
		elif params[1] == "SU":
			uid, user = self.get_user(params[2])
			if not user: return
			if len(params) > 3:
				user['su'] = params[3]
			elif len(params) > 2:
				user['su'] = ""
		elif params[1] == "CHGHOST":
			uid, user = self.get_user(params[2])
			if user: user['host'] = params[3]
		elif params[1] == "CERTFP":
			uid, user = self.get_user(params[2])
			if user: user['certfp'] = params[3]
		self.callHooks('encap', prefix, params)
			
	def irc_EOB(self, prefix, params):
		"""Handle EOB (End of Burst) messages by disabling burst mode
		prefix = server EOB'd
		params = None
		"""
		sid, svr = self.get_server(prefix)
		if svr:
		    host = svr['host']
		else:
		    host = prefix

		self.ready_to_start = True
			
	def irc_JOIN(self, prefix, params):
		"""Sent when a user joins an existing channel
		prefix = client UID
		p0 = ts
		p1 = channel name
		p2 = ?? (default modes?  always "+" in all dumps)
		"""
		
		self.join_channel(prefix, params[1])

	def irc_KICK(self, prefix, params):
		"""Remove user from channel on KICK
		prefix = kicker
		p0 = channel
		p1 = kicked
		p2... reason
		"""
		
		uid, user = self.get_user(params[1])
		if not user: return

		# if kicked, rejoin only if its from the log channel
		# this prevents the ban-evader problem
		if user['sid'] == self.sid and params[0] == self.logchan:
			self.sendMessage("JOIN", "%d" % self.mkts(), params[0], "+", prefix=uid)

		else:
			self.part_channel(uid, params[0])
			self.callHooks('kick', prefix, params)
			

	def irc_KILL(self, prefix, params):
		"""Handle received KILL messages
		Check if the user killed was one of our children, if so, resurrect them
		If not ours, remove them from the hash table
		
		prefix = killer
		p0 = killee
		p1... message
		"""
		
		uid, user = self.get_user(params[0])
		if not user:
			return

		self.callHooks('kill', prefix, params)
	
		if user['sid'] == self.sid:
			self.introduceUser(user)
		else:
			self.quit_user(params[0])		
	
	def irc_KLINE(self, prefix, params):
		"""Handle KLINE messages"""
		
		self.callHooks('kline', prefix, params)
			
	def irc_MODE(self, prefix, params):
		"""Handle mode changes
		prefix = user making the change
		p0 = channel or user
		p1 = modes
		p2... = mode arguments (for channels)
		"""
		
		if prefix == params[0]:
			self.changeUmodes(params[0], params[1])
		elif self.is_channel(params[0]):
			self.changeCmodes(params[0], params[1], *params[2:])
			
	def irc_NICK(self, prefix, params):
		"""Handle nick changes
		Remove mode +r, it'll get re-set by ENCAP SVSMODE if appropriate
		"""
		
		#cheaper to do a try/except than if
		try:
			self.user_t[istring(prefix)]['nick'] = params[0]
			self.user_t[istring(prefix)]['ts'] = params[1]
			self.changeUmodes(prefix, "-r")
		except Exception, err:
			self.log.exception("Error, unable to change nickname for %s: %s" % (prefix, err))
		
		self.callHooks('nick', prefix, params)
	
	def irc_NOTICE(self, prefix, params):
		"""Handle NOTICES received by the server
		Also check if the NOTICE was a CTCP and call that hook
		"""

		if len(params[1]) == 0:
			return
		
		pieces = params[1].split(" ")
		if self.is_channel(params[0]): target = params[0]
		else: target = prefix
		if params[1].find("doing a whois") != -1:
			try:
				self.privMsg(self.logchan,
					"WHOIS: %s %s is doing a whois on me %s" %
					(pieces[3], pieces[4], pieces[11]), sender=params[0])
			except Exception, err:
				#Probably not a real whois line
				self.log.error("Faked whois message received (%s/%s) (%s)" %
					(prefix,params,err))
				
		if params[1][0] == "\001":
			self.callHooks('ctcp', prefix, params)
		else:
			self.callHooks('notice', prefix, params)
			
	def irc_PART(self, prefix, params):
		"""Sent when a user leaves a channel.
		Check to see if the channel should be destroyed (last user, etc)
		prefix = UID
		p0 = channel name
		"""
		
		self.part_channel(prefix, params[0])
		self.callHooks('part', prefix, params[0])

	def irc_PASS(self, prefix, params):
		"""Handle PASS (password) messages from the uplink during negotiation.
		Check if the password is the one we have on file, disconnect if not.
		"""

		if params[0] != config.get('uplink', 'pass'):
			self.sendMessage("ERROR", ":Invalid downlink password")
			self.shutdown()
		self.uplink_sid = params[3]

	def irc_PING(self, prefix, params):
		"""Handle PING requests and send a proper pong."""

		if self.is_id(params[0]):
			sid, server = self.get_server(params[0])
			target = server['host']
		else: target = params[0]

		self.sendMessage("PONG", self.hostname, target, prefix=self.sid)
				
	def irc_PONG(self, prefix, params):
		"""Handle PONG replies, turn off bursting if we get one
		Typically a server only replies to PINGs after its EOB
		"""
		
		"""Reset our no-response counter."""
		self.keepalive_counter = 0
		
		sid, svr = self.get_server(prefix)
		if svr:
		    host = svr['host']
		else:
		    host = prefix

		if svr and svr['burst'] == 1:
			svr['burst']= 0
			self.privMsg(self.logchan,
				"Burst completed from %s in %d seconds." %
				(host, self.mkts() - svr['burst_time']))
			
	def irc_PRIVMSG(self, prefix, params):
		"""Handle messages seen by the server.
		The command handler hook "hooks" in here
		"""

		if len(params[1]) == 0:
			return
		
		uid, user = self.get_user(prefix)

		params[1] = istring(params[1])
		
		#throw in ctcp REQUESTS here, shouldn't be many
		if params[1][0] == "\001":
			muid, muser = self.get_user(params[0])
			if not muser: return

			if params[1][1:-1].upper() == 'VERSION':
				self.sendMessage("NOTICE", prefix, ":\001VERSION %s\001" %
					muser['version'], prefix=muid)
			return

		#handle privmsgs
		if params[1][0] != self.prefix:
			self.callHooks('privmsg', prefix, params)
			return
		
		self.log.info('COMMAND: %s: %s' % (user['nick'], '|'.join(params)))
			
		pieces = params[1].strip().split(" ")
		if self.is_channel(params[0]):
			target = params[0]
		else: target = prefix
		
		cmd = pieces[0][1:]
		# shortcuts--
		#   some modules have commands named after themselves so ~akill == ~akill.akill etc.
		#   also let core commands ride without a prefix
		if ('core.' + cmd) in self.commands:
			cmd = 'core.' + cmd
		elif '.' not in cmd:
			cmd += '.' + cmd
			
		if cmd in self.commands:
			hook = self.commands[cmd]
			if self.verify_acl(prefix, target, hook):
				try:
					try: msg = pieces[1:]
					except: msg = None
					if not hook['callback'](prefix, target, msg):
						self.notice(prefix,
							"Usage: %s%s %s" % (self.prefix, cmd, hook['usage']))
				except Exception, err:
					self.log.error('COMMAND: Error executing %s (%s)' %
						(cmd,err))
					if config.getint('control', 'debug') == 1: raise
				return
				
		#self.privMsg(self.logchan,
		#	"SECURITY: %s attempted unauthorized command: %s" %
		#	(user['nick'], '|'.join(params)))
		self.log.warning("SECURITY: %s attempted unauthorized command: %s" %
			(user['nick'], '|'.join(params)))
		
	def irc_QUIT(self, prefix, params):
		"""Handle user quits from the network
		"""
		
		self.quit_user(prefix, params, call_hook=True)
			
	def irc_SERVER(self, prefix, params):
		"""Handle SERVER messages during uplink negotiation
		SERVER messages ONLY appear during uplink negotiation AND for NON-TS6 SERVERS
		Since we don't give two craps about non-ts6, just ignore them if we don't see this
		 during negotiation/burst.
		"""
		
		if self.first_burst:
			self.first_burst = False
			self.server_t.update({self.sid:{'host':self.hostname, 'hops':u"0",
				'desc':self.version, 'uplink':u"", 'uplink_sid':u"",
				'burst':0, 'burst_time':self.mkts()}})
			su = {
				'uplink':self.hostname, 'uplink_sid':self.sid,
				'host':params[0], 'hops':params[1], 'desc':params[2],
				'burst':1, 'burst_time':self.mkts()
			}
			self.server_t.update({self.uplink_sid:su})
			self.server_tree.add_node(self.uplink_sid)
			self.server_tree.add_edge(self.sid, self.uplink_sid)
		
			if "netstats" in self.modules:
				self.sendMessage("VERSION", self.uplink_sid)
				
	def irc_SID(self, prefix, params):
		"""Handle SID (TS6 server introductions) messages
		prefix = Hub that links the server described in params
		"""
		
		self.callHooks("sid", prefix, params)

		self.sendMessage("PING", self.sid, params[2])
		
		sid, server = self.get_server(prefix)
		
		su = {
			'uplink':server['host'], 'uplink_sid':sid,
			'host':params[0], 'hops':params[1], 'desc':params[3],
			'burst':1, 'burst_time':self.mkts()
		}
		self.server_t.update({params[2]:su})
		self.server_tree.add_node(params[2])
		self.server_tree.add_edge(params[2], prefix)
		
		# netstats uses this
		if "netstats" in self.modules:
			self.sendMessage("VERSION", params[2])

	def irc_SJOIN(self, prefix, params):
		"""SJOIN's are server joins, used to relay new channel information.
		Often used during bursting and when new channels are made
		what clients are on what channels
		prefix = introducing server (SID)
		p0 = ts
		p1 = channel
		p2 = channel modes
		p[3...] = channel args (ugh)
		p[LAST] = user list, space seperated
		"""

		modes = params[2]

		offset = 0
		limit = 0
		key = ""
		for mode in modes:
			if mode == "l":
				try:
					limit = int(params[3+offset])
				except:
					limit = 0
				offset += 1
			elif mode == "k":
				key = params[3+offset]
				offset += 1
		try:	
			self.join_channel(list(params[3+offset].split()), params[1], ts=params[0], limit=limit, key=key, modes=modes)
		except Exception, err:
			self.log.exception("Error in SJOIN: prefix=%s, params=%s, err=%s" % (prefix, "|".join(params), err))
			
	def irc_SQUIT(self, prefix, params):
		"""Handle SQUIT (server quits)
		WE RECEIVE -ONE- SQUIT FOR HUB SPLITS
		 - determine who was on the other end
		 - delete off their users
		 - delete off the servers
		prefix = None
		params[0] = sid that squit
		params[1:] = squit message
		"""
		
		sid, server = self.get_server(params[0])
		if not server: return
			
		if self.sid == sid or self.uplink_sid == sid: return
			
		self.callHooks("squit", prefix, params)

		try:
			try:
				import networkx.algorithms as nxa
				prune = nxa.shortest_path(self.server_tree, self.sid, sid)[-2]
			except:
				prune = nx.traversal.path.shortest_path(self.server_tree, self.sid, sid)[-2]
			self.server_tree.remove_edge(sid, prune)
			nodes = nx.connected_components(self.server_tree)
		except Exception, err:
			self.log.exception('Error, unable to tracepath SQUIT (%s)' % err)
			self.privMsg(self.logchan, 'SQUIT CALCULATION ERROR')
			return
	
		if self.sid in nodes[0]:
			splits = nodes[1]
		else:
			splits = nodes[0]
		
		for uid, user in self.user_t.copy().iteritems():
			if user['sid'] in splits:
				self.quit_user(uid)

		for k in splits:
			del self.server_t[k]
			self.server_tree.remove_node(k)

	def irc_SVINFO(self, prefix, params):
		"""Handle SVSINFO command
		Things are pretty much cleared for burst start by this point, so start bursting
		Also load DEFAULT command handlers
		Also load modules too.
		Then send End Of Burst.
		"""

		for uid, user in self.user_t.iteritems():
			if user['sid'] == self.sid:
				user['introduced'] = False

		self.uid, user = self.createFakeUser(
			config.get('control', 'nick'),
			config.get('control', 'user'),
			config.get('control', 'host'),
			config.get('control', 'gecos'),
			config.get('control', 'modes'),
			config.get('control', 'nspass')
		)
		self.registerCommand('core', 'modload',
			{'permission':'m', 'callback':self.cmd_modload,
			'usage':"<module name> - loads a module"})
		self.registerCommand('core', 'modunload',
			{'permission':'m', 'callback':self.cmd_modunload,
			'usage':"<module name> - unloads a module"})
		self.registerCommand('core', 'modreload',
			{'permission':'m', 'callback':self.cmd_modreload,
			'usage':"<module name> - reloads a module"})
		self.registerCommand('core', 'reload',
			{'permission':'r', 'callback':self.cmd_reload,
			'usage':"- reloads certain configuration parameters"})
		self.registerCommand('core', 'shutdown',
			{'permission':'r', 'callback':self.cmd_shutdown,
			'usage':"- shuts down the server and disconnects"})
		self.registerCommand('core', 'silence',
			{'permission':'r', 'callback':self.cmd_silence,
			'usage':"- toggles visibility of ban messages to the log channel"})
		self.registerCommand('core', 'status',
			{'permission':'', 'callback':self.cmd_status,
			'usage':"- display technical information"})
		self.registerCommand('core', 'lookup',
			{'permission':'l', 'callback':self.cmd_lookup,
			'usage':"<nick> - lookup a user's uid, modes, and server"})
		self.registerCommand('core', 'clookup',
			{'permission':'l', 'callback':self.cmd_chan_lookup,
			'usage':"<channel> [all] - lookup a channel info"})
		self.registerCommand('core', 'help',
			{'permission':None, 'callback':self.cmd_help,
			'usage':"- shows help for all commands available *to you*"})

		self.startModules()

		for uid, user in self.user_t.iteritems():
			if user['sid'] == self.sid and not user['introduced']:
				self.introduceUser(user)

		# let network know we are done bursting
		self.sendMessage("EOB", prefix=self.sid)
		self.sendMessage("PING", self.sid, self.uplink_sid)

		if not self.keepalive_timer:
			self.keepalive_timer = task.LoopingCall(self.keepAlive)
			self.keepalive_timer.start(60, True)
		
	def irc_TBURST(self, prefix, params):
		"""TBURST's are topic bursts, which relay channel information
		during the burst phase.
		prefix = introducing server (NAME)
		p0 = ts (for channel creation?)
		p1 = channel name
		p2 = ts (for topic set?)
		(opt)p3 = topic setter (full nick!user@host)
		(opt)p4 = topic

		p3-4 do not exist if no topic.
		"""

		if len(params) < 3: return

		cname, chan = self.get_channel(params[1])
		if chan and params[3] != '':
			try:
				chan['topic'] = params[4]
			except IndexError:
				self.log.error("Error setting topic. params: %s" % ('|'.join(params)))
	
	def irc_TMODE(self, prefix, params):
		"""Relay channel mode changes
		p0 = ts
		p1 = channel
		p2 = modes (-/+...)
		p3... = args
		
		Something to note here is that CHANNEL USER MODES happen here too!
		so +vvo user1 user2 user3 will propogate here (if we ever code in channel umodes)
		"""
		
		self.changeCmodes(params[1], params[2], *params[3:])
		self.callHooks("tmode", prefix, params)

	def irc_TOPIC(self, prefix, params):
		"""Relays TOPIC change information
		prefix = uid of channel changer
		p0 = channel name
		p1 = topic
		"""
		
		cname, chan = self.get_channel(params[0])
		if chan:
			chan['topic'] = params[1]
		
	def irc_UID(self, prefix, params):
		"""Called when a user connects to the network or is introduced.
		prefix = server they live on
		params:
		0 = nick
		1 = hops
		2 = TS
		3 = modes
		4 = username
		5 = spoof/nethost
		6 = realip (0 if spoofed!)
		7 = UID
		8 = ???
		9 = rdns
		10 = gecos

		Of this data, hops is useless to track, so throw it away
		"""
		sid, server = self.get_server(prefix)
		if not server:
			self.log.error("Error, UID called from server %s, but it does not exist!(%s)" % (prefix, self.server_t))
			return

		nu = {
			'nick':params[0], 'user':params[4], 'host':params[5],
			'gecos':params[10], 'modes':params[3],
			'version':"", 'ip':params[6], 'rdns':params[9],
			'server':server['host'], 'sid':prefix,
			'ts':params[2], 'flags':"", 'su':"", 'certfp':"", 'channels':set(),
			'acl':""
		}
		
		self.user_t.update({istring(params[7]):nu})
		
		if self.flood and not self.flood_noticed:
			self.flood_noticed = True
			self.privMsg(self.logchan, "Flood detected, shutting down expensive systems...")
		elif not self.flood and self.flood_noticed:
			#only triggered on next user
			self.flood_noticed = False
			self.privMsg(self.logchan, "Flood gone, reactivating systems...")

		if not server['burst'] and params[6] != "0":
			#dont call hook on burst or an oper/spoofed
			self.callHooks("uid", prefix, params)

	def irc_UNKLINE(self, prefix, params):
		"""Handle UNKLINE messages"""
		
		self.callHooks('unkline', prefix, params)
	
	def irc_WHOIS(self, prefix, params):
		"""Handle WHOIS requests for server users.
		If requestor is an IRCOp, send them additional faked fields.
		"""
		
		muid, muser = self.get_user(params[0])
		ruid, ruser = self.get_user(prefix)

		if not muser:
			self.log.error("Error, local whois user does not exist! (%s)" % params[0])
			return

		mynick = muser['nick']
		self.privMsg(self.logchan,
			"WHOIS: %s is doing a whois on me [%s]" %
			(ruser['nick'], ruser['server']), sender=muid)
		self.sendMessage("311", prefix, "%s %s %s * :%s" %
			(mynick, muser['user'], muser['host'], muser['gecos']))
		#oper only
		if self.has_umode(prefix, "o"):
			self.sendMessage("319", prefix, mynick, ":%s" % " ".join(muser['channels']))
			self.sendMessage("312", prefix, mynick, self.hostname, self.version)
		else:
			self.sendMessage("312", prefix, mynick, config.get('server', 'whois_faked_server'), "hidden")
		self.sendMessage("313", prefix, mynick, ":is an IRC Operator - Network Service")
		#should make this real.
		self.sendMessage("307", prefix, mynick, ":has identified for this nick")
		#technically we are...
		self.sendMessage("671", prefix, mynick, ":is using a secure connection")
		#oper only
		if self.has_umode(prefix, "o"):
			self.sendMessage("310", prefix, mynick,
			":is using modes %s authflags: %s" %
			(muser['modes'], muser['flags']))
			self.sendMessage("338", prefix, mynick,
			":is actually %s@%s [%s]" %
			(muser['user'], muser['rdns'], "255.255.255.255"))
		self.sendMessage("318", prefix, mynick, ":End of WHOIS")
### End IRC hooks
