#!/usr/bin/python pseudoserver.py
#psm-ctcp.py
# module for pypseudoserver
# written by radicand
# doesn't do much by itself

from psmodule import *
from untwisted import task
from operator import itemgetter #for sort
from collections import defaultdict
from untwisted.istring import istring

class PSModule_ctcp(PSModule):
	VERSION = 1
	
	CTCP_BANNED = 0
	CTCP_TOTAL = 0

	WAITLIST_TIMEOUT = 60
	wait_list = {}
	blacklist = []
	task_checklist = None
	uid = ""

	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)

	def startup(self):
		if not PSModule.startup(self): return False

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ctcp_vbl (item SMALLINT(5) NOT NULL AUTO_INCREMENT, reply VARCHAR(255), found INT(10), PRIMARY KEY (item), UNIQUE KEY (reply));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ctcp_vlist (reply VARCHAR(255), found INT(10), UNIQUE KEY (reply));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ctcp_website (reply VARCHAR(255), found INT(10), UNIQUE KEY (reply));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ctcp_website_replies (keyword VARCHAR(255), reply VARCHAR(255), UNIQUE KEY (keyword));")
		except Exception, err:
			self.log.exception("Error creating tables for CTCP module (%s)" % err)
			raise
		
		try:
			self.dbp.execute("SELECT reply FROM ctcp_vbl;")
			for row in self.dbp.fetchall(): self.blacklist.append(row[0])
		except Exception, err:
			self.log.exception("Error loading blacklists from DB for CTCP module (%s)" % err)
			
		try:
			num = self.dbp.execute("SELECT num FROM record WHERE name='ctcp-total';")
			if num:
				self.CTCP_TOTAL = self.dbp.fetchone()[0]

			num = self.dbp.execute("SELECT num FROM record WHERE name='ctcp-found';")
			if num:
				self.CTCP_BANNED = self.dbp.fetchone()[0]

		except Exception, err:
			self.log.exception("Error loading in historical records for CTCP module (%s)" % err)
		
		self.reload()
		
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('ctcp', 'nick'),
				self.config.get('ctcp', 'user'),
				self.config.get('ctcp', 'host'),
				self.config.get('ctcp', 'gecos'),
				self.config.get('ctcp', 'modes'),
				self.config.get('ctcp', 'nspass'),
				version="PyPsd CTCP Version Checker v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating ctcp module user: %s" % err)
			raise

		try:
			self.noreply_check = self.config.getint('ctcp', 'check_no_reply')
		except Exception, err:
			self.log.exception("Error, setting 'ctcp:check_no_reply' does not exist or not int")
			self.noreply_check = 0

		if self.noreply_check:
			self.task_checklist = task.LoopingCall(self.timer_checkList)
			self.task_checklist.start(self.WAITLIST_TIMEOUT+1) # every 30 seconds
			
		return True
	
	def shutdown(self):
		PSModule.shutdown(self)
		
		try:
			if self.noreply_check:
				try:
					self.task_checklist.stop()
				except Exception, err:
					self.log.exception("Error shutting down CTCP noreply checker task (%s)" % err)
		except Exception, err:
			pass # self.noreply_check was not set because startup never got that far

		self.parent.quitFakeUser(self.uid)
		
		try:
			self.dbp.execute("INSERT INTO record (name, num) VALUES ('ctcp-total', %s) ON DUPLICATE KEY UPDATE num=%s;", (self.CTCP_TOTAL,self.CTCP_TOTAL))
			self.dbp.execute("INSERT INTO record (name, num) VALUES ('ctcp-found', %s) ON DUPLICATE KEY UPDATE num=%s;", (self.CTCP_BANNED,self.CTCP_BANNED))
		except Exception, err:
			self.log.exception("Error updating historical records for CTCP module (%s)" % err)

	def timer_checkList(self):
		now = self.parent.mkts()
		
		for uid,ts in self.wait_list.copy().iteritems():
			if ts+self.WAITLIST_TIMEOUT < now:
				try: user = self.parent.user_t[uid]
				except KeyError: continue #they quit off, cheaper to try than if
				self.wait_list.pop(uid)
				self.CTCP_BANNED += 1
				#readd akill code here later
				self.parent.privMsg(self.logchan,
					"CTCP: Banned %s!%s@%s - No VERSION response (#%d/%d)" %
					(user['nick'], user['user'], user['ip'], self.CTCP_BANNED, self.CTCP_TOTAL), self.uid)

## Begin event hooks
	def ctcp_UID(self, prefix, params):
		"""Sends a CTCP VERSION request to a user
		We setup a list with the timestamp we sent the request
		 and if we havent received the reply in X seconds, akill them.
		Also akill if they have a blacklisted reply.
		If user is from mibbit, request a CTCP WEBSITE, so we can track it.
		Rizon: Temporarily ignore nick `bRO-*` / eth32 / RAC until they fix their 'client'.
		"""
		if params[0].startswith(u'bRO-'): return #rizon
		elif params[0].startswith(u'RAC_'): return #rizon
		elif u'eth32' in params[0]: return #rizon

		#if mibbit req website
		if self.parent.has_umode(params[7], "W"):
			self.parent.privMsg(params[7], "\001WEBSITE\001", self.uid)

		self.parent.privMsg(params[7], "\001VERSION\001", self.uid)
		if self.noreply_check:
			self.wait_list.update({params[7]:self.parent.mkts()})
		self.CTCP_TOTAL += 1
		
	def ctcp_CTCP(self, prefix, params):
		"""Receiver for CTCP VERSION replies.
		Check against active blacklist.
		Remove from timeout list too.
		#checks ctcp website too
		TODO: clean this up
		"""
		if self.noreply_check:
			try:
				self.wait_list.pop(prefix)
			except KeyError:
				return #faked reply
		try:
			reply = istring(params[1][1:-1])
		except:
			return # Can this throw?
		try: user = self.parent.user_t[prefix]
		except KeyError: return #user no longer exists, extremely unlikely due to message order processing

		#ctcp website reply
		if reply.lower().startswith(u"website"):
			reply = reply[8:]
			try:
				self.dbp.execute(u"INSERT INTO ctcp_website (reply, found) VALUES(%s, 1) ON DUPLICATE KEY UPDATE found=found+1;", (reply,))
			except Exception, err:
				self.log.exception(u"Error updating ctcp_website: %s (%s)" % (reply, err))
			user['website'] = reply
			return

		#ctcp version reply
		if not reply.lower().startswith(u"version"): return
		reply = unicode(reply[8:])
		self.log.debug(u"Got CTCP VERSION: %s" % reply)

		try:
			self.dbp.execute(u"INSERT INTO ctcp_vlist (reply, found) VALUES('%s', 1) ON DUPLICATE KEY UPDATE found=found+1;", (reply,))
		except Exception, err:
			self.log.exception(u"Error updating ctcp_vlist: %s (%s)" % (reply, err))

		user['version'] = reply
		self.handleWebsiteReply(user)

		if reply in self.blacklist:
			self.CTCP_BANNED += 1
			if self.parent.get_user("GeoServ") == (None,None): serv = "OperServ"
			else: serv = "GeoServ"
			self.parent.privMsg(serv,
				"AKILL ADD +3d *@%s Compromised Host - Blacklisted VERSION reply." %
				(user['ip'],), self.uid)
			self.parent.privMsg(self.logchan,
				"CTCP: Banned %s!%s@%s - Blacklisted VERSION: %s (#%d/%d)" %
				(user['nick'], user['user'], user['ip'], reply, self.CTCP_BANNED, self.CTCP_TOTAL), self.uid)
			try:
				self.dbp.execute(u"UPDATE ctcp_vbl SET found=found+1 WHERE reply=%s;", (reply,))
			except Exception, err:
				self.log.exception("Error updating ctcp_vbl found count: %s (%s)" % (reply, err))

			if "dnsbl_admin" in self.parent.modules:
				uid2, user2 = self.parent.get_user(self.uid)
				self.parent.modules['dnsbl_admin'].add(user['ip'], "5", "Compromised host found by %s (%s!%s@%s)" %
					(user2['nick'],user['nick'], user['user'], user['ip']))
	
## Begin Command hooks
	def cmd_ctcpVersionAdd(self, source, target, pieces):
		if not pieces: return False
		try:
			self.dbp.execute("INSERT INTO ctcp_vbl (reply,found) VALUES(%s,0);", (u' '.join(pieces),))
			self.parent.privMsg(target, "Added VERSION blacklist #%d" % self.dbp.lastrowid, self.uid)
		except Exception, err:
			self.log.exception("Error adding version to CTCP module blacklist: (%s)" % err)
			return False

		self.blacklist.append(u' '.join(pieces))
		return True

	def cmd_ctcpVersionDel(self, source, target, pieces):
		if not pieces: return False
		try:
			num = self.dbp.execute("SELECT reply FROM ctcp_vbl WHERE item=%s", (pieces[0],))
			if not num: raise Exception("No such ID")
			else: reply = self.dbp.fetchone()[0]

			self.dbp.execute("DELETE FROM ctcp_vbl WHERE item=%s;", (pieces[0],))
			self.blacklist.remove(reply)
			self.parent.privMsg(target, "Removed blacklist for: %s" % reply, self.uid)
		except Exception, err:
			self.log.exception("Error deleting version blacklist for CTCP module: (%s)" % err)
			self.parent.privMsg(target, "No such ID", self.uid)
			
		return True
		
	def cmd_ctcpVersionBLList(self, source, target, pieces):
		try:
			self.dbp.execute("SELECT item, reply FROM ctcp_vbl;")
			for row in self.dbp.fetchall():
				self.parent.privMsg(target, "#%d: %s" % (row[0], row[1]), self.uid)
			self.log.debug("CVMEMORY: %s" % self.blacklist)
		except Exception, err:
			self.log.exception("Error selecting version blacklists for CTCP module: (%s)" % err)
		
		self.parent.privMsg(target, "END OF CVLIST", self.uid)
		return True
	def cmd_ctcpWebsiteList(self, source, target, pieces):
		try:
			w = defaultdict(lambda: 0)
			cm = 0
			cq = 0
			for k,v in self.parent.user_t.iteritems():
				if 'website' in v:
					tmp = v['website']
					w[tmp] += 1
				if v['user'] == u'cgiirc': cm += 1
				if v['user'] == u'qwebirc': cq += 1
				if v['user'].endswith(u'webchat'): cq += 1
		except Exception, err:
			self.log.exception("Error comprehending website list creation: %s" % (err,))
			self.parent.privMsg(target, "Error creating list response", self.uid)
			return True
		#sort by number of hits
		w = sorted(w.items(), key=itemgetter(1))	
		for k,v in w:
			self.parent.notice(source, "%dx: %s" % (v,k), self.uid)
		self.parent.notice(source, "END OF CWLIST(m=%d,q=%d)" % (cm,cq), self.uid)
		return True

	ctcpReplyCache = None
	
	def handleWebsiteReply(self, user):
		if self.ctcpReplyCache == None:
			self.ctcpReplyCache = []
			self.dbp.execute("SELECT * FROM ctcp_website_replies")
			for row in self.dbp.fetchall():
				self.ctcpReplyCache.append((row[0], row[1]))
		
		for reply in self.ctcpReplyCache:
			if 'version' in user and user['version'].lower().find(reply[0].lower()) != -1:
				self.parent.notice(user['nick'], reply[1], self.uid)
			elif 'website' in user and user['website'].lower().find(reply[0].lower()) != -1:
				self.parent.notice(user['nick'], reply[1], self.uid)
		return
		
	def cmd_ctcpReplyAdd(self, source, target, pieces):
		if len(pieces) < 2:
			return False
		try:
			self.dbp.execute("INSERT INTO ctcp_website_replies (keyword, reply) VALUES(%s, %s)", (pieces[0], ' '.join(pieces[1:])))
			self.parent.privMsg(target, "Added reply for keyword %s" % pieces[0], self.uid)
			self.ctcpReplyCache = None
		except Exception, err:
			self.log.exception("Error adding website reply for %s: %s" % (pieces[0], err))
			self.parent.privMsg(target, "Error creating reply keyword", self.uid)
		return True

	def cmd_ctcpReplyDel(self, source, target, pieces):
		if not pieces:
			return False
		try:
			self.dbp.execute("DELETE FROM ctcp_website_replies WHERE keyword=%s", pieces[0])
			self.parent.privMsg(target, "Removed reply for keyword %s" % pieces[0], self.uid)
			self.ctcpReplyCache = None
		except Exception, err:
			self.log.exception("Error removing website reply for %s: %s" % (pieces[0], err))
			self.parent.privMsg(target, "Error removing reply keyword", self.uid)
		return True

	def cmd_ctcpReplyList(self, source, target, pieces):
		try:
			self.dbp.execute("SELECT * FROM ctcp_website_replies")
			for row in self.dbp.fetchall():
				self.parent.privMsg(target, "%s: %s" % (row[0], row[1]), self.uid)
		except Exception, err:
			self.log.exception("Error listing website replies: %s" % err)
			self.parent.privMsg(target, "Error listing website replies.", self.uid)
		return True

	def cmd_ctcpVersionList(self, source, target, pieces):
		try:
			subcmd = pieces[0].lower()
		except IndexError:
			subcmd = "top"

		try:
			arg = pieces[1]
		except IndexError:
			arg = ""

		maxrow = 25
		count = 0
		if subcmd == "top":
			if arg != "":
				try:
					maxrow = int(arg)
				except:
					pass

			# pymysql apparently converts ints to strings, the %s here is intentional!
			self.dbp.execute("SELECT reply, found FROM ctcp_vlist ORDER BY found DESC LIMIT %s", maxrow)

			try:
				for row in self.dbp.fetchall():
					self.parent.privMsg(target, u"%s [%s times]" % (row[0], row[1]), self.uid)
					count += 1
			except Exception, err:
				self.log.exception("Error listing version replies [top]: %s" % err)
				self.parent.privMsg(target, "Error listing version replies.", self.uid)

		elif subcmd == "search":
			if arg == "":
				self.parent.privMsg(target, 'Please give a string to search for.')
				return False

			try:
				maxrow = int(pieces[2])
			except:
				pass

			self.dbp.execute(u"SELECT reply, found FROM ctcp_vlist WHERE reply LIKE '%%%s%%' LIMIT %s" % (arg, maxrow))
			try:
				for row in self.dbp.fetchall():
					self.parent.privMsg(target, u"%s [%s times]" % (row[0], row[1]), self.uid)
					count += 1
			except Exception, err:
				self.log.exception("Error listing version replies [search]: %s" % err)
				self.parent.privMsg(target, "Error listing version replies.", self.uid)
		else:
			self.parent.privMsg(target, 'Please use one of "top" or "search."')
			return False

		self.parent.privMsg(target, "END OF VLIST (%d results)" % count, self.uid)
		return True

## End Command hooks

	def getCommands(self):
		return (('addbl', {
			 'permission':'v',
			'callback':self.cmd_ctcpVersionAdd,
			'usage':"<version string> - blacklists a VERSION string"}),
			('delbl', {
			 'permission':'v',
			'callback':self.cmd_ctcpVersionDel,
			'usage':"<version string> - removes a blacklisted VERSION string"}),
			('listbl', {
			 'permission':'v',
			'callback':self.cmd_ctcpVersionBLList,
			'usage':"- shows all active blacklisted VERSION strings"}),
			('cwlist', {
			 'permission':'v',
			'callback':self.cmd_ctcpWebsiteList,
			'usage':"- shows source of all online mibbit users"}),
			('wadd', {
			 'permission' : 'v',
			 'callback' : self.cmd_ctcpReplyAdd,
			 'usage' : "<match> <reply> - sends a custom notice to specific clients"}),
			('wdel', {
			 'permission' : 'v',
			 'callback' : self.cmd_ctcpReplyDel,
			 'usage' : "<match> - removes the welcome notice for match"}),
			('wlist', {
			 'permission' : 'v',
			 'callback' : self.cmd_ctcpReplyList,
			 'usage' : "- shows the welcome notices for version matches"}),
			('vlist', {
			 'permission': 'v',
			 'callback': self.cmd_ctcpVersionList,
			 'usage': "{top|search} [num|searchstring] - shows the top client version replies or searches them. Defaults to showing top"}),
		)
	
	def getHooks(self):
		return (('uid', self.ctcp_UID),
			('ctcp', self.ctcp_CTCP))
