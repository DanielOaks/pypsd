from collection import *
from sys_base import *

MASK_NEWS = create_mask(2, 1)

class Channel(CollectionEntity):
	def __init__(self, name, flags, ban_source = None, ban_reason = None, ban_date = None, ban_expiry = None):
		CollectionEntity.__init__(self, name, flags, ban_source, ban_reason, ban_date, ban_expiry)

class ChannelManager(CollectionManager, Subsystem):
	def __init__(self, module):
		Subsystem.__init__(self, module, module.options, 'channels')
		CollectionManager.__init__(self, Channel)
		self.db_open()
		self.cursor.execute("CREATE TABLE IF NOT EXISTS quotes_channels (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(200) NOT NULL, flags BIGINT NOT NULL, ban_source TEXT, ban_reason TEXT, ban_date INT, ban_expiry INT, PRIMARY KEY (id), UNIQUE KEY (name))")
		self.cursor.execute("SELECT name, flags, ban_source, ban_reason, ban_date, ban_expiry FROM quotes_channels")

		for row in self.cursor.fetchall():
			name = istring(row[0])
			flags = int(row[1])
			ban_source = istring(row[2]) if row[2] != None else None
			ban_reason = istring(row[3]) if row[3] != None else None
			ban_date = int(row[4]) if row[4] != None else None
			ban_expiry = int(row[5]) if row[5] != None else None
			self[name] = Channel(name, flags, ban_source, ban_reason, ban_date, ban_expiry)

	def on_added(self, channel):
		self.module.join(channel)

	def on_removed(self, channel):
		self.module.part(channel)

	def on_banned(self, channel):
		self.module.part(channel)

	def on_unbanned(self, channel):
		if channel in self:
			self.module.join(channel)

	def commit(self):
		try:
			deleted_channels = [(chan, ) for chan in self.list_deleted()]
			changed_channels = [(chan.name, chan.flags, chan.ban_source, chan.ban_reason, chan.ban_date, chan.ban_expiry) for chan in self.list_dirty()]

			if len(deleted_channels) > 0:
				self.cursor.executemany("DELETE FROM quotes_channels WHERE name = %s", deleted_channels)
				self.module.elog.commit('Deleted %d channels from database.' % len(deleted_channels))

			if len(changed_channels) > 0:
				self.cursor.executemany("REPLACE INTO quotes_channels (name, flags, ban_source, ban_reason, ban_date, ban_expiry) VALUES (%s, %s, %s, %s, %s, %s)", changed_channels)
				self.module.elog.commit('Committed %d channels to database.' % len(changed_channels))

			self.clear_deleted()

			for chan in self.list_all():
				chan.dirty = False
		except Exception, err:
			self.module.elog.error('Channel commit failed: @b%s@b' % err)
