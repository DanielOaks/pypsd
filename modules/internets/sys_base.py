import threading
import MySQLdb as db
from untwisted.istring import istring

db_up = True

def get_db_up():
	global db_up

	return db_up

def set_db_up(state):
	global db_up

	db_up = state

class Subsystem(object):
	def __init__(self, module, options, name):
		self.module = module
		self.__options = options
		self.__lock = threading.Lock()
		self.__name = name
		self.__timer = None
		self.conn = None
		self.cursor = None
		self.reload()

	def db_open(self):
		self.conn = db.connect(
			host=self.module.config.get('database', 'host'),
			user=self.module.config.get('database', 'user'),
			passwd=self.module.config.get('database', 'passwd'),
			db=self.module.config.get('database', 'db'),
			unix_socket=self.module.config.get('database','sock')
		)
		self.conn.autocommit(True)
		self.cursor = self.conn.cursor()

	def db_close(self):
		if self.cursor != None:
			self.cursor.close()
			self.cursor = None
		
		if self.conn != None:
			self.conn.close()
			self.conn = None

	def reload(self):
		self.__delay = self.get_option('update_period', int, 120)

		if self.__timer != None:
			self.stop()
			self.on_reload()
			self.start()
		else:
			self.on_reload()

	def on_reload(self):
		pass

	def get_option(self, name, type, default):
		return self.__options.get('%s_%s' % (self.__name, name), type, default)

	def set_option(self, name, value):
		return self.__options.set('%s_%s' % (self.__name, name), value)

	def start(self):
		self.__timer = threading.Timer(self.__delay, self.worker)
		self.__timer.daemon = True
		self.__timer.start()

	def stop(self):
		self.__lock.acquire()

		try:
			if self.__timer != None:
				self.__timer.cancel()
				self.__timer = None
		finally:
			self.__lock.release()

	def force(self):
		self.__lock.acquire()

		try:
			self.commit()
		finally:
			self.__lock.release()

	def commit(self):
		pass

	def worker(self):
		global db_up

		try:
			if self.conn != None:
				self.conn.ping(True)
		finally:
			pass

		if db_up:
			self.__lock.acquire()
			try:
				self.commit()
			finally:
				self.__lock.release()

		self.__timer = threading.Timer(self.__delay, self.worker)
		self.__timer.daemon = True
		self.__timer.start()

