from urllib import urlencode
from feed import get_json

class UrbanDictionary(object):
	def __init__(self):
		self.last_def = {}
	
	def get_definitions(self, expr):
		if expr in self.last_def:
			return self.last_def[expr]
		
		url = 'http://www.urbandictionary.com/iphone/search/define?'
		url += urlencode({'term': expr})
		j = get_json(url)
		self.last_def = {expr: j}
		return j
