import random
import re
from datetime import datetime, timedelta
from xml.parsers.expat import ExpatError

from cmd_manager import *
from utils import *
from api.feed import FeedError
from api.idlerpg import IrpgPlayer
from api.quotes import FmlException
from api.weather import WeatherException


def get_citystate_from_zipcode(self, zipcode):
	"""Return [city,state] for the given U.S. zip code (if database has been imported)"""
	try:
		self.dbp.execute("SELECT city, state FROM zipcode_citystate WHERE zipcode=%s", [int(zipcode)])
		city, state = self.dbp.fetchone()
		return city, state
	except:
		return None


def command_weather(self, manager, opts, arg, channel, sender, userinfo):
	arg = self.get_location(opts, arg, channel, sender)
	if not arg:
		return
	w_state = ''
	if arg.isdigit():
		location = get_citystate_from_zipcode(self, arg)
		if location is None:
			self.errormsg(channel, 'zip code not recognised.')
			return False
		city, state = location
		location = '{city}, {state}, USA'.format(city=city, state=state)
		w_state = state + u', '
	else:
		location = arg.strip()

	try:
		w = self.weather.get_conditions(location)
	except WeatherException as exc:
		if exc == 'this key is not valid':
			self.elog.warning('WARNING: OpenWeatherMap API key is not correctly set (%s)' % exc)
			self.errormsg(channel, 'weather data is temporarily unavailable. Try again later')
		else:
			self.errormsg(channel, exc)
		return
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return

	code = get_tempcolor(w['temp_c'])

	self.msg(channel, format_weather(code, u"""@sep @b{w[city]}{w_state}{w[country]}@b @sep @bConditions@b {w[description]} @sep \
@bTemperature@b {tempcolor}{w[temp_c]}C / {w[temp_f]}F@o @sep \
@bPressure@b {w[pressure]}mb @sep @bHumidity@b {w[humidity]}% @sep \
@bRain@b {w[rain]} @sep \
Powered by OpenWeatherMap http://openweathermap.org/city/{w[id]} @sep""".format(w=w, tempcolor=code, w_state=w_state)))


def command_forecast(self, manager, opts, arg, channel, sender, userinfo):
	arg = self.get_location(opts, arg, channel, sender)
	if not arg:
		return
	w_state = ''
	if arg.isdigit():
		location = get_citystate_from_zipcode(self, arg)
		if location is None:
			self.errormsg(channel, 'zip code not recognised.')
			return False
		city, state = location
		location = '{city}, {state}, USA'.format(city=city, state=state)
		w_state = state + u', '
	else:
		location = arg.strip()

	try:
		w = self.weather.get_forecast(location)
	except WeatherException as exc:
		if exc == 'this key is not valid':
			self.elog.warning('WARNING: OpenWeatherMap API key is not correctly set (%s)' % exc)
			self.errormsg(channel, 'weather data is temporarily unavailable. Try again later')
		else:
			self.errormsg(channel, exc)
		return
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return

	fc = ' @sep '.join([u"""@b{day[name]}@b {day[description]} @c04{day[max_c]}C / {day[max_f]}F \
@c10{day[min_c]}C / {day[min_f]}F""".format(day=day) for day in w['days']])

	self.msg(channel, u'@sep @b{w[city]}{w_state}{w[country]}@b @sep {fc} @sep'.format(w=w, fc=fc, w_state=w_state))


def command_register_location(self, manager, opts, arg, channel, sender, userinfo):
	arg = arg.strip()
	try:
		w_state = ''
		if arg.isdigit():
			location = get_citystate_from_zipcode(self, arg)
			if location is None:
				self.errormsg(channel, 'zip code not recognised.')
				return False
			city, state = location
			location = '{city}, {state}, USA'.format(city=city, state=state)
			w_state = state + u', '
		else:
			location = arg.strip()

		w = self.weather.get_conditions(location)
	except WeatherException as exc:
		if exc == 'this key is not valid':
			self.elog.warning('WARNING: OpenWeatherMap API key is not correctly set (%s)' % exc)
			self.errormsg(channel, 'weather data is temporarily unavailable. Try again later')
		else:
			self.errormsg(channel, exc)
		return
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return

	loc_name = u'{w[city]}{w_state}{w[country]}'.format(w=w, w_state=w_state)

	self.users.set(sender, 'location', arg)
	self.msg(channel, u'%s: registered location @b%s@b' % (sender, loc_name))

def command_bing_translate(self, manager, opts, arg, channel, sender, userinfo):
	sp = arg.split(' ', 2)
	try:
		if len(sp) > 2:
			source, target, text = sp
			if source.lower() not in self.bing.languages or target.lower() not in self.bing.languages:
				source = self.bing.detect_language(arg)
				target = None
				translation = self.bing.translate(arg)
			else:
				source = source.lower()
				target = target.lower()
				translation = self.bing.translate(text, source, target)
		else:
			source = self.bing.detect_language(arg)
			target = None
			translation = self.bing.translate(arg)
	except FeedError, e:
		self.elog.warning('WARNING: Bing translate failed: %s' % e)
		self.errormsg(channel, e.msg)
		return
	
	self.msg(channel, '[t] [from %s] %s' % (source, translation))

def command_google_search(self, manager, opts, arg, channel, sender, userinfo):
	try:
		result = self.google.search(arg, userinfo['ip'] if userinfo['ip'] != '0' else '255.255.255.255')
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if result['responseStatus'] == 403:
		self.elog.warning('WARNING: Google Search failed: %s' % result['responseDetails'] if 'responseDetails' in result else 'unknown error')
		self.notice(sender, 'Google Search is temporarily unavailable. Try again later.')
		return
	
	result = result['responseData']['results']
	if not result:
		self.msg(channel, '[Google] No results found')
		return
	
	json = result[0]
	self.msg(channel, '[Google] @b%(title)s@b <@u%(url)s@u>' % {
					'title': unescape(json['titleNoFormatting']), 
					'url': json['unescapedUrl']})
	self.msg(channel, '[Google] @bDescription@b: %s' % unescape(json['content']).replace('<b>', '@b').replace('</b>', '@b'))

def command_calc(self, manager, opts, arg, channel, sender, userinfo):
	try:  # local calculation using PyParsing
		result = self.nsp.eval(arg)
		self.msg(channel, '[calc] {} = {}'.format(arg, result))
	except:  # just throw it at W|A, hopefully they can get it
		try:
			result = self.wolfram.alpha(arg)
		except FeedError as e:
			self.errormsg(channel, e.msg)
			return

		if result is None:
			self.msg(channel, '[W|A] Invalid input.')
		else:
			self.msg(channel, u'[W|A] {r[0]} = {r[1]}'.format(r=result))

def command_youtube_search(self, manager, opts, arg, channel, sender, userinfo):
	try:
		res = self.google.yt_search(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if not res:
		self.msg(channel, '[YouTube] No results found')
		return
	
	self.msg(channel, """@sep @bYouTube@b %(title)s @sep @bURL@b %(url)s (%(duration)s) @sep @bViews@b %(views)s @sep \
@bRating@b %(rating)s/5 - %(votes)s votes @c3@b[+]@b %(liked)s likes @c4@b[-]@b %(disliked)s dislikes @sep""" % {
			'title': res['title'],
			'url': 'http://www.youtube.com/watch?v=' + res['id'],
			'duration': '%s' % format_hms(res['duration']),
			'views': format_thousand(res['view_count']),
			'rating': round(res['rating'], 2) if res['rating'] else 0,
			'votes': format_thousand(res['rate_count']) if res['rate_count'] else 0,
			'liked': format_thousand(res['liked']) if res['liked'] else 0,
			'disliked': format_thousand(res['disliked']) if res['disliked'] else 0
			})

def command_dictionary(self, manager, opts, arg, channel, sender, userinfo):
	try:
		results = self.wordnik.definition(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if not results:
		self.msg(channel, '[dictionary] Nothing found')
		return
	
	if 'all' in opts:
		for n, res in enumerate(results, 1):
			self.notice(sender, u'@sep [{num}/{tot}] @bDefinition@b {res.word} @sep {res.text} @sep'.format(
						res=res, num=n, tot=len(results)))
	elif 'number' in opts:
		if opts['number'] - 1 < 0 or opts['number'] - 1 > len(results):
			self.errormsg(channel, 'option -n out of range: only %d definitions found.' % len(results))
			return
		
		result = results[opts['number'] - 1]
		self.msg(channel, u'@sep [{num}/{tot}] @bDefinition@b {res.word} @sep {res.text} @sep'.format(
						res=result, num=opts['number'], tot=len(results)))
	else:
		for n, res in enumerate(results, 1):
			self.msg(channel, u'@sep [{num}/{tot}] @bDefinition@b {res.word} @sep {res.text} @sep'.format(
					res=res, num=n, tot=len(results)))
			if n == 4:
				self.notice(sender, 'To view all definitions: .dict {res.word} -a. To view the n-th definition: .dict {res.word} -n <number>'.format(res=res))
				break

def command_urbandictionary(self, manager, opts, arg, channel, sender, userinfo):
	expr, sep, def_id = arg.partition('/')
	try:
		res = self.urbandictionary.get_definitions(expr.strip())
	except FeedError, e:
		self.errormsg(channel, e.msg)
		self.elog.warning('feed error in .urbandictionary: %s' % e)
		return
	
	if res['result_type'] == 'no_results' or res['result_type'] == 'fulltext':
		self.errormsg(channel, 'no results found')
	elif res['result_type'] == 'exact':
		if def_id:
			try:
				def_id = int(def_id)
				if def_id < 1:
					self.errormsg(channel, 'invalid definition number')
					return
				
				entry = res['list'][def_id - 1]
				definition = entry['definition'].replace('\r\n', ' / ').replace('\n', ' / ')
				example = entry['example'].replace('\r\n', ' / ').replace('\n', ' / ')
				self.msg(channel, u'@sep [{num}/{total}] {entry[word]} @sep {definition} @sep'.format(
						num = def_id,
						total = len(res['list']),
						res = res,
						definition = definition if len(definition) < 200 else definition[:200] + '...',
						entry = entry))
				self.msg(channel, u'@sep @bExample@b %s @sep' % (example if len(example) < 280 else example[:280] + '...'))
			except ValueError:
				self.errormsg(channel, 'invalid definition number')
			except IndexError:
				self.errormsg(channel, 'definition id out of range: only %d definitions available' % len(res['list']))
		else:	
			for num, entry in enumerate(res['list'], 1):
				if num == 4:
					self.notice(sender, 'To view a single definition with a related example, type: @b.u %s /def_number@b. For more definitions, visit: %s' % (expr, res['list'][0]['permalink']))
					break
				
				definition = entry['definition'].replace('\r\n', ' / ').replace('\n', ' / ')
				self.msg(channel, u'@sep [{num}/{total}] {entry[word]} @sep {definition} @sep'.format(
						num = num,
						total = len(res['list']),
						res = res,
						definition = definition if len(definition) < 200 else definition[:200] + '...',
						entry = entry))
	else:
		self.msg(channel, 'An exception occurred and has been reported to the developers. If this error persists please do not use the faulty command until it has been fixed.')
		self.elog.warning('unrecognized result type: %s' % res['result_type'])
	
def command_imdb(self, manager, opts, arg, channel, sender, userinfo):
	try:
		reply = self.imdb.get(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	except ValueError:
		self.errormsg(channel, 'movie not found')
		return
	
	if reply['Response'] != 'True':
		self.msg(channel, '[imdb] Nothing found')
		return
	
	self.msg(channel, u"""@sep @b{r[Title]}@b [{r[Year]}] Rated {r[Rated]} @sep @bRating@b {r[imdbRating]}/10, {r[imdbVotes]} votes @sep \
@bGenre@b {r[Genre]} @sep @bDirector@b {r[Director]} @sep @bActors@b {r[Actors]} @sep @bRuntime@b {r[Runtime]} @sep""".format(r=reply))
	self.msg(channel, u'@sep @bPlot@b {r[Plot]} @sep @uhttp://www.imdb.com/title/{r[imdbID]}/@u @sep'.format(r=reply))

def command_lastfm(self, manager, opts, arg, channel, sender, userinfo):
	try:
		user = self.lastfm.get_user(arg)
		if 'error' in user:
			self.errormsg(channel, user['message'])
			return
		latest = self.lastfm.get_recent_tracks(arg, 1)
	except FeedError as e:
		self.errormsg(channel, e.msg)
		return
	
	user = user['user']
	userinfo = []
	if user['realname']:
		userinfo.append(user['realname'])
	if user['age']:
		userinfo.append(user['age'])
	if user['country']:
		userinfo.append(user['country'])

	if userinfo:
		userinfo = ' [%s]' % ', '.join(userinfo)
	else:
		userinfo = ''

	if 'track' in latest['recenttracks']:
		if isinstance(latest['recenttracks']['track'], list):
			latest = latest['recenttracks']['track'][0]
		else:
			latest = latest['recenttracks']['track']
		try:
			latest['@attr']['nowplaying']
			latest_str = u' @bNow playing@b {latest[artist][#text]} - {latest[name]} @sep'.format(latest=latest)
		except KeyError:
			latestdate = get_timespan(datetime.fromtimestamp(int(latest['date']['uts'])))
			latest_str = u' @bLatest track@b {latest[artist][#text]} - {latest[name]} ({latestdate} ago) @sep'.format(
				latest=latest, latestdate=latestdate)
	else:
		latest_str = ''
	
	self.msg(channel, u'@sep @b{user[name]}@b{userinfo} @sep @bPlays@b {plays} since {regdate} @sep \
@bLink@b {user[url]} @sep{latest_track}'.format(
				userinfo = userinfo,
				plays = format_thousand(int(user['playcount'])),
				regdate = user['registered']['#text'][:10],
				user = user,
				latest_track = latest_str))

def command_url_shorten(self, manager, opts, arg, channel, sender, userinfo):
	if not arg.startswith('http://') and not arg.startswith('https://'):
		self.errormsg(channel, 'a valid URL must start with http:// or https://')
		return
	
	try:
		reply = self.urls.shorten(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if reply['status_code'] != 200:
		self.errormsg(channel, 'an error occurred.')
		self.elog.warning('[shorten] error: code %d, %s' % (reply['status_code'], reply['status_txt']))
	else:
		self.msg(channel, '@sep @bShort URL@b %s @sep' % reply['data']['url'])

def command_url_expand(self, manager, opts, arg, channel, sender, userinfo):
	if not arg.startswith('http://') and not arg.startswith('https://'):
		self.errormsg(channel, 'a valid URL must start with http:// or https://')
		return
	
	try:
		reply = self.urls.expand(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if 'error' in reply:
		self.errormsg(channel, reply['error'])
	else:
		self.msg(channel, '@sep @bLong URL@b {reply[long-url]} @sep @bContent-type@b {reply[content-type]} @sep'.format(reply=reply))

def command_idlerpg(self, manager, opts, arg, channel, sender, userinfo):
	try:
		player = IrpgPlayer(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if not player.name:
		self.errormsg(channel, 'player not found. @bNote@b: nicks are case sensitive.')
		return
	
	self.msg(channel, """@sep @b{player.name}@b [{status}] @sep @bLevel@b {player.level} {player.classe} @sep @bNext level@b \
{nextlevel} @sep @bIdled@b {idled_for} @sep @bAlignment@b {player.alignment} @sep""".format(
				player = player,
				status = '@c3ON@c' if player.is_online else '@c4OFF@c',
				nextlevel = timedelta(seconds=player.ttl),
				idled_for = timedelta(seconds=player.idled_for)))

def command_ipinfo(self, manager, opts, arg, channel, sender, userinfo):
	try:
		reply = self.ipinfo.get_info(arg)
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	self.msg(channel, """@sep @bIP/Host@b {arg} ({reply[ip_addr]}) @sep @bLocation@b {reply[city]}, {reply[region]}, \
{reply[country_name]} [{reply[country_code]}] @sep{map}""".format(
				reply = reply,
				arg = arg.lower(),
				map = ' http://maps.google.com/maps?q=%s,%s @sep' % (reply['latitude'], reply['longitude']) if reply['latitude'] and reply['longitude'] else ''))
	
dice_regex = re.compile('^(?:(\d+)d)?(\d+)(?:([\+\-])(\d+))?$')
def command_dice(self, manager, opts, arg, channel, sender, userinfo):
	r = dice_regex.search(arg)
	if not r:
		self.errormsg(channel, 'invalid format')
		return
	
	num, faces, type, modifier = r.groups()
	if num:
		num = int(num)
	else:
		num = 1
	faces = int(faces)
	if num < 1 or num > 32 or faces < 2 or faces > 65536:
		self.errormsg(channel, 'parameter out of range')
		return
	
	total = 0
	results = []
	for n in xrange(int(num)):
		randnum = random.randint(1, int(faces))
		total += randnum
		results.append(randnum)
	
	if type == '-':
		modifier = int(modifier)
		total -= modifier
		max = num * faces - modifier
	elif type == '+':
		modifier = int(modifier)
		total += modifier
		max = num * faces + modifier
	else:
		max = num * faces
		
	self.msg(channel, '@sep @bTotal@b {total} / {max} [{percent}%] @sep @bResults@b {results} @sep'.format(
				total = total,
				max = max,
				percent = 100 * total / max if max != 0 else '9001',
				results = str(results)))

def command_qdb(self, manager, opts, arg, channel, sender, userinfo):
	try:
		if not arg:
			quote = self.quotes.get_qdb_random()
		else:
			try:
				quote_id = int(arg)
				quote = self.quotes.get_qdb_id(quote_id)
				if not quote:
					self.errormsg(channel, 'quote @b%d@b not found' % quote_id)
					return
			except ValueError:
				self.errormsg(channel, 'invalid quote ID')
				return
			except ExpatError: # qdb returns a malformed xml when the quote doesn't exist
				self.errormsg(channel, 'quote @b%d@b not found' % quote_id)
				return
	except FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	id = quote['id']
	for line in quote['lines']:
		self.msg(channel, u'[qdb {id}] {line}'.format(id=id, line=line.replace('\n', '')))

def command_fml(self, manager, opts, arg, channel, sender, userinfo):
	try:
		if not arg:
			quote = self.quotes.get_fml()
		else:
			try:
				quote_id = int(arg)
				quote = self.quotes.get_fml(quote_id)
				if not quote:
					self.errormsg(channel, 'quote @b%d@b not found' % quote_id)
					return
			except (ValueError, IndexError):
				self.errormsg(channel, 'invalid quote ID')
				return
	except (FeedError, FmlException) as e:
		self.errormsg(channel, e.msg)
		self.elog.warning('WARNING: .fml error: %s' % e.msg)
		return

	self.msg(channel, u'[fml #{quote[id]}] {quote[text]}'.format(quote=quote))

def command_internets_info(self, manager, opts, arg, channel, sender, userinfo):
	self.notice(sender, '@sep @bRizon Internets Bot@b @sep @bVersion@b %(major)d.%(minor)d @sep @bDevelopers@b martin <martin@rizon.net> @sep @bHelp/feedback@b %(channel)s @sep' % {
		'major'   : self.VERSION,
		'minor'   : self.VERSION_MINOR,
		'channel' : '#internets'})

def command_internets_help(self, manager, opts, arg, channel, sender, userinfo):
	command = arg.lower()

	if command == '':
		message = ['internets: .help internets - for internets commands']
	elif command == 'internets':
		message = manager.get_help()
	else:
		message = manager.get_help(command)

		if message == None:
			message = ['%s is not a valid command.' % arg]

	for line in message:
		self.notice(sender, line)

class UserCommandManager(CommandManager):
	def get_prefix(self):
		return '.'

	def get_commands(self):
		return {
			'cc': 'calc',
			'calc': (command_calc, ARG_YES, 'Calculates an expression', [], 'expression'),
			
			'dict': 'dictionary',
			'dictionary': (command_dictionary, ARG_YES, 'Search for a dictionary definition', [
				('number', '-n', 'display the n-th result', {'type': '+integer'}, ARG_YES),
				('all', '-a', 'display all results (using /notice)', {'action': 'store_true'}, ARG_YES)], 'word'),
			
			'u': 'urbandictionary',
			'urbandictionary': (command_urbandictionary, ARG_YES, 'Search for a definition on Urban Dictionary', [], 'word'),
			
			'g': 'google',
			'google': (command_google_search, ARG_YES, 'Search for something on Google', [], 'google_search'),
			
			't': 'translate',
			'translate': (command_bing_translate, ARG_YES, 'Translate something from a language to another', [], 'from to text'),
			
			'yt': 'youtube',
			'youtube': (command_youtube_search, ARG_YES, 'Search for something on YouTube', [], 'youtube_search'),
			
			'w': 'weather',
			'weather': (command_weather, ARG_OPT, 'Displays current weather conditions for a location', [
				('nick', '-n', 'use the weather location linked to a nick', {'action': 'store_true'}, ARG_YES)]),
			
			'f': 'forecast',
			'forecast': (command_forecast, ARG_OPT, 'Displays 5-day forecast for a location', [
				('nick', '-n', 'use the weather location linked to a nick', {'action': 'store_true'}, ARG_YES)]),
			
			'regloc': 'register_location',
			'register_location': (command_register_location, ARG_YES, 'Links a location to your nick that will be used as default location in .w and .f', [], 'location'),
			
			'imdb': (command_imdb, ARG_YES, 'Search for information on a movie on IMDB', [], 'movie_title'),
			
			'lastfm': (command_lastfm, ARG_YES, 'Returns information on a Last.fm user', [], 'lastfm_user'),
			
			'shorten': (command_url_shorten, ARG_YES, 'Shortens a URL using http://j.mp', [], 'long_url'),
			
			'expand': (command_url_expand, ARG_YES, 'Expands a shortened URL using http://longurl.org', [], 'shortened_url'),
			
			'irpg': 'idlerpg',
			'idlerpg': (command_idlerpg, ARG_YES, 'Returns info on a player in Rizon IdleRPG (http://idlerpg.rizon.net/)', [], 'player_name'),
			
			'ipinfo': (command_ipinfo, ARG_YES, 'Returns short info on a IP address/hostname', [], 'ip/host'),
			
			'd': 'dice',
			'dice': (command_dice, ARG_YES, 'Rolls X N-sided dice with an optional modifier A (XdN+A format)', [], 'dice_notation'),
			
			'qdb': (command_qdb, ARG_OPT, 'Displays a quote from qdb.us', []),
			
			'fml': (command_fml, ARG_OPT, 'Displays a quote from http://www.fmylife.com', []),
			
			'info': (command_internets_info, ARG_NO|ARG_OFFLINE, 'Displays version and author information', []),
			'help': (command_internets_help, ARG_OPT|ARG_OFFLINE, 'Displays available commands and their usage', []),
		}
