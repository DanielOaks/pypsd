import sys_base
from datetime import datetime
from utils import *

def build_ban_message(entity):
	message = 'Source: @b%s@b.' % entity.ban_source

	if entity.ban_date != None:
		message += ' Date: @b%s@b.' % datetime.fromtimestamp(entity.ban_date)

	if entity.ban_expiry != None:
		message += ' Expiry: @b%s@b.' % datetime.fromtimestamp(entity.ban_expiry)

	if entity.ban_reason != None:
		message += ' Reason: @b%s@b.' % entity.ban_reason

	return message

def admin_chan(self, source, target, pieces):
	if len(pieces) < 1:
		return False

	action = pieces[0]

	if len(pieces) < 2:
		if action in ['l', 'list']:
			channels = ['%s' % chan.name for chan in self.channels.list_valid()]

			if len(channels) == 0:
				self.msg(target, 'No channels.')
			else:
				self.multimsg(target, 10, 'Channels: ', ', ', channels)
		elif action in ['bl', 'blist']:
			bans = ['@c10@b%s@o - %s' % (chan.name, build_ban_message(chan)) for chan in self.channels.list_banned()]

			if len(bans) == 0:
				self.msg(target, 'No banned channels.')
			else:
				self.multimsg(target, 1, '', ', ', bans)
		else:
			return False

		return True

	channel = pieces[1]

	if action in ['b', 'ban']:
		now = unix_time(datetime.now())

		if len(pieces) > 2:
			try:
				duration = parse_timespan(pieces[2])
			except:
				duration = 0

			if duration <= 0:
				self.msg(target, '%s is not a valid duration.' % pieces[2])
				return True

			expiry = now + duration
		else:
			expiry = None

		if len(pieces) > 3:
			reason = ' '.join(pieces[3:])
		else:
			reason = None

		message = 'Banned channel @b%s@b.' % channel

		if reason != None:
			message += ' Reason: @b%s@b.' % reason

		if expiry != None:
			message += ' Expiry: @b%s@b.' % datetime.fromtimestamp(expiry)

		self.channels.ban(channel, self.parent.get_user(source)[1]['nick'], reason, now, expiry)
		self.msg(target, message)
	elif action in ['u', 'unban']:
		if not self.channels.is_banned(channel):
			self.msg(target, 'Channel @b%s@b is not banned.' % channel)
		else:
			self.channels.unban(channel)
			self.msg(target, 'Unbanned channel @b%s@b' % channel)
	elif action in ['i', 'info']:
		if channel in self.channels:
			chan = self.channels[channel]

			message = ''

			if not chan.registered:
				message += '(Temporary) '

			message += '@b%s@b.' % chan.name
			message += ' News: @b%s@b.' % ('enabled' if chan.news else 'disabled')

			if self.channels.is_banned(channel):
				message += ' Banned by @b%s@b' % chan.ban_source

				if chan.ban_reason != None:
					message += ' for @b%s@b' % chan.ban_reason

				message += ' on @b%s@b.' % datetime.fromtimestamp(chan.ban_date)

				if chan.ban_expiry != None:
					message += ' Ban expires on @b%s@b.' % datetime.fromtimestamp(chan.ban_expiry)

			self.msg(target, message)
		else:
			self.msg(target, 'Channel @b%s@b is not in the database.' % channel)
	elif action in ['a', 'add']:
		if not channel in self.channels:
			self.channels.add(channel)
			self.msg(target, 'Added channel @b%s@b.' % channel)
		else:
			self.msg(target, 'Channel @b%s@b is already in the database.' % channel)
	elif action in ['r', 'remove']:
		if channel in self.channels:
			self.channels.remove(channel)
			self.msg(target, 'Removed channel @b%s@b.' % channel)
		else:
			self.msg(target, 'Channel @b%s@b is not in the database.' % channel)
	elif action in ['n', 'news']:
		if channel in self.channels:
			cur_state = self.channels[channel].news
			self.channels.set(channel, 'news', not cur_state)
			self.msg(target, 'Toggled news for channel @b%s@b. Current state: %s.' % (channel, 'disabled' if cur_state else 'enabled'))
		else:
			self.msg(target, 'Channel @b%s@b is not in the database.' % channel)
	else:
		return False

	return True

def admin_user(self, source, target, pieces):
	if len(pieces) < 1:
		return False

	action = pieces[0]

	if len(pieces) < 2:
		if action in ['l', 'list']:
			users = ['%s' % user.name for user in self.users.list_valid()]

			if len(users) == 0:
				self.msg(target, 'No users.')
			else:
				self.multimsg(target, 10, 'Users: ', ', ', users)
		elif action in ['bl', 'blist']:
			bans = ['@c10@b%s@o - %s' % (user.name, build_ban_message(user)) for user in self.users.list_banned()]

			if len(bans) == 0:
				self.msg(target, 'No banned users.')
			else:
				self.multimsg(target, 1, '', ', ', bans)
		else:
			return False

		return True

	username = pieces[1]

	if action in ['b', 'ban']:
		now = unix_time(datetime.now())

		if len(pieces) > 2:
			try:
				duration = parse_timespan(pieces[2])
			except:
				duration = 0

			if duration <= 0:
				self.msg(target, '%s is not a valid duration.' % pieces[2])
				return True

			expiry = now + duration
		else:
			expiry = None

		if len(pieces) > 3:
			reason = ' '.join(pieces[3:])
		else:
			reason = None

		message = 'Banned user @b%s@b.' % username

		if reason != None:
			message += ' Reason: @b%s@b.' % reason

		if expiry != None:
			message += ' Expiry: @b%s@b.' % datetime.fromtimestamp(expiry)

		self.users.ban(username, self.parent.get_user(source)[1]['nick'], reason, now, expiry)
		self.msg(target, message)
	elif action in ['u', 'unban']:
		if not self.users.is_banned(username):
			self.msg(target, 'User @b%s@b is not banned.' % username)
		else:
			self.users.unban(username)
			self.msg(target, 'Unbanned user @b%s@b' % username)
	elif action in ['i', 'info']:
		if username in self.users:
			user = self.users[username]

			message = ''

			if not user.registered:
				message += '(Temporary) '

			message += '@b%s@b.' % user.name

			if user.location != None:
				message += ' Location: @b%s@b.' % user.location

			if self.users.is_banned(username):
				message += ' Banned by @b%s@b' % user.ban_source

				if user.ban_reason != None:
					message += ' for @b%s@b' % user.ban_reason

				message += ' on @b%s@b.' % datetime.fromtimestamp(user.ban_date)

				if user.ban_expiry != None:
					message += ' Ban expires on @b%s@b.' % datetime.fromtimestamp(user.ban_expiry)

			self.msg(target, message)
		else:
			self.msg(target, 'User @b%s@b is not in the database.' % username)
	elif action in ['a', 'add']:
		if not username in self.users:
			self.users.add(username)
			self.msg(target, 'Added user @b%s@b.' % username)
		else:
			self.msg(target, 'User @b%s@b is already in the database.' % username)
	elif action in ['r', 'remove']:
		if username in self.users:
			self.users.remove(username)
			self.msg(target, 'Removed user @b%s@b.' % username)
		else:
			self.msg(target, 'User @b%s@b is not in the database.' % username)
	else:
		return False

	return True

def admin_db(self, source, target, pieces):
	if len(pieces) >= 1:
		state = pieces[0].lower()

		if state in ['on', 'enable']:
			sys_base.set_db_up(True)
		elif state in ['off', 'disable']:
			sys_base.set_db_up(False)
		elif state in ['state']:
			pass
		else:
			return False

	self.msg(target, 'Automatic database commits: @b%s@b.' % ('enabled' if sys_base.get_db_up() else 'disabled'))
	return True

def admin_opt(self, source, target, pieces):
	if len(pieces) < 1:
		for option in sorted(self.options):
			opt = self.options[option]
			self.msg(target, '@b%s@b: %s' % (opt[0], opt[1]))

		return True

	if len(pieces) < 2:
		return False

	operation = pieces[0]
	name = pieces[1]

	if operation == 'get':
		value = self.options.get(name, unicode)

		if value == None:
			self.msg(target, 'Option @b%s@b not found.' % name)
		else:
			self.msg(target, '@b%s@b: %s' % (name, value))
	elif operation == 'set':
		if len(pieces) < 3:
			return False

		value = ' '.join(pieces[2:])
		value = self.options.set(name, value)
		self.msg(target, '@b%s@b: %s' % (name, value))
	elif operation == 'clear':
		if self.options.clear(name):
			self.msg(target, 'Option @b%s@b cleared.' % name)
		else:
			self.msg(target, 'Option @b%s@b not found.' % name)

	return True

def admin_sys(self, source, target, pieces):
	if len(pieces) < 2:
		return False

	starget = pieces[0]
	operation = pieces[1]
	names = []
	subsystems = []

	if 'o' in starget:
		names.append('options')
		subsystems.append(self.options)

	if 'u' in starget:
		names.append('users')
		subsystems.append(self.users)

	if 'c' in starget:
		names.append('channels')
		subsystems.append(self.channels)

	if 'n' in starget:
		names.append('news')
		subsystems.append(self.news)

	if 'a' in starget:
		names.append('auth')
		subsystems.append(self.auth)

	if 'f' in starget:
		names.append('antiflood')
		subsystems.append(self.antiflood)

	if len(names) == 0:
		return False

	if operation in ['u', 'update']:
		for subsystem in subsystems:
			subsystem.force()

		self.msg(target, 'Forced update for @b%s@b.' % '@b, @b'.join(names))
	elif operation in ['r', 'reload']:
		for subsystem in subsystems:
			subsystem.reload()

		self.msg(target, 'Forced reload for @b%s@b.' % '@b, @b'.join(names))
	elif operation in ['d', 'delay']:
		if len(pieces) == 2:
			for subsystem in subsystems:
				self.msg(target, 'Auto-update delay for @b%s@b is %d seconds.' % (subsystem.name, subsystem.delay))
		else:
			try:
				seconds = int(pieces[2])
			except:
				return False

			if seconds < 10:
				self.msg(target, 'Auto-update delay must be greater than 10 seconds.')
				return True

			for subsystem in subsystems:
				subsystem.set_option('update_period', seconds)
				subsystem.reload()

			self.msg(target, 'Auto-update delay for @b%s@b set to @b%d@b seconds.' % ('@b, @b'.join(names), seconds))
	else:
		return False

	return True

def admin_log(self, source, target, pieces):
	if len(pieces) < 1:
		self.msg(target, 'Log level is @b%d@b.' % self.elog.level)
		return True

	try:
		level = int(pieces[0])
	except:
		return False

	if level < 0 or level > 7:
		return False

	self.elog.set_level(level)
	self.msg(target, 'Log level set to @b%d@b.' % level)
	return True

def admin_msg(self, source, target, pieces):
	if len(pieces) < 1:
		return False

	sender = self.parent.get_user(source)[1]['nick']
	message = ' '.join(pieces)

	for channel in self.channels.list_valid():
		self.msg(channel.name, '@b[Global message by %s]@b %s' % (sender, message))

	return True

def admin_unregistered(self, source, target, pieces):
	if len(pieces) < 1:
		return False

	action = pieces[0]

	if action not in ['check', 'list', 'part']:
		return

	channels = []
	for chan in self.channels.list_all():
		c = self.parent.get_channel(chan.name)[1]
		modes = c['modes']
		if 'z' not in modes:
			channels.append({'name': chan.name, 'modes': modes, 'users': len(c['members'])})
	
	total = len(channels)
	if total == 0:
		self.msg(target, 'No unregistered channels.')
		return True
	
	if action == 'check':
		self.msg(target, 'There are %d unregistered channels.' % total)
	elif action == 'list':
		for n, chan in enumerate(channels, 1):
			self.msg(target, '%(n)d) @b%(name)s@b [%(modes)s] @b%(users)d@b users' % {
				'n': n,
				'name': chan['name'],
				'modes': chan['modes'] if chan['modes'].startswith('+') else '+' + chan['modes'],
				'users': chan['users']})
	elif action == 'part':
		for n, chan in enumerate(channels, 1):
			self.channels.remove(str(chan['name']))
			self.msg(target, '%(n)d) Removed unregistered channel @b%(name)s@b' % {
				'n': n,
				'name': chan['name']})
	
	return True

def admin_stats(self, source, target, pieces):
	self.msg(target, 'Registered users: @b%d@b.' % len(self.users.list_all()))
	self.msg(target, 'Registered channels: @b%d@b.' % len(self.channels.list_all()))
	return True

def get_commands():
	return {
		'chan'       : (admin_chan,            '<ban|unban|info|add|remove|list|blist|news> <channel> [reason]'),
		'unreg'      : (admin_unregistered,    '<check|list|part> - remove unregistered channels'),
		'user'       : (admin_user,            '<ban|unban|info|add|remove|list|blist> <user> [reason]'),
		'stats'      : (admin_stats,           'counts registered users and channels'),
		'db'         : (admin_db,              '[on|off] - enables/disables auto commits to db'),
		'opt'        : (admin_opt,             '[get|set|clear] [option] [value] - manipulates options (list all if no arguments)'),
		'sys'        : (admin_sys,             '<subsystem> <operation> [value] - (subsystems: options (o), users (u), channels (c), news (n), auth (a), antiflood (f)) (operations: update (u), reload (r), delay (d))'),
		'log'        : (admin_log,             '[level] - gets or sets the log level (0-7).'),
		'msg'        : (admin_msg,             '<message> - sends a message to all channels'),
	}

