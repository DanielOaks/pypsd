#!/usr/bin/python pseudoserver.py
# psm_wikimonitor.py
# module for pypseudoserver
# written by ElChE <elche@rizon.net>
#
# rizon wiki monitor module

import datetime
import threading
import urllib2
import xml.dom.minidom
from untwisted.istring import istring
from psmodule import *

class MonitorThread(threading.Thread):
	def __init__(self, callback):
		threading.Thread.__init__(self)
		self._exit_event = threading.Event()
		self._callback = callback

	def run(self):
		while True:
			if self._exit_event.wait(self.interval):
				return

			self._callback()

	def stop(self):
		self._exit_event.set()

class PSModule_wikimonitor(PSModule):
	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(config.get('wikimonitor', 'nick'))
		except Exception, err:
			self.log.exception("Error reading 'wikimonitor:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(config.get('wikimonitor', 'channel'))
		except Exception, err:
			self.log.exception("Error reading 'wikimonitor:channel' configuration option: %s" % err)
			raise

		self.paused = False
		self.last_error = ''
		self.last_date = datetime.datetime.utcnow()

	def startup(self):
		if not PSModule.startup(self):
			return False

		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('wikimonitor', 'nick'),
				self.config.get('wikimonitor', 'user'),
				self.config.get('wikimonitor', 'host'),
				self.config.get('wikimonitor', 'gecos'),
				self.config.get('wikimonitor', 'modes'),
				self.config.get('wikimonitor', 'nspass'),
				join_chans = [self.chan])
		except Exception, err:
			self.log.exception("Error creating user for wikimonitor module, is the config file correct? (%s)" % err)
		
		self.thread = MonitorThread(self.check_callback)
		self.thread.interval = 60
		self.thread.start()
		return True

	def shutdown(self):
		try:
			self.thread.stop()
		except:
			pass
		
		PSModule.shutdown(self)
		self.parent.quitFakeUser(self.uid)

	def msg(self, message):
		self.parent.privMsg(self.chan, message, self.uid)

	def cmd_start(self, source, target, pieces):
		self.paused = False
		self.msg('Wiki monitor enabled')
		return True

	def cmd_stop(self, source, target, pieces):
		self.paused = True
		self.msg('Wiki monitor disabled')
		return True

	def cmd_status(self, source, target, pieces):
		status = ('disabled' if self.paused else 'enabled')

		if self.last_error != '':
			status = '%s, error (%s)' % (status, self.last_error)

		self.msg('Wiki monitor status: %s' % status)
		return True

	def cmd_interval(self, source, target, pieces):
		if len(pieces) == 0:
			self.msg('Wiki monitor update interval: %ss' % self.thread.interval)
			return True

		if len(pieces) == 1:
			try:
				self.thread.interval = int(pieces[0])
				self.msg('Wiki monitor update interval set to %ds' % self.thread.interval)
				return True
			except:
				pass
			
		return False

	def getCommands(self):
		return (
			('enable', {
			'permission' : 'w',
			'callback' : self.cmd_start,
			'usage' : "- enables monitoring"}),
			('disable', {
			'permission' : 'w',
			'callback' : self.cmd_stop,
			'usage' : "- disables monitoring"}),
			('status', {
			'permission' : 'w',
			'callback' : self.cmd_status,
			'usage' : "- checks status"}),
			('interval', {
			'permission' : 'w',
			'callback' : self.cmd_interval,
			'usage' : "[<time>] - gets or sets update interval"}),
		)

	def check_callback(self):
		if self.paused:
			return

		try:
			contents = urllib2.urlopen('http://wiki.rizon.net/index.php?title=Special:RecentChanges&feed=atom').read()
			root = xml.dom.minidom.parseString(contents)
			feed = root.getElementsByTagName("feed")[0]
			entries = feed.getElementsByTagName("entry")
			max_date = self.last_date

			for entry in reversed(entries):
				date = datetime.datetime.strptime(entry.getElementsByTagName("updated")[0].firstChild.data, '%Y-%m-%dT%H:%M:%SZ')

				if date > self.last_date:
					max_date = max(max_date, date)
					title = entry.getElementsByTagName("title")[0].firstChild.data
					link = entry.getElementsByTagName("id")[0].firstChild.data
					author = entry.getElementsByTagName("author")[0].getElementsByTagName("name")[0].firstChild.data
					self.msg('%s modified by %s (%s)' % (title, author, link))

			self.last_date = max_date
			self.last_error = ''
		except Exception, ex:
			new_error = str(ex)

			if self.last_error != new_error:
				self.last_error = new_error
				self.msg('Wiki monitor error: %s' % ex)
