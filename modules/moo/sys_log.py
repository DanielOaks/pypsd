IRCCOLOR_WHITE      = 0
IRCCOLOR_BLACK      = 1
IRCCOLOR_BLUE       = 2
IRCCOLOR_GREEN      = 3
IRCCOLOR_LIGHTRED   = 4
IRCCOLOR_BROWN      = 5
IRCCOLOR_PURPLE     = 6
IRCCOLOR_ORANGE     = 7
IRCCOLOR_YELLOW     = 8
IRCCOLOR_LIGHTGREEN = 9
IRCCOLOR_CYAN       = 10
IRCCOLOR_LIGHTCYAN  = 11
IRCCOLOR_LIGHTBLUE  = 12
IRCCOLOR_PINK       = 13
IRCCOLOR_GREY       = 14
IRCCOLOR_LIGHTGREY  = 15

class LogManager(object):
	def __init__(self, module):
		self.module = module
		self.chan = module.chan
		self.level = 7
#		self.level = module.options.get('log_level', int, 7)

	def error(self, message):
		self.elog(message, 0, IRCCOLOR_BROWN),

	def exception(self, message):
		self.elog(message, 0, IRCCOLOR_LIGHTRED),

	def traceback(self, message):
		self.elog(message, 1, IRCCOLOR_PURPLE),

	def operation(self, message):
		self.elog(message, 2, IRCCOLOR_LIGHTGREEN),

	def request(self, message):
		self.elog(message, 3, IRCCOLOR_LIGHTCYAN),

	def chanserv(self, message):
		self.elog(message, 4, IRCCOLOR_ORANGE),

	def commit(self, message):
		self.elog(message, 5, IRCCOLOR_PINK),

	def command(self, message):
		self.elog(message, 6, IRCCOLOR_LIGHTBLUE),

	def debug(self, message):
		self.elog(message, 7, IRCCOLOR_YELLOW),

	def set_level(self, level):
		self.level = level
		self.module.options.set('log_level', level)

	def elog(self, message, level, color):
		if level > self.level:
			return

		self.module.msg(self.chan, '@c%d[%d] %s@o' % (color, level, message))
