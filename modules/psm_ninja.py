#!/usr/bin/python pseudoserver.py
#psm-ninja.py
# module for pypseudoserver
# written by radicand
# doesn't do much by itself

from psmodule import *
from untwisted import task
from untwisted.istring import istring

class PSModule_ninja(PSModule):
	VERSION = 1
	
	RANKING_PERIOD = 3 # seconds
	KILL_THRESH = 3
	MIN_MSGLEN = 10
	uid = ""
	
	def startup(self):
		if not PSModule.startup(self): return False
		self.log = logging.getLogger(__name__)

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ninja_msg (item SMALLINT(5) NOT NULL AUTO_INCREMENT, msg VARCHAR(255), \
				seen INT(10), PRIMARY KEY (item), UNIQUE KEY (msg));")
		except Exception, err:
			self.log.exception("Error creating table for ninja module (%s)" % err)
			raise
		
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('ninja', 'nick'),
				self.config.get('ninja', 'user'),
				self.config.get('ninja', 'host'),
				self.config.get('ninja', 'gecos'),
				self.config.get('ninja', 'modes'),
				self.config.get('ninja', 'nspass'),
				version="PyPsd Ninja v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating ninja module user: %s" % err)
			raise

		self.task_joinChans = task.LoopingCall(self.timer_joinChans)
		self.task_joinChans.start(self.RANKING_PERIOD+1) # every X seconds
		
		return True
	
	def shutdown(self):
		PSModule.shutdown(self)
		
		try:
			self.task_joinChans.stop()
		except Exception, err:
			self.log.exception("Error shutting down ninja joinchans task (%s)" % err)

		self.parent.quitFakeUser(self.uid)

	def timer_joinChans(self):
		"""Every X seconds, scan through all channels and join the top 100?
		"""

		channel_t = self.parent.channel_t
		uid, user = self.parent.get_user(self.uid)

		data = []
		for k,v in channel_t.iteritems():
			length = len(v['members'])
			if "k" not in v['modes'] and "i" not in v['modes'] and length > 1:
				data.append( (length, k) )
				
		ct, chans = zip(*sorted(data, reverse=True)[:100])
		chans += (self.logchan,) # never leave logchan
		now = self.parent.mkts()
		
		for chan in chans:
			if chan not in user['channels']:
				# taken from ts6protocol.py
				self.parent.sendMessage("JOIN", "%d" % now, chan, "+", prefix=self.uid)
				self.parent.join_channel(self.uid, chan)
#				self.parent.privMsg(self.logchan, "Joined %s" % chan, self.uid)
#		for chan in user['channels']:
#			if chan not in chans:
#				# taken from ts6protocol.py
#				self.parent.sendMessage("PART", chan, prefix=self.uid)
#				self.parent.part_channel(self.uid, chan)
#				self.parent.privMsg(self.logchan, "Left %s" % chan, self.uid)

## Begin event hooks
	def ninja_PRIVMSG(self, prefix, params):
		"""Keeps a tab of messages received, if a message is seen more than 3
		times (or was inserted into the database manually (automatically 3),
		then AKILL the user.
		"""
			
		uid, user = self.parent.get_user(params[0])
		if uid != self.uid: return #ignore stuff not for this module

		reply = params[1]
		
		if len(reply) < self.MIN_MSGLEN: return
		
		seen = 0
		
		# autolearning below
		# to disable autolearning, just get rid of the insert section
		try:
			num = self.dbp.execute("SELECT seen FROM ninja_msg WHERE %s LIKE CONCAT('%%',msg,'%%') ORDER BY seen DESC LIMIT 1;", (reply,))
			seen = cur.fetchone()
			print seen
			if seen > 0:
				self.dbp.execute("UPDATE ninja_msg SET seen=seen+1 WHERE %s LIKE CONCAT('%%',msg,'%%');", (reply,))
			else:
				self.dbp.execute("INSERT INTO ninja_msg (msg, seen) VALUES (%s,%s);", (reply,1))
		except Exception, err:
			self.log.exception("Error updating ninja_msg: %s (%s)" % (reply, err))
			return
			
		if seen < (self.KILL_THRESH-1,): return #don't bother processing if below threshold
			

		try: user = self.parent.user_t[prefix]
		except KeyError: return #user no longer exists, extremely unlikely due to message order processing
			
		if self.parent.has_umode(prefix, "o"):
			self.parent.privMsg(self.logchan,
				"NINJA: Would have AKILL'd %s, but they are an operator: %s" %
				(user['nick'], reply), self.uid)
			return
		if user['ip'] == "0":
			self.parent.privMsg(self.logchan,
				"NINJA: Would have AKILL'd %s, but they are using a spoof: %s" %
				(user['nick'], reply), self.uid)
			return

		if self.parent.get_user("GeoServ") == (None,None): serv = "OperServ"
		else: serv = "GeoServ"
		self.parent.privMsg(serv,
			"AKILL ADD +3d *@%s Compromised Host - Blacklisted PM received." %
			(user['ip'],), self.uid)
		self.parent.privMsg(self.logchan,
			"NINJA: Banned %s!%s@%s - Blacklisted privmsg: %s" %
			(user['nick'], user['user'], user['ip'], reply), self.uid)

		if "dnsbl_admin" in self.parent.modules:
			uid2, user2 = self.parent.get_user(self.uid)
			self.parent.modules['dnsbl_admin'].add(user['ip'], "5", "Compromised host found by %s" %
				(user2['nick'],))
	
## Begin Command hooks
	def cmd_ninjaPrivmsgAdd(self, source, target, pieces):
		if not pieces: return False
		try:
			self.dbp.execute("INSERT INTO ninja_msg (msg,seen) VALUES(%s,%s);", (' '.join(pieces), self.KILL_THRESH))
			self.parent.privMsg(target, "Added privmsg blacklist #%d" % self.dbp.lastrowid, self.uid)
		except Exception, err:
			self.log.exception("Error adding msg to ninja module blacklist: (%s)" % err)
			return False

		return True

	def cmd_ninjaPrivmsgDel(self, source, target, pieces):
		if not pieces: return False
		try:
			self.dbp.execute("DELETE FROM ninja_msg WHERE item=%s;", (pieces[0],))
			self.parent.privMsg(target, "Removed blacklist", self.uid)
		except Exception, err:
			self.log.exception("Error deleting privmsg blacklist for ninja module: (%s)" % err)
			self.parent.privMsg(target, "No such ID", self.uid)
			
		return True
		
	def cmd_ninjaPrivmsgList(self, source, target, pieces):
		try:
			self.dbp.execute("SELECT item, seen, msg FROM ninja_msg;")
			for row in self.dbp.fetchall():
				self.parent.privMsg(target, "#%d(%dx): %s" % row, self.uid)
		except Exception, err:
			self.log.exception("Error selecting privmsg blacklists for ninja module: (%s)" % err)
		
		self.parent.privMsg(target, "END OF NINJA", self.uid)
		return True
## End Command hooks

	def getCommands(self):
		return (('nadd', {
			'permission':'j',
			'callback':self.cmd_ninjaPrivmsgAdd,
			'usage':"<message> - blacklists a privmsg string"}),
			('ndel', {
			'permission':'j',
			'callback':self.cmd_ninjaPrivmsgDel,
			'usage':"<message> - removes a blacklisted privmsg string"}),
			('nlist', {
			'permission':'j',
			'callback':self.cmd_ninjaPrivmsgList,
			'usage':"- shows all active blacklisted privmsg strings"}),
		)
	
	def getHooks(self):
		return (('privmsg', self.ninja_PRIVMSG),)
