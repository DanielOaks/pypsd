#!/usr/bin/python pseudoserver.py
#psm_dnsbl-admin.py
# module for pypseudoserver
# written by radicand, edited by Zion
# note that this is for a local dnsbl db

from psmodule import *

import MySQLdb as db
from untwisted import task
import re
import os

class PSModule_dnsbl_admin(PSModule):
	VERSION = 2
	
	uid = ""

	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)
		
		self.reload()
		try:
			self.dbp.execute('CREATE TABLE IF NOT EXISTS active (id MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY, ip VARCHAR(100), cid VARCHAR(100), comment VARCHAR(255), date TIMESTAMP, KEY (cid), KEY (date), UNIQUE KEY (ip));')
		except Exception, err:
			self.log.exception('Error creating table for dnsbl_admin (%s)' % err)
			raise
		
	def startup(self):
		if not PSModule.startup(self): return False
		
		self.uid = self.parent.uid
		stats = self.dnsbl_stats()
		if stats:
			self.parent.privMsg(self.logchan, 'DNSBL_admin loaded. Holding %s record(s) in database.' % stats[0], self.uid)
			for each in stats[1].keys():
				self.parent.privMsg(self.logchan, 'Zone #%s : Records: %s' % (each, stats[1][each]))

			self.task_reload = task.LoopingCall(self.dnsbl_reload)
			self.task_reload.start(1800) # reload DNSBL every 30 minutes
			
			return True
				
		else:
			self.parent.privMsg(self.logchan, 'Failed to read database stats, probably MySQL error', self.uid)
			
			return False

	def shutdown(self):
		PSModule.shutdown(self)

		try:
			self.task_reload.stop()
		except Exception, err:
			self.log.exception("Error shutting down DNSBL reload task (%s)" % err)

	def reload(self):
		PSModule.reload(self)

		try:
			self.dnsbl_url = self.config.get('dnsbl_admin', 'dnsbl_url')
		except Exception, err:
			self.log.exception("Error reading dnsbl_admin config.ini settings: %s" % err)
			raise
			
	def _validate_ip(self, ip):
		parts = ip.split('.')
		if len(parts) == 4 and all(part.isdigit() for part in parts) and all(0 <= int(part) <= 255 for part in parts):
			return True
		else:
			return False
			
	def add(self, ip, code, reason, source="internal"):
		"""External method to allow other modules to
		use for purposes of banning (e.g., BOPM discoveries)"""

		if self.dnsbl_add(ip, code, reason):
			self.log.debug("%s/%s/%s Added to DNSBL via add()" % (ip, code, reason))
			return True
		else:
			self.log.error("%s/%s/%s add() failed" % (ip, code, reason))
			return False

	def cmd_search(self, source, target, pieces):
		"""Search the dnsbl for a matching ip string"""
		
		if not pieces: return False
		if not len(pieces) == 2: return False
		
		results = self.dnsbl_search(pieces[0], pieces[1])
		if results == None:
			self.log.error("Error searching dnsbl: Result was none.")
			self.parent.privMsg(target, "Error searching DNSBL, sorry.", self.uid)
			return True

		for m in results:
			self.parent.notice(source, "#%s %s: %s (#%s) - %s" %
				(m[0], m[1], m[3], m[2], m[4]), self.uid)
		
		self.parent.notice(source, "END OF DSEARCH (%d/%d)" % (len(results),min(int(pieces[1]),len(results))), self.uid)
		return True

	def cmd_add(self, source, target, pieces):
		
		if not pieces: return False
		if len(pieces) < 3: return False

		uid, user = self.parent.get_user(source)
		if uid != None:
			accounting = "%s!%s@%s" % (user['nick'], user['user'], user['host'])
		else:
			accounting = "unknown"
		
		if not self.add(pieces[0], pieces[1], " ".join(pieces[2:]), accounting):
			self.parent.privMsg(target, "Error adding to DNSBL, sorry.", self.uid)
			return True

		# Code 1 is the proxy scanner, which always tries to agressively add the ban, so we don't have to
		if pieces[1] == "1":
			return True

		#who fell into the net?
		i=0
		victims = ""
		try:
			for k,v in self.parent.user_t.iteritems():
				if v['ip'] == pieces[0]:
					victims += v['nick'] + ","
					i += 1
			victims = victims[:-1]
			self.parent.privMsg(target, "%s Added, %d affected (%s)." % (pieces[0], i, victims), self.uid)
		except Exception, e:
			self.log.exception("Error iterating list: %s" % e)

		uid, user = self.parent.get_user("GeoServ")
		if not user:
			uid, user = self.parent.get_user("OperServ")
		self.parent.privMsg(uid, "AKILL ADD +3d *@%s Blacklisted IP. Visit %s%s for more information: Code %s: %s." %
			(pieces[0], self.dnsbl_url, pieces[0], pieces[1], " ".join(pieces[2:])), self.uid)
		
		return True
		
	def cmd_del(self, source, target, pieces):
		"""Search the dnsbl for a matching ip string"""
		
		if not pieces: return False

		# temporary hack for Rebelnoob
		self.parent.privMsg('proxywatch', 'whitelist %s' % pieces[0], self.uid)
		
		if self.dnsbl_del(pieces[0]):
			self.parent.privMsg(target, "%s deleted." % pieces[0], self.uid)
			uid, user = self.parent.get_user("GeoServ")
			if not user:
				uid, user = self.parent.get_user("OperServ")
			self.parent.privMsg(uid, "AKILL DEL *@%s" % pieces[0], self.uid)
		else:
			self.parent.privMsg(target, "Deletion failed! Malformed IP?", self.uid)
		
		return True
		
	def cmd_reload(self, source, target, pieces):
		if self.dnsbl_reload():
			self.parent.privMsg(target, "DNSBL list reloaded.", self.uid)
		else:
			self.parent.privMsg(target, "Error while reloading DNSBL list.", self.uid)
			
		return True
		
	def cmd_stats(self, source, target, pieces):
		stats = self.dnsbl_stats()
		if not stats == None:
			self.parent.notice(source, 'DNSBL database stats:', self.uid)
			self.parent.notice(source, 'Total records: %s' % stats[0], self.uid)
			for each in stats[1].keys():
				self.parent.notice(source, 'Zone #%s : Records: %s' % (each, stats[1][each]), self.uid)
		
			return True
		
		else:	
			self.parent.privMsg(target, 'Error reading DNSBL stats', self.uid)

			return True
			
	def dnsbl_stats(self):
		try:
			self.dbp.execute("SELECT cid, COUNT(ip) FROM active GROUP BY cid;")
			fetch = self.dbp.fetchall()
			records = {}
			for c in fetch:
				records[c[0]] = c[1]
					
			return (sum(records.itervalues()), records)
			
		except Exception, err:
			self.log.exception('Error while reading database for stats (%s).' % err)
			return None
			
	def dnsbl_reload(self):
		try:
			self.dbp.execute("DELETE FROM active WHERE `date` < DATE_SUB(NOW(''), INTERVAL 10 DAY);")
		except Exception, err:
			self.log.exception("Error removing expired IPs (%s), unable to continue..." % err)
			
			return False
			
		buffer = '$SOA 300 dnsbl.rizon.net routing.rizon.net 0 600 300 86400 300\n$NS 300 dnsbl.rizon.net\n'
		
		try:
			self.dbp.execute("SELECT ip, cid FROM active;")

			records = self.dbp.fetchall()
			if len(records) == 0:
				self.log.info('Database is empty. Still reloading.')
			else:
				for r in records:
					buffer += r[0] + ' :' + r[1] + ': \n'
			
			list_path = self.config.get('dnsbl_admin', 'list_path')
			tmp_path = list_path + '.tmp'

			with open(tmp_path, 'w') as f:
				f.write(buffer)

			os.rename(tmp_path, list_path) # eliminates the possibility of rbldnsd reading a partial file (compared to directly writing to list_path)
			
			self.log.info('DNSBL list reloaded. Holding %s records in database.' % len(records))
			
			return True
			
		except Exception, err:
			self.log.exception("Error reloading DNSBL list (%s)" % err)
			
			return False

	def dnsbl_append(self, ip, code):
		with open(self.config.get('dnsbl_admin', 'list_path'), 'a') as f:
			f.write(ip + ' :' + code + ': \n')
	
	def dnsbl_add(self, ip, code, reason):	
		if self._validate_ip(ip):
			try:
				affected = self.dbp.execute("REPLACE INTO active (date, cid, ip, comment) VALUES (CURRENT_TIMESTAMP, '%s', '%s', '%s');" % (code, ip, reason))
				if affected == 1: # inserted rather than updated (which would be == 2) -- updates must wait until the next periodic reload
					self.dnsbl_append(ip, code)
				return True
			except Exception, err:
				self.log.exception('Error while adding IP into database (%s).' % err)
				
		return False
				
	def dnsbl_del(self, ip):
		if self._validate_ip(ip):
			try:
				self.dbp.execute("DELETE FROM active WHERE ip LIKE '%s';" % ip)
				self.dnsbl_reload()
				return True
			except Exception, err:
				self.log.exception('Error while removing IP from database (%s).' % err)
				
	def dnsbl_search(self, ip, nresult):
		buffer = []
		try:
			self.dbp.execute("SELECT * FROM active WHERE ip LIKE '%%%s%%' LIMIT %s;" % (ip, nresult))
			for ip in self.dbp.fetchall():
				buffer.append([ip[0], ip[1], ip[2], ip[3], ip[4]])
			return buffer
		except Exception, err:
			self.log.exception('Error while searching dnsbl database (%s)' % err)
		
		return None
			
	def getCommands(self):
		return (
			('search', {
			'permission':'d',
			'callback':self.cmd_search,
			'usage':"<partial ip address> <results> - searches and returns given number of results from the dnsbl"}),
			('add', {
			'permission':'d',
			'callback':self.cmd_add,
			'usage':"<ip address> <class=2|3|5> <reason> - add a ip to the dnsbl"}),
			('del', {
			'permission':'d',
			'callback':self.cmd_del,
			'usage':"<ip address> - deletes an ip from the dnsbl"}),
			('reload', {
			'permission':'D',
			'callback':self.cmd_reload,
			'usage':"- reloads dnsbl list"}),
			('stats', {
			'permission':'d',
			'callback':self.cmd_stats,
			'usage':"- display dnsbl database stats"}),
		)
