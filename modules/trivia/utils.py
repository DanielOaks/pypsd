import time

def create_mask(shift, bits):
	return (shift, bits)

def mask(flags, type, value):
	shift = type[0]
	bits = 2 ** (type[1] - 1)

	return (flags & ~(bits << shift)) | (value << shift)

def unmask(flags, type):
	shift = type[0]
	bits = 2 ** (type[1] - 1)

	return (flags >> shift) & bits

def unix_time(datetime):
	return int(time.mktime(datetime.timetuple()))

def parse_timespan(text):
	buffer = ''
	duration = 0

	for char in text:
		if char == 'd':
			duration += int(buffer) * 24 * 60 * 60
			buffer = ''
		elif char == 'h':
			duration += int(buffer) * 60 * 60
			buffer = ''
		elif char == 'm':
			duration += int(buffer) * 60
			buffer = ''
		elif char == 's':
			duration += int(buffer)
			buffer = ''
		else:
			buffer += char

	return duration

def format_name(name):
	if name.endswith('s'):
		return "%s'" % name
	else:
		return "%s's" % name

def format_thousand(number, add_prefix = False):
	if not isinstance(number, int):
		return str(number)

	text = str(abs(number))

	length = len(text)
	count = (length - 1) / 3

	for i in xrange(1, count + 1):
		text = text[:length - (i * 3)] + ',' + text[length - (i * 3):]

	if number < 0:
		text = '-' + text
	elif add_prefix:
		text = '+' + text

	return text

def format_ascii_irc(message):
	return message.replace('@errsep', '@b@c4::@o').replace('@nsep', '@b@c7::@o').replace('@sep', '@b@c10::@o').replace('@b', chr(2)).replace('@c', chr(3)).replace('@o', chr(15)).replace('@u', chr(31))

def strip_ascii_irc(message):
	stripped = ''

	for char in message:
		if char not in [chr(2), chr(15), chr(22), chr(31)]: #Not actually stripping color codes, but we don't need that (yet)
			stripped += char

	return stripped
