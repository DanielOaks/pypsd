from collection import InvariantCollection
from sys_base import Subsystem
from untwisted.istring import istring

class OptionManager(InvariantCollection, Subsystem):
	def __init__(self, module):
		InvariantCollection.__init__(self)
		Subsystem.__init__(self, module, self, 'options')
		self.module = module
		self.db_open()
		self.cursor.execute("CREATE TABLE IF NOT EXISTS trivia_options (name VARCHAR(51) NOT NULL, value TEXT, PRIMARY KEY (name))")
		self.cursor.execute("SELECT name, value FROM trivia_options")
		self.deleted_values = []

		for row in self.cursor.fetchall():
			name = istring(row[0])
			value = istring(row[1])
			self[name] = [name, value, False]

	def get(self, name, type, default = None):
		if not name in self:
			if default != None:
				self[name] = [name, istring(default), True]

			return default

		return type(self[name][1])

	def set(self, name, value):
		new_value = istring(value)

		if not name in self:
			self[name] = [name, new_value, True]
		else:
			if (self[name][1] != new_value):
				self[name][1]= new_value
				self[name][2]= True

		return value

	def clear(self, name):
		if not name in self:
			return False

		self.deleted_values.append(name)
		del(self[name])
		return True

	def commit(self):
		try:
			deleted_values = [str(value) for value in self.deleted_values if value not in self]
			changed_values = [(str(value[0]), str(value[1])) for value in self.list_all() if value[2]]

			if len(deleted_values) > 0:
				self.cursor.executemany("DELETE FROM trivia_options WHERE name = %s", deleted_values)
				self.module.elog.commit('Deleted %d options from database.' % len(deleted_values))

			if len(changed_values) > 0:
				self.cursor.executemany("REPLACE INTO trivia_options (name, value) VALUES (%s, %s)", changed_values)
				self.module.elog.commit('Committed %d options to database.' % len(changed_values))

			self.deleted_values = []

			for value in self.list_all():
				value[2] = False
		except Exception, err:
			self.module.elog.error('Option commit failed: @b%s@b' % err)
