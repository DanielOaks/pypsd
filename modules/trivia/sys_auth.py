import time
from sys_base import *

class Request(object):
	def __init__(self, user, channel, action):
		self.user = user
		self.channel = channel
		self.action = action
		self.time = int(time.time())

class AuthManager(Subsystem):
	def __init__(self, module):
		Subsystem.__init__(self, module, module.options, 'auth')
		self.requests = {}

	def request(self, user, channel, action):
		if user in self.requests:
			self.module.msg(user, 'Sorry, you already have a pending request. Please retry later.')
			return
		
		self.requests[user] = Request(user, channel, action)
		self.module.parent.privMsg('ChanServ', 'WHY %s %s' % (channel, user), self.module.uid)
	
	def accept(self, user):
		if not user in self.requests:
			return

		request = self.requests[user]
		action = request.action
		channel = request.channel
		
		if action == 'request':
			if not channel in self.module.channels:
				self.module.channels.add(channel)
				self.module.msg(user, 'Joined @b%s@b.' % channel)
		elif action == 'remove':
			if self.module.channels.is_valid(channel):
				self.module.channels.remove(channel)
				self.module.msg(user, 'Parted @b%s@b.' % channel)
		elif action == 'news':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'news', True)
				self.module.msg(user, 'Enabled news in @b%s@b.' % channel)
		elif action == 'nonews':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'news', False)
				self.module.msg(user, 'Disabled news in @b%s@b.' % channel)
		elif action.startswith('set_theme_'):
			reqtheme = action[10:].lower()
			settheme = ""
			for theme in self.module.themes:
				if reqtheme == theme[0].lower() or reqtheme == theme[1].lower():
					settheme = theme[0] # we only care for the table name
					break
			else:
				# XXX: Breaking compatibility; original trivia first checks
				# if the calling user is founder; we save the effort.
				self.module.notice(user, 'Theme %s not found.' % reqtheme)
				del self.requests[user]
				return

			# XXX: Breaking compatibility; orig trivia uses malformed msg
			self.module.dbp.execute("UPDATE trivia_chans SET theme = %s WHERE id = %s",
					(settheme, self.module.get_cid(channel)))
			self.module.notice(user,
				"Updated the trivia theme of %s to %s." % (channel, reqtheme))
		else:
			self.module.elog.request('Invalid request. Action: @b%s@b. Channel: @b%s@b. User: @b%s@b.' % (action, channel, user))
			self.module.msg(user, 'Invalid request: @b%s@b.' % action)
			del self.requests[user]
			return

		self.module.elog.request('Request accepted. Action: @b%s@b. Channel: @b%s@b. User: @b%s@b.' % (action, channel, user))
		del self.requests[user]

	def reject_not_founder(self, user, channel):
		self.reject(user, 'You are not the channel founder of @b%s@b.' % channel)

	def reject_not_registered(self, channel):
		for user in self.requests:
			if self.requests[user].channel == channel:
				self.reject(user, 'Channel @b%s@b is unregistered.' % channel)
				break
			
	def reject(self, user, reason = ''):
		if not user in self.requests:
			return

		request = self.requests[user]
		action = request.action
		channel = request.channel
		self.module.elog.request('Request rejected. Action: @b%s@b. Channel: @b%s@b. User: @b%s@b. Reason: @b%s@b.' % (action, channel, user, reason))
		self.module.msg(user, reason)
		del self.requests[user]

	def commit(self):
		timeout_time = int(time.time()) - 20
		expired_requests = []
		
		for user in self.requests:
			request = self.requests[user]
			
			if request.time < timeout_time:
				expired_requests.append(user)
				action = request.action
				channel = request.channel
				
				try:
					self.module.elog.request('Request expired. Action: @b%s@b. Channel: @b%s@b. User: @b%s@b.' % (action, channel, user))
					self.module.msg(user, 'Request @b%s@b expired. Please retry later.' % action)
				except Exception, err:
					pass
		
		for user in expired_requests:
			del self.requests[user]
