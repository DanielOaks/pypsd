#!/usr/bin/python pseudoserver.py
# psm_quotes.py
# based on psm_limitserv.py written by celestin - martin <martin@rizon.net>

import sys
import types
import datetime
import random
from untwisted.istring import istring
from psmodule import *
from quotes import cmd_admin, sys_auth, sys_channels, sys_log, sys_options
from quotes.utils import *

class PSModule_quotes(PSModule):
	VERSION = 0.1
	uid = ""
	initialized = False
	
	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.auth.start()
		
	def bind_function(self, function):
		func = types.MethodType(function, self, PSModule_quotes)
		setattr(PSModule_quotes, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append((command, {'permission': 'j', 'callback': self.bind_function(list[command][0]), 
												'usage': list[command][1]}))

	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(config.get('quotes', 'nick'))
		except Exception, err:
			self.log.exception("Error reading 'quotes:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(config.get('quotes', 'channel'))
		except Exception, err:
			self.log.exception("Error reading 'quotes:channel' configuration option: %s" % err)
			raise
		
		self.bind_admin_commands()
					
	def startup(self):
		if not PSModule.startup(self):
			return False

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS quotes_chans (id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(200) NOT NULL, UNIQUE KEY(name)) ENGINE=MyISAM;")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS quotes_quotes (id INT(10) NOT NULL, quote VARCHAR(512) NOT NULL, nick VARCHAR(30) NOT NULL, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, channel INT NOT NULL, KEY `chan_index` (`channel`)) ENGINE=MyISAM;")
		except Exception, err:
			self.log.exception("Error creating table for quotes module (%s)" % err)
			raise
		
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('quotes', 'nick'),
				self.config.get('quotes', 'user'),
				self.config.get('quotes', 'host'),
				self.config.get('quotes', 'gecos'),
				self.config.get('quotes', 'modes'),
				self.config.get('quotes', 'nspass'),
				join_chans = [self.chan],
				version="PyPsd Quotes v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating quotes module user: %s" % err)
			raise

		try:
			self.options = sys_options.OptionManager(self)
			self.auth = sys_auth.AuthManager(self)
			self.channels = sys_channels.ChannelManager(self)
			self.elog = sys_log.LogManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for quotes module (%s)' % err)
			raise
			
		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.elog.debug('Joined channels.')
		
		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for quotes module (%s)' % err)
			raise

		self.initialized = True
		self.elog.debug('Started threads.')
		return True
	
	def shutdown(self):
		PSModule.shutdown(self)

		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()
		
		quotes_modules = [module for module in sys.modules if module.startswith('modules.quotes')]

		for module in quotes_modules:
			sys.modules.pop(module)

		self.parent.quitFakeUser(self.uid)

	def join(self, channel):
		self.parent.sendMessage("JOIN", str(self.parent.mkts()), channel, '+', prefix = self.uid)
		self.parent.join_channel(self.uid, channel)
		self.dbp.execute("INSERT IGNORE INTO quotes_chans(name) VALUES(%s)", (str(channel),))

	def part(self, channel):
		uid, user = self.parent.get_user(self.uid)

		if istring(channel) in user['channels']:
			self.parent.sendMessage("PART", channel, prefix=self.uid)
			self.parent.part_channel(self.uid, channel)
			self.dbp.execute("DELETE FROM quotes_quotes WHERE channel=%s", self.get_cid(channel))
			self.dbp.execute("DELETE FROM quotes_chans WHERE name=%s", str(channel))

	def msg(self, target, message):
		if message != '':
			self.parent.privMsg(target, format_ascii_irc(message), self.uid)

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count
		
	def notice(self, target, message):
		if message != '':
			self.parent.notice(target, format_ascii_irc(message), self.uid)
			
## Begin event hooks
	def quotes_NOTICE(self, prefix, params):
		if not self.initialized:
			return
		
		uid, user = self.parent.get_user(params[0])
		foo, userinfo = self.parent.get_user(prefix)

		if uid != self.uid or (userinfo != None and userinfo['nick'] != 'ChanServ'):
			return

		msg = params[1].strip()

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return

		self.elog.chanserv('%s' % msg)
		sp = msg.split(' ')

		if userinfo == None:
			if 'tried to kick you from' in msg:
					nick = strip_ascii_irc(sp[1])
					channel = strip_ascii_irc(sp[7])
					self.notice(nick, 'To remove this bot (must be channel founder): @b/msg %s remove %s@b' % (user['nick'], channel))
			return

		if "isn't registered" in msg:
			self.auth.reject_not_registered(strip_ascii_irc(sp[1]))
			return

		if len(sp) < 6:
			return

		if 'inviting' in sp[2]: #It's an invite notice. Let's ignore it.
			return

		nick = strip_ascii_irc(sp[0])
		channel = sp[5][0:len(sp[5]) - 1]
		
		if 'Founder' in sp[2]:
			self.auth.accept(nick)
		else:
			self.auth.reject_not_founder(nick, channel)	
	
	def get_cid(self, cname):
		"""Fetches the channel id for a given channel name."""
		self.dbp.execute("SELECT id FROM quotes_chans WHERE name = %s", (str(cname),))
		cid = self.dbp.fetchone()[0]
		return cid

	def read_quote(self, qid, cid, cname):
		"""Fetches the quote id qid from the channel with id cid and displays
		the result in the channel cname. cname is to be passed on so we can
		avoid querying the db about the name, which we have in all scenarios
		where read_quote is called anyway."""
		qid = str(qid)
		num = self.dbp.execute("SELECT nick, quote, time FROM quotes_quotes WHERE id = %s AND channel = %s",
				(qid, cid))

		if num == 0:
			self.msg(cname, "[Quote] " + qid + " does not exist!")
			return

		nick, quote, time = self.dbp.fetchone()
		tdelta = int((datetime.datetime.now() - time).total_seconds())
		tdeltastr = ""
		years = weeks = days = hours = minutes = 0

		while tdelta > 31540000:
			years += 1
			tdelta -= 31540000
		while tdelta > 604800:
			weeks += 1
			tdelta -= 604800
		while tdelta > 86400:
			days += 1
			tdelta -= 86400
		while tdelta > 3600:
			hours += 1
			tdelta -= 3600
		while tdelta > 60:
			minutes += 1
			tdelta -= 60

		if years > 0:
			tdeltastr += str(years) + " year"
			if years != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if weeks > 0:
			tdeltastr += str(weeks) + " week"
			if weeks != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if days > 0:
			tdeltastr += str(days) + " day"
			if days != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if hours > 0:
			tdeltastr += str(hours) + " hour"
			if hours != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if minutes > 0:
			tdeltastr += str(minutes) + " minute"
			if minutes != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if tdelta > 0:
			tdeltastr += str(tdelta) + " second"
			if tdelta != 1:
				tdeltastr += "s"

		self.msg(cname, "[Quote] #%s added by %s %s ago." % (qid, nick, tdeltastr))
		self.parent.privMsg(cname, '[Quote] %s' % quote, self.uid)
		return

	def quotes_PRIVMSG(self, prefix, params):
	# Parse ADD/DEL requests
		if not self.initialized:
			return

		bar, myself = self.parent.get_user(self.uid)
		channel = params[0]

		sender_id, userinfo = self.parent.get_user(prefix)
		sender = userinfo['nick']

		msg = params[1].strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			arg = ''
		else:
			command = msg[:index]
			arg = msg[index + 1:]

		command = command.lower()
		
		if self.channels.is_valid(channel) and command.startswith("."): # a channel message
			command = command[1:]
			if command == 'help' and arg == '':
				self.notice(sender, "Quotes: .help quotes - for quote commands.")
			elif command == 'help' and arg.startswith('quotes'):
				self.notice(sender, "Quotes: .quote add <quote> - adds given quote to database, must have voice or higher on channel.")
				self.notice(sender, "Quotes: .quote del <number> - removes quote number from database, must be channel founder.")
				self.notice(sender, "Quotes: .quote search <word> - searches database for given word.")
				self.notice(sender, "Quotes: .quote read <number> - messages quote matching given number.")
				self.notice(sender, "Quotes: .quote random - messages a random quote.")
				self.notice(sender, "Quotes: .quote total - messages number of quotes in database.")
			elif command == 'quote':
				args = arg.split(' ')
				cid = self.get_cid(channel)

				if args[0] == 'add':
					foo, c = self.parent.get_channel(channel)
					# Voice is the lowest rank, i.e., we can just check for emptiness
					if c['members'][sender_id] == "":
						self.notice(sender, "You must have voice or higher on the channel to add quotes.")
						return
					quote = istring(' '.join(args[1:]))

					# Extremely inefficient. If possible, somehow merge into
					# just one MySQL query
					# MAX(id) may return NULL if (and only if) there are no results, i.e., 0
					# quotes. Work around that with IFNULL(expr,elsevalue).
					self.dbp.execute("SELECT IFNULL(MAX(id)+1,1) FROM quotes_quotes WHERE channel = %s", (cid,))
					qid = self.dbp.fetchone()[0]

					self.dbp.execute("INSERT INTO quotes_quotes(id, quote, nick, channel) VALUES(%s, %s, %s, %s)",
							(qid, str(quote), sender, cid))

					self.msg(channel, "[Quote] Added quote #%d by %s" % (qid, sender))
					self.parent.privMsg(channel, '[Quote] %s' % quote, self.uid)

				if args[0] == 'del':
					if len(args) > 1 and not args[1].isdigit():
						self.notice(sender, args[1] + " is not a valid quote number.")
						return

					self.notice(sender, "Checking if you are the channel founder.")
					self.auth.request(sender, channel, 'delete_quote' + args[1])

				if args[0] == 'search':
					if len(args) < 2:
						return

					num = self.dbp.execute("SELECT id FROM quotes_quotes WHERE channel = %s AND quote LIKE CONCAT('%%',%s,'%%')",
							(cid, args[1]))
					res = self.dbp.fetchall()
					if num == 0:
						self.msg(channel, "[Quote] No quotes found.")
						return
					if num > 1:
						ids = []
						for row in res:
							ids.append(str(row[0]))
						self.msg(channel, "[Quote] %d matches found: #%s" % (num, ','.join(ids)))
						return

					self.read_quote(res[0][0], cid, channel)

				if args[0] == 'read':
					if len(args) < 2:
						return

					self.read_quote(args[1], cid, channel)
					
				if args[0] == 'random':
					self.dbp.execute("SELECT id FROM quotes_quotes WHERE channel = %s ORDER BY RAND() LIMIT 1", (cid,))
					res = self.dbp.fetchall()

					if not res:
						self.msg(channel, "[Quote] No quotes found!")
						return

					self.read_quote(res[0][0], cid, channel)

				if args[0] == 'total':
					self.dbp.execute("SELECT COUNT(id) FROM quotes_quotes WHERE channel = %s", (cid,))
					qtotal = self.dbp.fetchone()[0]
					
					if qtotal != 1:
						self.msg(channel, "[Quote] %d quotes in total" % qtotal)
					else:
						self.msg(channel, "[Quote] %d quote in total" % qtotal)

				if args[0] == 'last':
					self.dbp.execute("SELECT id FROM quotes_quotes where channel = %s ORDER BY id DESC LIMIT 1", (cid,))
					res = self.dbp.fetchall()

					if not res:
						self.msg(channel, "[Quote] No quotes found!")
						return

					self.read_quote(res[0][0], cid, channel)

			return
		
		elif channel == myself['nick']:
			if command == 'request':
				if not arg:
					self.msg(sender, '@bUsage@b: request @b#channel@b')
					return
			
				if self.channels.is_valid(arg):
					self.msg(sender, "I'm already in @b%s@b." % arg)
				elif self.channels.is_banned(arg):
					chan = self.channels[arg]
					self.elog.request('Request for banned channel @b%s@b from @b%s@b.' % (arg, sender))
					message = 'Request failed, channel @b%s@b was banned by @b%s@b.' % (arg, chan.ban_source)
		
					if chan.ban_reason != None:
						message += ' Reason: @b%s@b.' % chan.ban_reason
		
					if chan.ban_expiry != None:
						message += ' Expires: @b%s@b.' % datetime.datetime.fromtimestamp(chan.ban_expiry)
		
					self.msg(sender, message)
				else:
					self.auth.request(sender, arg, 'request')
			elif command == 'remove':
				if not arg:
					self.msg(sender, '@bUsage@b: remove @b#channel@b')
					return
			
				if not self.channels.is_valid(arg):
					self.msg(sender, "I'm not in @b%s@b." % arg)
				else:
					self.auth.request(sender, arg, 'remove')
			elif command == 'help' or command == 'hi' or command == 'hello':
				self.msg(sender, '@brequest@b requests a channel (must be founder)')
				self.msg(sender, '@bremove@b removes a channel (must be founder)')
				self.msg(sender, '@bhelp@b displays help text')
				#self.msg(sender, 'For more information, see: http://forum.rizon.net/showthread.php?981-Rizon-LimitServ')
			else:
				self.msg(sender, 'Invalid message. Say help for a list of valid messages.')
	
	def delete_quote(self, channel, user, qid):
		cid = self.get_cid(channel)

		self.dbp.execute("DELETE FROM quotes_quotes WHERE id = %s AND channel = %s", (qid, cid))
		self.notice(user, "Deleted quote #%d" % qid)
		return

	def quotes_TMODE(self, prefix, params):
		if not self.initialized:
			return

		channel = params[1]
		modes = params[2]

		if modes == '-z' and channel in self.channels:
			self.channels.remove(channel)
			self.dbp.execute("DELETE FROM quotes_quotes WHERE channel = %s", (self.get_cid(channel),))
			self.dbp.execute("DELETE FROM quotes_chans WHERE name = %s", (channel,))
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)
	
	def quotes_KICK(self, prefix, params):
		if not self.initialized:
			return
		
		if params[1] == self.nick:
			self.parent.sendMessage("JOIN", str(self.parent.mkts()), channel, '+', prefix = self.uid)
	
	def getCommands(self):
		return self.commands_admin
	
	def getHooks(self):
		return (('privmsg', self.quotes_PRIVMSG),
					('notice', self.quotes_NOTICE),
					('tmode', self.quotes_TMODE),
					('kick', self.quotes_KICK)
               )
