#!/usr/bin/python
#cron for netstats module to generate rrds
# graphs users and opers per server
# make sure to set the values in config.ini properly!

import MySQLdb as db
import re, datetime, ConfigParser, os
_thisdir, _script = os.path.split(__file__)
os.chdir(_thisdir + "/../")

config = ConfigParser.ConfigParser()
config.read("config.ini")
rrd_dir = config.get('netstats', 'rrd_dir')
img_dir = config.get('netstats', 'rrd_imgs')
rrd_create_cmd = "rrdtool create %s%s.rrd -b %s --step 300 DS:users:GAUGE:600:0:50000 DS:opers:GAUGE:600:0:50000 \
	RRA:AVERAGE:0.5:1:2030 RRA:AVERAGE:0.5:6:1500 RRA:AVERAGE:0.5:288:700 RRA:LAST:0.5:1:288 RRA:MAX:0.5:6:1500 RRA:MAX:0.5:288:700"
rrd_create_cmd_chans = "rrdtool create %s/channels.rrd -b %s --step 300 DS:chans:GAUGE:600:0:50000 \
	RRA:AVERAGE:0.5:1:2030 RRA:AVERAGE:0.5:6:1500 RRA:AVERAGE:0.5:288:700 RRA:LAST:0.5:1:288 RRA:MAX:0.5:6:1500 RRA:MAX:0.5:288:700"
rrd_graph_cmd = r'rrdtool graph -  --imgformat=PNG  --start=-1%s  --title="Users: %s (%s)"  --base=1000  --height=80  --width=400  --alt-autoscale-max  --lower-limit=.01  \
	--vertical-label="Users"  --slope-mode  --font TITLE:11:  --font AXIS:7:  --font LEGEND:8:  --font UNIT:7:  \
	DEF:a=%s:users:AVERAGE  DEF:b=%s:users:LAST  DEF:c=%s:users:MAX  \
	DEF:d=%s:opers:AVERAGE  DEF:e=%s:opers:LAST  DEF:f=%s:opers:MAX  \
	CDEF:dn=d,UN,0,d,IF \
	CDEF:en=d,UN,0,d,IF \
	CDEF:fn=d,UN,0,d,IF \
	CDEF:cdefa=b,en,+ \
	CDEF:cdefb=a,dn,+ \
	CDEF:cdefc=c,fn,+ \
	AREA:cdefb#4668E477:"Total\t"  GPRINT:cdefa:LAST:"Cur\\:%%8.2lf %%s\t" GPRINT:cdefb:AVERAGE:"Avg\\:%%8.2lf %%s\t" GPRINT:cdefc:MAX:"Max\\:%%8.2lf %%s\n" \
	LINE1:a#000000FF:"Users\t"  GPRINT:b:LAST:"Cur\\:%%8.2lf %%s\t"  GPRINT:a:AVERAGE:"Avg\\:%%8.2lf %%s\t" GPRINT:c:MAX:"Max\\:%%8.2lf %%s\n" \
	LINE1:d#FF0000FF:"Opers\t"  GPRINT:e:LAST:"Cur\\:%%8.2lf %%s\t"  GPRINT:d:AVERAGE:"Avg\\:%%8.2lf %%s\t" GPRINT:f:MAX:"Max\\:%%8.2lf %%s" \
	> %s-%s.png'
rrd_graph_cmd_chans = r'rrdtool graph -  --imgformat=PNG  --start=-1%s  --title="Users: %s (%s)"  --base=1000  --height=80  --width=400  --alt-autoscale-max  --lower-limit=.01  \
	--vertical-label="Users"  --slope-mode  --font TITLE:11:  --font AXIS:7:  --font LEGEND:8:  --font UNIT:7:  \
	DEF:a=%s:users:AVERAGE  DEF:b=%s:users:LAST  DEF:c=%s:users:MAX  \
	DEF:d=%s:opers:AVERAGE  DEF:e=%s:opers:LAST  DEF:f=%s:opers:MAX  \
	DEF:g=%s/channels.rrd:chans:AVERAGE  DEF:h=%s/channels.rrd:chans:LAST  DEF:i=%s/channels.rrd:chans:MAX  \
	CDEF:dn=d,UN,0,d,IF \
	CDEF:en=d,UN,0,d,IF \
	CDEF:fn=d,UN,0,d,IF \
	CDEF:cdefa=b,en,+ \
	CDEF:cdefb=a,dn,+ \
	CDEF:cdefc=c,fn,+ \
	AREA:cdefb#4668E477:"Total\t"  GPRINT:cdefa:LAST:"Cur\\:%%8.2lf %%s\t" GPRINT:cdefb:AVERAGE:"Avg\\:%%8.2lf %%s\t" GPRINT:cdefc:MAX:"Max\\:%%8.2lf %%s\n" \
	LINE1:a#000000FF:"Users\t"  GPRINT:b:LAST:"Cur\\:%%8.2lf %%s\t"  GPRINT:a:AVERAGE:"Avg\\:%%8.2lf %%s\t" GPRINT:c:MAX:"Max\\:%%8.2lf %%s\n" \
	LINE1:d#FF0000FF:"Opers\t"  GPRINT:e:LAST:"Cur\\:%%8.2lf %%s\t"  GPRINT:d:AVERAGE:"Avg\\:%%8.2lf %%s\t" GPRINT:f:MAX:"Max\\:%%8.2lf %%s\n" \
	LINE1:g#FFFF00FF:"Chans\t"  GPRINT:h:LAST:"Cur\\:%%8.2lf %%s\t"  GPRINT:g:AVERAGE:"Avg\\:%%8.2lf %%s\t" GPRINT:i:MAX:"Max\\:%%8.2lf %%s" \
	> %s-%s.png'
rrd_update_cmd = "rrdtool update %s%s.rrd -t users:opers %s:%s:%s"
rrd_update_cmd_chans = "rrdtool update %s/channels.rrd -t chans %s:%s"

dbx = db.connect(
	host=config.get('database', 'host'),
	user=config.get('database', 'user'),
	passwd=config.get('database', 'passwd'),
	db=config.get('database', 'db'),
)
dbx.autocommit(True) #no need to have transactions
dbp = dbx.cursor()
frames = ['Day', 'Week', 'Month', 'Year']

dbp.execute("SELECT channel, users, UNIX_TIMESTAMP(NOW()) AS ts FROM chanstate")
c = dbp.fetchall()

#update or crete channel rrd, no graph- its in totals
for row in c:
	print("CHAN: %s, #U: %s, Last: %s" % (row[0], row[1], row[2]))
	if not os.path.exists("%s/channels.rrd" % rrd_dir):
		#create rrd
		os.system(rrd_create_cmd_chans %
			(rrd_dir, int(row[2])-1))
	os.system(rrd_update_cmd_chans %
		(rrd_dir, row[2], row[1]))
	
#draw user graphs now
dbp.execute("SELECT sid,server,users,opers,uptime,UNIX_TIMESTAMP(seen) AS seen from state WHERE graphed='0';")
c = dbp.fetchall()

for row in c:
	print("SID: %s, Server: %s, #U: %s, #O: %s, UP: %s, Last: %s" %
		(row[0],row[1],row[2],row[3],row[4],row[5]))
	rpath = "%s%s.rrd" % (rrd_dir, row[1])
	ipath = "%s%s" % (img_dir, row[1])
	if not os.path.exists(rpath):
		#create RRD
		os.system(rrd_create_cmd %
			(rrd_dir, row[1], row[5]-1))
	os.system(rrd_update_cmd %
		(rrd_dir, row[1], row[5], row[2], row[3]))
		
	if row[1] == "TOTAL":
		for period in frames:
			os.system(rrd_graph_cmd_chans %
				(period[0].lower(), row[1], period,
				rpath, rpath, rpath, rpath, rpath, rpath,
				rrd_dir, rrd_dir, rrd_dir,
				ipath, period[0].lower()))
	else:
		for period in frames:
			os.system(rrd_graph_cmd %
				(period[0].lower(), row[1], period,
				rpath, rpath, rpath, rpath, rpath, rpath,
				ipath, period[0].lower()))
			
dbp.execute("UPDATE state SET graphed='1'")


###### CLEAN UP KLINES ######
dbp.execute("DELETE FROM kline WHERE expires < NOW();")
dbp.close()
dbx.close()
