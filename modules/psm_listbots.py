# psm_listbots.py: List pypsd bots but non-dynamically
# vim: set expandtab:

from psmodule import *
from untwisted.istring import istring

class PSModule_listbots(PSModule):
	VERSION = 1
	uid = ""
	
	def startup(self):
		if not PSModule.startup(self):
			return False
		self.log = logging.getLogger(__name__)

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS pypsd_bots (nick VARCHAR(30) NOT NULL PRIMARY KEY, description VARCHAR(200) NOT NULL) ENGINE=MyISAM;")
		except Exception, err:
			self.log.exception("Error creating table for listbots module: %s" % err)
			return False

		try:
			self.uid, user = self.parent.createFakeUser(
					self.config.get('listbots', 'nick'),
					self.config.get('listbots', 'user'),
					self.config.get('listbots', 'host'),
					self.config.get('listbots', 'gecos'),
					self.config.get('listbots', 'modes'),
					self.config.get('listbots', 'nspass'),
					version="pypsd listbots"
					)
			self.reload()

			return True
		except Exception, e:
			self.log.error("Error creating listbots user: %s" % err)
			raise

	def shutdown(self):
		PSModule.shutdown(self)
		self.parent.quitFakeUser(self.uid)

	def reload(self):
		try:
			PSModule.reload(self)
			self.chan = istring(self.config.get('listbots', 'channel'))
			
			try:
				self.parent.sendMessage("PART", self.chan, prefix = self.uid)
				self.parent.part_channel(self.uid, chan)
			except Exception, err:
				pass
				
			self.parent.sendMessage("JOIN", str(self.parent.mkts()), self.chan, '+', prefix = self.uid)
			self.parent.join_channel(self.uid, chan)
					
			return True
			
		except Exception, err:
			self.log.warning('Trying to part channel that module is not joined, this should be safe')
			
			return True

	###
	# Admin commands
	###
	def cmd_add(self, source, target, pieces):
		if not pieces or len(pieces) < 2:
			return False

		# We won't sanity check for the nick's existence here, after all,
		# they could be on another py instance or not be loaded right now
		# for some reason.

		desc = ""
		for i, piece in enumerate(pieces):
			if i == 0:
				continue

			if i != 1:
				desc += ' '
			desc += piece

		try:
			self.dbp.execute('INSERT INTO pypsd_bots(nick, description) VALUES(%s, %s) ON DUPLICATE KEY UPDATE description = %s',
					(pieces[0], desc, desc))
		except Exception, err:
			self.log.exception('Error updating listbots: (%s)' % err)
			self.parent.privMsg(target, 'Error adding listbots record', self.uid)
			return True

		self.parent.privMsg(target, 'Added %s (%s).' % (pieces[0], desc), self.uid)

		return True

	def cmd_del(self, source, target, pieces):
		if not pieces:
			return False

		try:
			self.dbp.execute('SELECT nick FROM pypsd_bots WHERE nick = %s', pieces[0])
			if not self.dbp.fetchone():
				self.parent.privMsg(target, 'Bot %s does not exist.', pieces[0])
				return True

			self.dbp.execute('DELETE FROM pypsd_bots WHERE nick = %s', pieces[0])
		except Exception, err:
			self.log.exception('Error updating listbots: (%s)' % err)
			self.parent.privMsg(target, 'Error deleting listbots record', self.uid)
			return True

		self.parent.privMsg(target, 'Deleted %s.' % pieces[0], self.uid)

		return True

	def cmd_list(self, source, target, pieces):
		count = 0

		try:
			self.dbp.execute('SELECT nick, description FROM pypsd_bots')
		except Exception, err:
			self.log.exception('Error fetching listbots: (%s)' % err)
			self.parent.privMsg(target, 'Error fetching listbots records', self.uid)
			return True

		self.parent.privMsg(target, 'List of listed bots:', self.uid)

		for row in self.dbp.fetchall():
			# Sanely, we can assume that hardly any nick will be longer than
			# 9 chars, even if nicklen is 30. Or the list will look like crap,
			# either of those work.
			self.parent.privMsg(target, '%-9s - %s' % (row[0], row[1]), self.uid)
			count += 1

		self.parent.privMsg(target, 'End of list of listed bots (%d bots).' % count, self.uid)

		return True

	def hook_PRIVMSG(self, prefix, pieces):
		foo, me = self.parent.get_user(self.uid)
		if istring(pieces[0]) != me['nick']: # Channel messages go IGNORED
			return

		uid, u = self.parent.get_user(prefix)
		if not u: # Servers?
			return

		msg = pieces[1].strip()

		if msg not in ('HELP', 'LIST', 'COMMANDS', 'SHOWCOMMANDS', 'INFO'):
			self.parent.notice(uid,
					'Unknown command, do /msg %s HELP' % me['nick'], self.uid)
			return

		try:
			self.dbp.execute('SELECT nick, description FROM pypsd_bots')
		except Exception, err:
			self.log.exception('Error fetching listbots: (%s)' % err)
			self.parent.notice(uid, 'An error has occurred. Please do not use this bot anymore for the time being.', self.uid)
			return

		self.parent.notice(uid, 'All commands listed here are only available to channel founders.', self.uid)
		self.parent.notice(uid, 'Once the bot joined, use .help for more information on any one bot.', self.uid)
		self.parent.notice(uid, ' ', self.uid)

		rows = self.dbp.fetchall()

		for i, row in enumerate(rows):
			self.parent.notice(uid, '\002%s\002: %s' % (row[0], row[1]), self.uid)
			self.parent.notice(uid, 'To request: \002/msg %s request \037#channel\037\002' % (row[0]), self.uid)
			self.parent.notice(uid, 'To remove: \002/msg %s remove \037#channel\037\002' % (row[0]), self.uid)
			if i < len(rows)-1:
				self.parent.notice(uid, ' ', self.uid)

	def getCommands(self):
		return (
			('add', {
				'permission': 'B',
				'callback': self.cmd_add,
				'usage': '<nick> <description> - adds a new bot to the list (or change its description) with the given description'
				}),
			('del', {
				'permission': 'B',
				'callback': self.cmd_del,
				'usage': '<nick> - removes a bot listed'
				}),
			# For convenience, also alias list here
			('list', {
				'permission': 'B',
				'callback': self.cmd_list,
				'usage': '- displays all bots listed and their description'
				})
			)

	def getHooks(self):
		return (('privmsg', self.hook_PRIVMSG),
				)

