import itertools
import random
import re
from datetime import datetime
from decimal import Decimal, InvalidOperation
from math import ceil
from operator import itemgetter

from api import battle, citizen, company, country, exchange, extapi, feed, map as erepmap, market, region, utils
from cmd_manager import *
from utils import *

def command_citizen_register(self, manager, opts, arg, channel, sender):
	try:
		c = citizen.from_heapy(int(arg), by_id=True) if 'id' in opts else citizen.from_heapy(arg)
	except ValueError, e:
		self.errormsg(channel, '%s is not a valid id.' % arg)
		return
	except feed.FeedError, e:
		if e.msg == 'Citizen not found or not included in database':
			if 'id' not in opts:
				self.errormsg(channel, 'to link a citizen, use: @b.regnick -i @uyour_id@u@b')
				return
			try:
				c = citizen.from_html(int(arg))
			except feed.FeedError, e:
				self.errormsg(channel, e.msg if e.msg != 'not found.' else 'citizen not found.')
				return
		else:
			self.errormsg(channel, e.msg)
			return
	
	self.users.set(sender, 'citizen', c.id)
	self.msg(channel, '%s: registered eRepublik citizen @b%s@b' % (sender, c.name))

def command_citizen_info(self, manager, opts, arg, channel, sender):
	c = self.get_citizen(opts, arg, channel, sender)

	if c == None:
		return

	if not c.is_organization:
		position = 'President' if c.is_president else 'Congressman' if c.is_congressman else ''
		
		message = u"""@sep @b{c.name}@b [{c.id}] @sep @bWellness@b {c.wellness} @sep @bStrength@b {c.strength:,.2f} @sep @bRank@b {c.rank[name]} \
[{c.rank[id]}] {c.rank_points:,}/{dmg_to_rank:,} points @sep @bLevel@b {c.level} ({c.experience:,} XP) @sep @bAge@b {age:,} days @sep \
@bLocation@b {c.region[name]}, {c.country[name]} @sep @bCitizenship@b {c.citizenship[name]}{position}{party}{employer} @sep""".format(
			c           = c,
			dmg_to_rank = c.rank['points'] - 1,
			age         = c.age,
			position    = ' @sep @bPosition@b %s' % position if position else '',
			party       = ' @sep @bParty@b %s' % (c.party['name']) if c.is_party_member else '',
			employer    = u' @sep @bEmployed at@b {c.employer[name]} [{c.employer[id]}]'.format(c=c) if c.employer else '')

	else:
		message = '@sep [Org] @b%(name)s@b [%(id)d] @sep @bLocation@b %(region)s, %(country)s @sep @bAge@b %(age)d days @sep' % {
			'name'        : c.name,
			'id'          : c.id,
			'age'         : c.age,
			'region'      : c.region['name'],
			'country'     : c.country['name']}
	
	self.msg(channel, format_citizen_message(c, message))
	
def command_citizen_avatar(self, manager, opts, arg, channel, sender):
	c = self.get_citizen(opts, arg, channel, sender)
	
	if c == None:
		return
	
	if 'small' in opts:
		av = c.avatar
	elif 'medium' in opts:
		av = c.avatar.replace('_55x55', '_100x100')
	elif 'large' in opts:
		av = c.avatar.replace('_55x55', '_142x142')
	else:
		av = c.avatar.replace('_55x55', '')
	
	self.msg(channel, format_citizen_message(c, "@sep @b%(name)s@b'%(s)s avatar link @sep %(link)s @sep" % {
		'name': c.name,
		's'   : 's' if c.name[-1] != 's' else '',
		'link': av}))

def command_citizen_fightcalc(self, manager, opts, arg, channel, sender):
	fights = 1 if 'fights' not in opts or 'objective' in opts else opts['fights']
	if fights >= 100000 or fights == 0:
		self.errormsg(channel, 'fights: expected value (0 <= x <= 100000), got %d instead' % fights)
		return
	
	if 'rank' in opts and 'strength' in opts:
		c = None
	else:
		c = self.get_citizen(opts, arg, channel, sender)
	
		if c == None:
			return
		
		if c.is_organization:
			self.errormsg(channel, '%s is an organization.' % c.name)
			return
	
	colors = [10, 9, 3, 8, 7, 4, 5, 6]
	rank = c.rank if 'rank' not in opts else opts['rank']
	strength = c.strength if 'strength' not in opts else opts['strength']
	natural_enemy = True if 'naturalenemy' in opts else False
	
	if 'nextrank' in opts:
		if not c:
			self.errormsg(channel, 'option -N needs a citizen')
			return
		
		natural_enemy = False # NE bonus removed for training
		objective = (c.rank['points'] - c.rank_points) * 10
		obj_str = 'fights required to rank up - {inf:,} '.format(inf=objective)
	elif 'influence' in opts:
		if not c:
			self.errormsg(channel, 'option -I needs a citizen')
			return
		
		if opts['influence'] >= 10000000000:
			self.errormsg(channel, 'option -I out of range')
			return
		
		natural_enemy = False
		objective = 10 * (opts['influence'] - c.rank_points)
		if objective < 0:
			self.errormsg(channel, 'this citizen already has more than {rp:,} rank points'.format(rp=opts['influence']))
			return
		
		obj_str = 'fights required to reach {rp:,} rank points, {inf:,} '.format(rp=opts['influence'], inf=objective)
	elif 'objective' in opts:
		objective = opts['objective']
		if objective >= 10000000000:
			self.errormsg(channel, 'option -o out of range')
			return
		
		obj_str = 'fights required for {inf:,} '.format(inf=objective)
	else:
		objective = None
		obj_str = ''
	
	damage = [utils.fight_calc(q*20 if q != 7 else 200, rank, strength, natural_enemy) for q in range(8)]
	if objective:
		damage = [1 + (objective / d) if objective % d != 0 else objective / d for d in damage]
	damagestr = ['@c%d[Q%d: @b%d@b]@c' % (z[0], q, fights * z[1]) for q, z in enumerate(zip(colors, damage))]
	
	message = '@sep @b%(name)s@b(%(rank)s, %(strength)s strength%(ne)s) %(obj)sinfluence%(fights)s @sep %(damage)s @sep' % {
		'name'    : '%s ' % c.name if c else '',
		'rank'    : '%s [%d]' % (rank['name'], rank['id']),
		'strength': strength,
		'ne'      : ', against Natural Enemy' if natural_enemy else '',
		'obj'     : obj_str,
		'fights'  : ' in %d fights' % fights if fights != 1 else '',
		'damage'  : ' '.join(damagestr)}
	
	self.msg(channel, format_citizen_message(c, message))

def command_citizen_sscalc(self, manager, opts, arg, channel, sender):
	natural_enemy = True if 'naturalenemy' in opts else False
	friends = 0 if 'friends' not in opts else opts['friends']
	if friends not in [0, 1, 2]:
		self.errormsg(channel, 'Invalid number of friends. Acceptable values: 0, 1, 2.')
		return
	
	if 'strength' in opts:
		c = None
	else:
		c = self.get_citizen(opts, arg, channel, sender)
		
		if c == None:
			return
		
		if c.is_organization:
			self.errormsg(channel, '%s is an organization.' % c.name)
			return
		
	strength = c.strength if 'strength' not in opts else opts['strength']
	base = 6
	bonus = ['daily reward (+1 str)']
	bonus.append('%d friend%s%s' % (friends, 's' if friends != 1 else '', ' (+%s str)' % (friends * 0.5) if friends > 0 else ''))
	
	next = (int(strength / 250) + 1) * 250
	diff = next - strength
	inc = base
	cost = Decimal('0')
	
	septbonus = 'septemberbonus' in opts
	if septbonus:
		inc += Decimal('1.5')
		bonus.append('September training bonus (+30% str)')
		
	if 'climbingcenter' in opts:
		cost += Decimal('0.19')
		inc += Decimal('2.5')
		if septbonus:
			inc += Decimal('0.8')
		bonus.append('climbing center (+2.5 str, +0.19g)')
	if 'shootingrange' in opts:
		cost += Decimal('1.49')
		inc += 5
		if septbonus:
			inc += Decimal('1.5')
		bonus.append('shooting range (+5 str, +1.49g)')
	if 'specialforces' in opts:
		cost += Decimal('1.79')
		inc += 10
		if septbonus:
			inc += 3
		bonus.append('special forces (+10 str, +1.79g)')
	if natural_enemy:
		inc += Decimal('0.5')
		bonus.append('natural enemy (+0.5 str)')
	
	inc += friends * Decimal('0.5')
	
	days = int(ceil(diff / inc))
	total_cost = cost * days
	message = """@sep {name}({strength:,} / {required:,} strength) days/gold required until next Super Soldier medal @sep @bResult@b {days} days and \
{gold:.2f} gold @sep @bBonus@b [+{bonus} / day] {bonus_list} @sep""".format(
		name = '@b%s@b ' % c.name if c else '',
		strength = strength,
		required = next,
		days = days,
		gold = total_cost,
		bonus = inc,
		bonus_list = ', '.join(bonus))
	self.msg(channel, format_citizen_message(c, message))

def command_citizen_link(self, manager, opts, arg, channel, sender):
	c = self.get_citizen(opts, arg, channel, sender)

	if c == None:
		return

	if not c.is_organization:
		message = '@sep @b%(name)s@b profile @sep http://www.erepublik.com/en/citizen/profile/%(id)d @sep' % {
			'name' : format_name(c.name),
			'id'   : c.id}
	else:
		message = '@sep @b%(name)s@b profile @sep http://economy.erepublik.com/en/accounts/%(id)d @sep' % {
			'name' : format_name(c.name),
			'id'   : c.id}
	
	self.msg(channel, format_citizen_message(c, message))

def command_citizen_donate(self, manager, opts, arg, channel, sender):
	c = self.get_citizen(opts, arg, channel, sender)

	if c == None:
		return

	self.msg(channel, format_citizen_message(c, """@sep @b%(name)s@b donations @sep @bItems@b http://www.erepublik.com/en/economy/donate-items/%(id)d @sep \
@bMoney@b http://www.erepublik.com/en/economy/donate-money/%(id)d @sep""") % {
		'name' : format_name(c.name),
		'id'   : c.id})

def command_citizen_message(self, manager, opts, arg, channel, sender):
	c = self.get_citizen(opts, arg, channel, sender)

	if c == None:
		return

	self.msg(channel, format_citizen_message(c, '@sep @b%(name)s@b message link @sep http://www.erepublik.com/en/main/messages-compose/%(id)d @sep') % {
		'name' : format_name(c.name),
		'id'   : c.id})

def command_citizen_medals(self, manager, opts, arg, channel, sender):
	c = self.get_citizen(opts, arg, channel, sender, False)

	if c == None:
		return

	medals = []
	total = 0

	for medal in sorted(c.medals, key = lambda medal: c.medals[medal], reverse = True):
		medals.append('@b%d@bx %s' % (c.medals[medal], medal.title()))
		total += c.medals[medal]
	
	message = '@sep @b%(name)s@b @sep @bMedals@b %(count)d%(medals)s @sep' % {
		'name'  : c.name,
		'count' : total,
		'medals': ' @sep %s' % ' @sep '.join(medals) if total > 0 else ''}

	self.msg(channel, format_citizen_message(c, message))

def command_citizen_rankings(self, manager, opts, arg, channel, sender):
	c = self.get_citizen(opts, arg, channel, sender, no_api_request=True)

	if c == None:
		return
	
	try:
		if c['id']:
			rankings = extapi.get_rankings(c['id'])
		else:
			rankings = extapi.get_rankings(c['name'])
	except feed.FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	if 'error' in rankings:
		self.errormsg(channel, rankings['error'])
		self.elog.warning('[warning] 1way API fail: %s' % rankings['error'])
		return
	
	country = rankings['country_rankings']
	world = rankings['world_rankings']

	def format_ranking(source):
		return '@bStrength@b {str} ({cstr:,.2f}) @sep @bRank points@b {rp} ({r[rp]:,}) @sep @bQ5 Influence@b {inf} ({r[inf]:,}) @sep @bXP@b {xp} ({r[xp]:,})'.format(
				r = rankings,
				str = format_ordinal(source['strength'], add_thousands_sep=True),
				cstr = Decimal(rankings['str']),
				rp = format_ordinal(source['rank_points'], add_thousands_sep=True),
				inf = format_ordinal(source['influence'], add_thousands_sep=True),
				xp = format_ordinal(source['level'], add_thousands_sep=True))
			
	self.msg(channel, format_citizen_message(rankings, u"""@sep @b{r[name]}@b [{r[id]}] rankings in @b{country_name}@b ({country[total]:,} citizens) @sep {ranking_str} @sep""".format(
		r = rankings,
		country = country,
		ranking_str = format_ranking(country),
		country_name = erepmap.get_country_name(rankings['cid'])), rankings['sex']))
	self.msg(channel, format_citizen_message(rankings, u"""@sep @b{r[name]}@b [{r[id]}] @bglobal@b rankings ({world[total]:,} citizens) @sep {ranking_str} @sep""".format(
		r = rankings,
		world = world,
		ranking_str = format_ranking(world)), rankings['sex']))

def command_market_info(self, manager, opts, arg, channel, sender):
	regex = re.compile("^(?:(\D+?)\s(\D+?))(?:\s([1-6]))?$")
	r = regex.search(arg)

	if r == None or len(r.groups()) != 3:
		self.usagemsg(channel, '.market INDUSTRY COUNTRY QUALITY', ['.market food usa 2', '.market grain italy'])
		return

	industry, country, quality = r.groups()
	
	if not quality:
		quality = 1
	else:
		quality = int(quality)

	try:
		industry = utils.get_industry(industry)
	except utils.InvalidValueError, err:
		self.errormsg(channel, str(err))
		return
	
	if quality > 5 and industry['id'] != 1 and industry['id'] != 2:
		self.errormsg(channel, 'quality parameter out of range (1-5)')
		return

	try:
		if country != 'world':
			country_id = erepmap.get_country_id(country)
			if country_id == None:
				self.errormsg(channel, 'country \'%s\' not found.' % country)
				return
			country_name = erepmap.get_country_name(country_id)
		else:
			country_id = 0
			country_name = 'World'
	except feed.FeedError, e:
		self.errormsg(channel, e.msg)
		return

	try:
		m = market.get(industry, country_id, quality)
	except feed.FeedError, e:
		self.errormsg(channel, e.msg)
		return

	if len(m) > 0:
		best = m[0]
		currency = erepmap.get_country_currency(best['country'])
		
		self.msg(channel, u"""@sep Best offer for {quality}@b{product}@b in @b{country}@b @sep @bPrice@b {off[price]} {currency} ({off[goldprice]} GOLD) \
{off[amount]} available @sep @bLink@b {link} @sep @bLast update@b {last_update} ago @sep""".format(
			quality        = 'Q%d ' % quality if industry['domain']['name'] != 'Land' else '',
			product        = industry['name'],
			country        = country_name,
			off            = best,
			currency       = currency,
			link           = 'http://economy.erepublik.com/en/market/%d/%d/%d' % (best['country'], industry['market_id'], quality),
			last_update    = get_timespan(datetime.fromtimestamp(best['last_updated_ts']))))
	else:
		self.msg(channel, 'No products available on the market with selected attributes.')

def command_region_info(self, manager, opts, arg, channel, sender):
	r = self.get_region(opts, arg, channel, sender)

	if r == None:
		return

	self.msg(channel, '@sep @b%(region)s, %(country)s@b [%(id)d] @sep @bPopulation@b %(pop)d%(raw)s%(cons)s @sep' % {
		'region'   : r.name,
		'country'  : r.country['name'],
		'id'       : r.id,
		'pop'      : r.population,
		'raw'      : ' @sep @bResource@b %s' % r.raw_materials if r.raw_materials else '',
		'cons'     : ' @sep @bConstructions@b%s%s' % (' %dx hospital%s' % (len(r.hospitals), 's' if len(r.hospitals) != 1 else '') if r.hospitals != 0 else '', ' %dx defense system%s' % (len(r.defense), 's' if len(r.defense) != 1 else '') if r.defense != 0 else '') if len(r.hospitals) != 0 and len(r.defense) != 0 else ''})

def command_region_borders(self, manager, opts, arg, channel, sender):
	r = self.get_region(opts, arg, channel, sender)

	if r == None:
		return

	neighbors = {}

	for n in r.neighbours:
		country = n['country']['name']
		reg = n['region']['name']

		if country in neighbors:
			neighbors[country].append(reg)
		else:
			neighbors[country] = [reg]

	self.msg(channel, '@sep Borders of @b%(region)s, %(country)s@b @sep %(borders)s @sep' % {
		'region'  : r.name,
		'country' : r.country['name'],
		'borders' : ' @sep '.join(['@b%s:@b %s' % (n , ', '.join(neighbors[n])) for n in neighbors])})

def command_region_distance(self, manager, opts, arg, channel, sender):
	if ',' in arg:
		start, end = arg.split(',', 1)
	else:
		start, end = None, arg
	
	if not end:
		self.errormsg(channel, 'region not found')
		return
	
	if start:
		c = None
	else:
		c = self.get_citizen(opts, '', channel, sender)
		if c == None:
			return
		start = c.region['name']
	
	start_zone = erepmap.get_region_zone(start.strip())
	if not start_zone:
		self.errormsg(channel, 'region not found')
		return
	
	end_country = erepmap.get_country_id(end.strip())
	if not end_country:
		end_zone = erepmap.get_region_zone(end.strip()) # not a country, check to see if it's a region name
		if not end_zone: # not even a region
			self.errormsg(channel, 'region not found')
			return
		else: # it's a region, calculate path normally
			distance = erepmap.get_region_distance(start_zone, end_zone)
			self.msg(channel, '@sep {start[name]} [{szone}] -> {end[name]} [{ezone}] @sep @bDistance@b {dist} zones @sep @bCost@b {cost} currency units @sep'.format(
				start = erepmap.get_region(start.strip()),
				end = erepmap.get_region(end.strip()),
				szone = start_zone,
				ezone = end_zone,
				dist = distance,
				cost = 20 * distance
				))
	else: # get all regions in this country, check shortest path for each region in a different zone
		country_regions = country.get_regions(end_country)
		country_regions = country_regions['regions']
		paths = [] # [{'zone': zone, 'regions': regions in this zone, iterator group, 'cost': movement cost}, ...]
		for zone, zone_regions in itertools.groupby(sorted(country_regions, key=itemgetter('zone')), itemgetter('zone')):
			paths.append({'zone': zone, 'cost': erepmap.get_region_distance(start_zone, zone), 'regions': list(zone_regions)})
		
		cheapest = sorted(paths, key=itemgetter('cost'))
		if cheapest:
			cheapest = cheapest[0]
			self.msg(channel, '@sep {start[name]} [{szone}] -> @b{endcountry}@b ({end[name]}) [{ezone}] @sep @bDistance@b {dist} zones @sep @bCost@b {cost} currency units @sep'.format(
				start = erepmap.get_region(start.strip()),
				end = random.choice(cheapest['regions']),
				endcountry = erepmap.get_country_name(end_country),
				szone = start_zone,
				ezone = cheapest['zone'],
				dist = cheapest['cost'],
				cost = 20 * cheapest['cost']
				))
		else:
			self.errormsg(channel, '@b%s@b has no regions' % erepmap.get_country_name(end_country))

def command_country_info(self, manager, opts, arg, channel, sender):
	if 'id' in opts:
		try:
			country_id = int(arg)
		except ValueError:
			self.errormsg(channel, '%s is not a valid id.' % arg)
			return
	else:
		country_id = erepmap.get_country_id(arg)
	
	if not country_id:
		self.errormsg(channel, 'country @b%s@b not found.' % arg)
		return
	
	try:
		countryfeed = country.from_id(country_id)
	except feed.FeedError as e:
		self.errormsg(channel, e.msg)
		return
	
	if 'error' in countryfeed:
		self.errormsg(channel, countryfeed['error'])
		self.elog.warning('[warning] 1way API fail: %s' % countryfeed['error'])
		return
	
	if len(countryfeed['citizenships']) > 4:
		cits = countryfeed['citizenships'][:3]
		cits.append({'amount': sum(map(itemgetter('amount'), countryfeed['citizenships'][3:])), 'name': 'other countries'})
	else:
		cits = countryfeed['citizenships']
	
	if len(countryfeed['residents']) > 4:
		resids = countryfeed['residents'][:3]
		resids.append({'amount': sum(map(itemgetter('amount'), countryfeed['residents'][3:])), 'name': 'other countries'})
	else:
		resids = countryfeed['residents']
		
	self.msg(channel, u"""@sep @b{country[name]}@b [{country[id]}] @sep @bCapital@b {capital} \
@sep @bCitizenships@b {country[citizenships_total]:,} [{citizenships}] @sep @bResidents@b {country[residents_total]:,}{residents} \
@sep @bLink@b http://www.erepublik.com/en/country/society/{country[link]} @sep""".format(
			country = countryfeed,
			capital = countryfeed['capital']['name'] if countryfeed['capital'] else 'None',
			citizenships = ', '.join(['{percent:.1%} in {country}'.format(percent = c['amount'] / float(countryfeed['citizenships_total']), country = c['name']) for c in cits]),
			residents = (' [%s]' % ', '.join(['{percent:.1%} from {country}'.format(percent = c['amount'] / float(countryfeed['residents_total']), country = c['name']) for c in resids])) if resids else ''))
	

def command_country_resources(self, manager, opts, arg, channel, sender):
	if 'id' in opts:
		try:
			country_id = int(arg)
		except ValueError:
			self.errormsg(channel, '%s is not a valid id.' % arg)
			return
	else:
		country_id = erepmap.get_country_id(arg)
	
	if not country_id:
		self.errormsg(channel, 'country @b%s@b not found.' % arg)
		return
	
	try:
		regions = country.get_regions(country_id)
	except feed.FeedError as e:
		self.errormsg(channel, e.msg)
		return
	
	if 'error' in regions:
		self.errormsg(channel, regions['error'])
		self.elog.warning('[warning] 1way API fail: %s' % regions['error'])
		return
	
	country_info = regions['country']
	region_list = regions['regions']
	
	frm = [7, 8, 9, 10, 11]
	wrm = [12, 13, 14, 15, 16]
	
	food_regions = []
	weapon_regions = []
	
	for region in itertools.ifilter(itemgetter('is_connected'), region_list):
		bonus = region['resource']['id']
		if bonus in frm:
			food_regions.append(region)
		elif bonus in wrm:
			weapon_regions.append(region)
	
	food_regions = dict((key['name'], list(group)) for (key, group) in itertools.groupby(sorted(food_regions, key=itemgetter('resource')), itemgetter('resource')))
	weapon_regions = dict((key['name'], list(group)) for (key, group) in itertools.groupby(sorted(weapon_regions, key=itemgetter('resource')), itemgetter('resource')))
	
	self.msg(channel, """@sep @b{country[name]}@b resources @sep @bFood@b +{food_bonus}%{food_list} @sep @bWeapons@b +{weapons_bonus}%{weapons_list} @sep \
@bLink@b http://www.erepublik.com/en/country/economy/{country[link]} @sep""".format(
		country = country_info,
		food_bonus = 20 * len(food_regions),
		weapons_bonus = 20 * len(weapon_regions),
		food_list = (' (%s)' % ', '.join(['%dx %s' % (len(regions), key) for key, regions in sorted(food_regions.iteritems())])) if food_regions else '',
		weapons_list = (' (%s)' % ', '.join(['%dx %s' % (len(regions), key) for key, regions in sorted(weapon_regions.iteritems())])) if weapon_regions else ''
		))

def command_country_tax(self, manager, opts, arg, channel, sender):
	sp = arg.split(' ', 1)
	if len(sp) == 1:
		self.usagemsg(channel, '.tax industry country', ['.tax food italy'])
		return
	
	industry, country_name = sp
	try:
		industry = utils.get_industry(industry)
	except utils.InvalidValueError, err:
		self.errormsg(channel, str(err))
		return
	
	country_id = erepmap.get_country_id(country_name)
	if not country_id:
		self.errormsg(channel, 'invalid country name')
		return
	
	res = country.get_taxes(country_id)
	tax = itertools.ifilter(lambda x: x['industry']['id'] == industry['market_id'], res['taxes']).next()
	self.msg(channel, """@sep @bTaxes@b in {country_name} @sep @bIndustry@b {tax[industry][name]} @sep @bIncome@b {tax[income]}% @sep @bImport@b {tax[import]}% @sep \
@bVAT@b {tax[vat]}% @sep @bLast updated@b {last_update} ago @sep""".format(
		country_name = res['country']['name'],
		tax = tax,
		last_update = get_timespan(datetime.fromtimestamp(res['last_updated_ts']))))
	
def command_country_mpp(self, manager, opts, arg, channel, sender):
	if arg == '' and not 'id' in opts:
		citizen = self.get_citizen(opts, arg, channel, sender)
		
		if citizen == None:
			return
		
		arg = citizen.country['id']
		opts['id'] = True
		
	try:
		id = int(arg) if 'id' in opts else erepmap.get_country_id(arg)
	except ValueError:
		self.errormsg(channel, '%s is not a valid id.' % arg)
	except feed.FeedError, e:
		self.errormsg(channel, e.msg)
		return

	if id == None:
		self.errormsg(channel, 'country not found.')
		return

	name = erepmap.get_country_name(id)
	if not name:
		self.errormsg(channel, 'country not found.')
		return
	
	try:
		mpps = erepmap.get_mpp_list(id)
	except feed.FeedError, e:
		self.errormsg(channel, e.msg)
		return

	message = ['@b%s@b %s' % (mpp['country'], '%s/%s' % (mpp['expiration'][6:8], mpp['expiration'][4:6])) for mpp in mpps]
	count = len(mpps)
	
	if count > 0:
		self.multimsg(channel, 15, '@sep @b%(name)s@b @sep %(count)s MPP%(s)s @sep ' % {
			'name'  : name,
			'count' : count,
			's'     : 's' if count != 1 else ''}, 
			' @sep ', message, ' @sep ')
	else:
		self.msg(channel, '@sep @b%(name)s@b has no active MPPs @sep' % {'name': name})

def command_battle_info(self, manager, opts, arg, channel, sender):
	try:
		battle_id = int(arg)
	except ValueError:
		self.errormsg(channel, '%s is not a valid battle id.' % arg)
		return
	
	try:
		b = battle.get_battle(battle_id)
	except feed.FeedError as e:
		self.errormsg(channel, e.msg)
		return
		
	if not b.active:
		self.msg(channel, '@sep @bBattle [{b.id}]@b {b.region[name]} @sep {attacker}@b vs @b{b.defender[name]}@b @sep @bWinner@b {winner} @sep @bEnded@b {b.finished_at} @sep'.format(
					b = b,
					attacker = '@b' + b.attacker['name'] if not b.is_resistance else 'Resistance force of @b%s' % b.attacker['name'],
					winner = erepmap.get_country_name(b.winner_id)))
	else:
		domination = b.status['domination']
		if domination > 50:
			separator = '<'
		elif domination < 50:
			separator = '>'
		else:
			separator = '-'
		self.msg(channel, u"""@sep @bBattle [{b.id}]@b {b.region[name]} @sep {attacker}@b vs @b{b.defender[name]}@b @sep \
{status} @sep @bScore@b {b.status[attacker_points]} to \
{b.status[defender_points]} @sep @bTime@b {b.status[elapsed_time]} minutes @sep @bLink@b http://www.erepublik.com/en/military/battlefield/{b.id} @sep""".format(
					b = b,
					attacker = '@b' + b.attacker['name'] if not b.is_resistance else 'Resistance force of @b%s' % b.attacker['name'],
					status = '@bDomination@b {wallatt:.2f}% {separator} {domination:.2f}%'.format(
						wallatt = 100 - domination,
						separator = separator,
						domination = domination) if b.status['attacker_points'] < 1800 and b.status['defender_points'] < 1800 else
							'@b{winner}@b won this round @sep @bStatus@b pending'.format(winner = b.attacker['name'] if b.status['attacker_points'] >= 1800 else b.defender['name'])
					))

def command_battle_list(self, manager, opts, arg, channel, sender):
	try:
		battles = erepmap.get_battles(arg)
	except feed.FeedError, e:
		self.errormsg(channel, e.msg)
		return
	
	message = [u'[{b[battle_id]}] {b[attacker]} vs {b[defender]} in {b[region]}'.format(b=b) for b in sorted(battles, key=itemgetter('battle_id'))]
	
	if len(message) == 0:
		self.notice(sender, 'There are no active battles matching your search.')
	else:
		self.multinotice(sender, 6, '@sep @bActive battles@b%s @sep ' % (' [%s]' % arg if arg != '' else ''), ' @sep ', message, ' @sep ')

def command_mass(self, manager, opts, arg, channel, sender):
	if not self.channels[channel].mass:
		self.notice(sender, 'The .mass command is disabled for this channel. To enable it, type @b/msg eRepublik chanmass %s@b (founder only).' % channel)
		return
	
	chan = self.parent.get_channel(channel)
	sender_uid, senderinfo = self.parent.get_user(sender)
	members = chan[1]['members']
	sender_modes = members[sender_uid]
	if '@' not in sender_modes and '&' not in sender_modes and '~' not in sender_modes:
		self.notice(sender, 'This command is only available to channel ops, admins and founders.')
		return
	
	userlist = []
	for member in members:
		u = self.parent.get_user(member)[1]
		if not u or u['server'] in ['services.rizon.net', 'geo.rizon.net', 'py3.rizon.net']:
			continue
		
		user = u['nick']
		if not self.users.is_valid(user) or self.users[user].mass:
			userlist.append(user)
	
	if userlist:
		out = ''
		while len(userlist) > 0:
			if out:
				out += ' ' + userlist.pop()
			else:
				out += userlist.pop()
			if len(out) > 400:
				self.msg(channel, '@sep @bMass@b @sep %s @sep' % out)
				out = ''
		self.msg(channel, '@sep @bMass@b @sep %s @sep' % out)
		self.msg(channel, '@sep @bMass@b%s @sep %s @sep' % (' [@c4%s@c]' % arg if arg else '', 'To opt-out from being highlighted in .mass, type @b/msg eRepublik nousermass@b'))

def command_erepublik_info(self, manager, opts, arg, channel, sender):
	self.notice(sender, '@sep @bRizon eRepublik Bot@b @sep @bVersion@b %(major)d.%(minor)d @sep @bDevelopers@b ElChE and martin @sep @bHelp/feedback@b %(channel)s @sep' % {
		'major'   : self.VERSION,
		'minor'   : self.VERSION_MINOR,
		'channel' : '#fishbot'})

def command_erepublik_help(self, manager, opts, arg, channel, sender):
	command = arg.lower()

	if command == '':
		message = ['erepublik: .help erepublik - for erepublik commands']
	elif command == 'erepublik':
		message = manager.get_help()
	else:
		message = manager.get_help(command)

		if message == None:
			message = ['%s is not a valid command.' % arg]

	for line in message:
		self.notice(sender, line)

id_opt = ('id', '-i', 'look up by id instead of name', {'action': 'store_true'}, ARG_YES)
nick_opt = ('nick', '-n', 'look up by irc nick', {'action': 'store_true'}, ARG_YES)
heapy_opt = ('heapy', '-h', 'use a custom API, not live data (might be a few days old) [use when erep API is offline]', {'action': 'store_true'}, ARG_OFFLINE)

class UserCommandManager(CommandManager):
	def get_prefix(self):
		return '.'

	def get_commands(self):
		return {
			'regcit': 'register_citizen',
			'regnick': 'register_citizen',
			'register_citizen': (command_citizen_register, ARG_YES, 'links an eRepublik citizen to an IRC nickname', [
				id_opt
			], 'citizen_name'),

			'lp': 'lookup',
			'lookup': (command_citizen_info, ARG_OPT, 'looks up a citizen', [
				id_opt,
				nick_opt,
				heapy_opt
			]),
				
			'fc': 'fightcalc',
			'fightcalc': (command_citizen_fightcalc, ARG_OPT, 'calculates influence done with different weapon qualities', [
				id_opt,
				nick_opt,
				heapy_opt,
				('strength', '-s', 'uses a custom strength', {'type': '+decimal'}, ARG_OFFLINE|ARG_OFFLINE_REQ),
				('rank', '-r', 'uses a custom military rank (use rank ID, not name)', {'type': 'rank'}, ARG_OFFLINE|ARG_OFFLINE_REQ),
				('fights', '-f', 'specifies number of fights', {'type': '+integer'}, ARG_OFFLINE),
				('naturalenemy', '-e', 'adds 10% natural enemy bonus to influence', {'action': 'store_true'}, ARG_OFFLINE),
				('nextrank', '-N', 'calculates how many fights are required to rank up', {'action': 'store_true'}),
				('objective', '-o', 'calculates how many fights are required to make a given amount of influence', {'type': '+integer'}, ARG_OFFLINE),
				('influence', '-I', 'calculates how many fights are required to reach a given amount of rank points', {'type': '+integer'}, ARG_OPT)
			]),
			
			'ssc': 'sscalc',
			'sscalc': (command_citizen_sscalc, ARG_OPT, 'calculates days/gold required for the next SuperSoldier medal', [
				id_opt,
				nick_opt,
				heapy_opt,
				('strength', '-s', 'uses a custom strength', {'type': '+decimal'}, ARG_OFFLINE|ARG_OFFLINE_REQ),
				('friends', '-f', 'how many friends used in training (0, 1, 2)', {'type': '+integer'}, ARG_OFFLINE),
				('climbingcenter', '-1', 'adds climbing center bonus to training (+2.5 points)', {'action': 'store_true'}, ARG_OFFLINE),
				('shootingrange', '-2', 'adds shooting range bonus to training (+5 points)', {'action': 'store_true'}, ARG_OFFLINE),
				('specialforces', '-3', 'adds special forces bonus to training (+10 points)', {'action': 'store_true'}, ARG_OFFLINE),
				('naturalenemy', '-e', 'adds natural enemy bonus to training (+0.5 points)', {'action': 'store_true'}, ARG_OFFLINE),
				('septemberbonus', '-b', 'adds +30% bonus to daily training (September\'s bonus)', {'action': 'store_true'}, ARG_OFFLINE)
			]),

			'link': (command_citizen_link, ARG_OPT, "link to a citizen's profile", [
				id_opt,
				nick_opt,
				heapy_opt
			]),

			'donate': (command_citizen_donate, ARG_OPT, 'link to the donation page of a citizen', [
				id_opt,
				nick_opt,
				heapy_opt
			]),

			'message': (command_citizen_message, ARG_OPT, 'link to send a message to a citizen', [
				id_opt,
				nick_opt,
				heapy_opt
			]),
			
			'avatar': (command_citizen_avatar, ARG_OPT, 'link to a citizen\'s avatar', [
				id_opt,
				nick_opt,
				heapy_opt,
				('small', '-s', 'links to the small sized avatar (55x55px)', {'action': 'store_true'}, ARG_OPT),
				('medium', '-m', 'links to the medium sized avatar (100x100px)', {'action': 'store_true'}, ARG_OPT),
				('large', '-l', 'links to the large sized avatar (142x142px)', {'action': 'store_true'}, ARG_OPT)
			]),

			'medals': (command_citizen_medals, ARG_OPT, 'medals owned by a citizen', [
				id_opt,
				nick_opt,
				heapy_opt
			]),
			
#			'ranking': 'rankings',
#			'rankings': (command_citizen_rankings, ARG_OPT|ARG_OFFLINE, 'shows a citizen\'s ranking position', [
#				id_opt,
#				nick_opt
#			]),

			'market': 'bestprice',
			'bp' : 'bestprice',
			'bestprice': (command_market_info, ARG_YES|ARG_OFFLINE, 'shows the best offer in the selected market', [], 'industry country quality'),

			'region': (command_region_info, ARG_OPT, 'information about a region', [
				id_opt
			]),

			'borders': (command_region_borders, ARG_OPT, 'list of regions borders', [
				id_opt
			]),
			
			'dist': 'distance',
			'distance': (command_region_distance, ARG_YES, 'calculates zones-distance between two regions', [], 'from_region, to_region'),

#			'country': (command_country_info, ARG_YES|ARG_OFFLINE, 'information about a country', [
#				id_opt
#			], 'country_name'),
			
			'res': 'resources',
			'resource': 'resources',
			'resources': (command_country_resources, ARG_YES|ARG_OFFLINE, "summary of a country's resources", [
				id_opt
			], 'country_name'),
			
			'tax': (command_country_tax, ARG_OPT|ARG_OFFLINE, 'information about a country', [], 'country industry'),

			'mpp': 'mpps',
			'mpps': (command_country_mpp, ARG_OPT, 'shows a country\'s active mpp list and their expiration date', [
				id_opt
			]),
			
			'battle': (command_battle_info, ARG_YES, 'information about a battle', [], 'battle_id'),
			
			'battles': (command_battle_list, ARG_OPT, 'shows a list of active battles', []),
			
			'mass': (command_mass, ARG_OPT, 'highlights all the nicks in the channel (limited to channel ops, admins and founders)', [], 'optional_message'),

			'info': (command_erepublik_info, ARG_NO|ARG_OFFLINE, 'Displays version and author information', []),
			'help': (command_erepublik_help, ARG_OPT|ARG_OFFLINE, 'Displays available commands and their usage', []),
		}
