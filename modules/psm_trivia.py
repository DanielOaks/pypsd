#!/usr/bin/python pseudoserver.py
# psm_trvia.py
# 
# based on psm_quotes.py, which is based on psm_limitserv.py written by
# celestin - martin <martin@rizon.net>

import sys
import types
import datetime
import random
from untwisted.istring import istring
from psmodule import *
from trivia import cmd_admin, sys_auth, sys_channels, sys_log, sys_options, Trivia
from trivia.utils import *

class PSModule_trivia(PSModule):
	VERSION = 0.1
	uid = ""
	initialized = False

	# (table name, display name); display name is currently forced to be
	# compatible with original Trivia
	themes = (('anime', 'Anime'),
			  ('default', 'default'),
			  ('geography', 'Geography'),
			  ('history', 'History'), 
			  ('lotr-books', 'LOTR-Books'),
			  ('lotr-movies', 'LOTR-Movies'),
			  ('movies', 'Movies'),
			  ('naruto', 'Naruto'),
			  ('sciandnature', 'ScienceAndNature'),
			  ('simpsons', 'Simpsons'),
			  ('sg1qs', 'Stargate'))

	# channel name => Trivia instance
	trivias = {}
	
	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.auth.start()
		
	def bind_function(self, function):
		func = types.MethodType(function, self, PSModule_trivia)
		setattr(PSModule_trivia, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append((command, {'permission': 'j', 'callback': self.bind_function(list[command][0]), 
												'usage': list[command][1]}))

	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(config.get('trivia', 'nick'))
		except Exception, err:
			self.log.exception("Error reading 'trivia:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(config.get('trivia', 'channel'))
		except Exception, err:
			self.log.exception("Error reading 'trivia:channel' configuration option: %s" % err)
			raise
		
		self.bind_admin_commands()
					
	def startup(self):
		if not PSModule.startup(self):
			return False

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS trivia_chans (id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(200) NOT NULL, theme VARCHAR(64), UNIQUE KEY(name)) ENGINE=MyISAM;")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS trivia_scores (nick VARCHAR(30), points INT(10) DEFAULT '0', fastest_time FLOAT, highest_streak INT(10), channel INT(10)) ENGINE=MyISAM;");
		except Exception, err:
			self.log.exception("Error creating table for trivia module (%s)" % err)
			raise
		
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('trivia', 'nick'),
				self.config.get('trivia', 'user'),
				self.config.get('trivia', 'host'),
				self.config.get('trivia', 'gecos'),
				self.config.get('trivia', 'modes'),
				self.config.get('trivia', 'nspass'),
				join_chans = [self.chan],
				version="PyPsd Trivia v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating trivia module user: %s" % err)
			raise

		try:
			self.options = sys_options.OptionManager(self)
			self.auth = sys_auth.AuthManager(self)
			self.channels = sys_channels.ChannelManager(self)
			self.elog = sys_log.LogManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for trivia module (%s)' % err)
			raise
			
		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.elog.debug('Joined channels.')
		
		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for trivia module (%s)' % err)
			raise

		self.initialized = True
		self.elog.debug('Started threads.')
		return True
	
	def shutdown(self):
		PSModule.shutdown(self)

		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()

		for cname, trivia in self.trivias.iteritems():
			trivia.stop(True)

		self.trivias.clear()
		
		trivia_modules = [module for module in sys.modules if module.startswith('modules.trivia')]

		for module in trivia_modules:
			sys.modules.pop(module)

		self.parent.quitFakeUser(self.uid)

	def join(self, channel):
		self.parent.sendMessage("JOIN", str(self.parent.mkts()), channel, '+', prefix = self.uid)
		self.parent.join_channel(self.uid, channel)
		self.dbp.execute("INSERT IGNORE INTO trivia_chans(name) VALUES(%s)", (str(channel),))

	def part(self, channel):
		uid, user = self.parent.get_user(self.uid)

		if istring(channel) in user['channels']:
			self.stop_trivia(channel, True)
			self.parent.sendMessage("PART", channel, prefix=self.uid)
			self.parent.part_channel(self.uid, channel)
			self.dbp.execute("DELETE FROM trivia_chans WHERE name=%s", (str(channel),))

	def msg(self, target, message):
		if message != '':
			self.parent.privMsg(target, format_ascii_irc(message), self.uid)

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count
		
	def notice(self, target, message):
		if message != '':
			self.parent.notice(target, format_ascii_irc(message), self.uid)
			
## Begin event hooks
	def trivia_NOTICE(self, prefix, params):
		if not self.initialized:
			return
		
		uid, user = self.parent.get_user(params[0])
		foo, userinfo = self.parent.get_user(prefix)

		if uid != self.uid or (userinfo != None and userinfo['nick'] != 'ChanServ'):
			return

		msg = params[1].strip()

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return

		self.elog.chanserv('%s' % msg)
		sp = msg.split(' ')

		if userinfo == None:
			if 'tried to kick you from' in msg:
				nick = strip_ascii_irc(sp[1])
				channel = strip_ascii_irc(sp[7])
				self.notice(nick, 'To remove this bot (must be channel founder): @b/msg %s remove %s@b' % (user['nick'], channel))
			return

		if "isn't registered" in msg:
			self.auth.reject_not_registered(strip_ascii_irc(sp[1]))
			return

		if len(sp) < 6:
			return

		if 'inviting' in sp[2]: #It's an invite notice. Let's ignore it.
			return

		nick = strip_ascii_irc(sp[0])
		channel = sp[5][0:len(sp[5]) - 1]
		
		if 'Founder' in sp[2]:
			self.auth.accept(nick)
		else:
			self.auth.reject_not_founder(nick, channel)	
	
	def get_cid(self, cname):
		"""Fetches the channel id for a given channel name."""
		self.dbp.execute("SELECT id FROM trivia_chans WHERE name = %s", (cname,))
		cid = self.dbp.fetchone()[0]
		return cid

	def stop_trivia(self, cname, forced):
		'''Stops a trivia instance and removes it from our dict.'''
		if istring(cname) not in self.trivias:
			return

		self.trivias[istring(cname)].stop(forced)
		del self.trivias[istring(cname)]

	def trivia_PRIVMSG(self, prefix, params):
	# Parse ADD/DEL requests
		if not self.initialized:
			return

		bar, myself = self.parent.get_user(self.uid)
		channel = params[0]

		sender_id, userinfo = self.parent.get_user(prefix)
		sender = userinfo['nick']

		msg = params[1].strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			arg = ''
		else:
			command = msg[:index]
			arg = msg[index + 1:]
		
		command = command.lower()

		if self.channels.is_valid(channel): # a channel message
			if command.startswith("."): # a command
				# Log each and every command. This is very, very abusive and
				# thus should NOT be used in production if it can be helped.
				# But who cares about ethics? Well, I apparently don't.
				self.elog.debug("%s:%s > %s" % (sender, channel, msg[1:]))
				command = command[1:]
				if command == 'help' and arg == '':
					self.notice(sender, "Trivia: .help trivia - for trivia commands.")
				elif command == 'help' and arg.startswith('trivia'):
					self.notice(sender, "Trivia: .trivia [number] - to start playing.")
					self.notice(sender, "Trivia: .strivia - to stop current round.")
					self.notice(sender, "Trivia: .topten/.tt - lists top ten players.")
					self.notice(sender, "Trivia: .rank [nick] - shows yours or given nicks current rank.")
					# Defunct in orig trivia
					#self.notice(sender, "Trivia: .next - skips question.")
					self.notice(sender, "Trivia: .themes - lists available question themes.")
					self.notice(sender, "Trivia: .theme set <name> - changes current question theme (must be channel founder).")
				elif command == 'trivia':
					if istring(channel) in self.trivias:
						self.elog.debug("Trivia, but we're in %s" % channel)
						return

					rounds = 1024
					if arg.isdigit() and arg > 0:
						rounds = arg

					self.dbp.execute("SELECT theme FROM trivia_chans WHERE name = %s", (channel,))
					theme = self.dbp.fetchone()[0]
					if not theme:
						theme = "default"

					self.trivias[istring(channel)] = Trivia.Trivia(channel, self, theme, int(rounds))
				elif command == 'strivia':
					# stop_trivia does sanity checking
					self.stop_trivia(channel, True)
				elif command == 'topten' or command == 'tt':
					self.dbp.execute("SELECT nick, points FROM trivia_scores WHERE channel = %s ORDER BY points DESC LIMIT 10", (self.get_cid(channel),))
					# XXX: This is silent if no entries have been done; but
					# original trivia does so, too. Silly behavior?
					out = []
					for i, row in enumerate(self.dbp.fetchall()):
						# i+1 = 1 should equal out[0]
						out.append("%d. %s %d" % (i+1, row[0], row[1]))

					self.notice(sender, '; '.join(out))
				elif command == 'rank':
					self.dbp.execute("SELECT nick, points FROM trivia_scores WHERE channel = %s ORDER BY points DESC", (self.get_cid(channel),))
					rows = self.dbp.fetchall()
					out = ""
					# XXX: This is inefficient
					for i, row in enumerate(rows):
						if row[0].lower() == sender.lower():
							out = "You are currently ranked #%d with %d points" % (i+1, row[1])
							if i > 0:
								out += ", %d points behind %s" % (rows[i-1][1] - row[1], rows[i-1][0])
							out += "."

					if out != "":
						self.notice(sender, out)
				#elif command == 'next': # Defunct in orig trivia
					#pass
				elif command == 'themes':
					for theme in self.themes:
						# stupid original trivia design is stupid and breaks good
						# db coding practice >.>
						self.dbp.execute("SELECT COUNT(*) FROM `trivia_questions_%s`" % theme[0])
						self.notice(sender, "Theme: %s, %d questions" %
								(theme[1], self.dbp.fetchone()[0]))
				elif command == 'theme':
					args = arg.split(' ')
					settheme = ""
					if len(args) < 2 or args[0].lower() != 'set':
						return

					self.notice(sender, "Checking if you are the channel founder.")
					self.auth.request(sender, channel, 'set_theme_' + args[1])
			else: # not a command, but might be an answer!
				if istring(channel) not in self.trivias:
					return # no trivia running, disregard that

				self.trivias[istring(channel)].check_answer(sender, msg)

		elif channel == myself['nick']:
			if command == 'request':
				if not arg:
					self.msg(sender, '@bUsage@b: request @b#channel@b')
					return
			
				if self.channels.is_valid(arg):
					self.msg(sender, "I'm already in @b%s@b." % arg)
				elif self.channels.is_banned(arg):
					chan = self.channels[arg]
					self.elog.request('Request for banned channel @b%s@b from @b%s@b.' % (arg, sender))
					message = 'Request failed, channel @b%s@b was banned by @b%s@b.' % (arg, chan.ban_source)
		
					if chan.ban_reason != None:
						message += ' Reason: @b%s@b.' % chan.ban_reason
		
					if chan.ban_expiry != None:
						message += ' Expires: @b%s@b.' % datetime.datetime.fromtimestamp(chan.ban_expiry)
		
					self.msg(sender, message)
				else:
					self.auth.request(sender, arg, 'request')
			elif command == 'remove':
				if not arg:
					self.msg(sender, '@bUsage@b: remove @b#channel@b')
					return
			
				if not self.channels.is_valid(arg):
					self.msg(sender, "I'm not in @b%s@b." % arg)
				else:
					self.auth.request(sender, arg, 'remove')
			elif command == 'help' or command == 'hi' or command == 'hello':
				self.msg(sender, '@brequest@b requests a channel (must be founder)')
				self.msg(sender, '@bremove@b removes a channel (must be founder)')
				self.msg(sender, '@bhelp@b displays help text')
			else:
				self.msg(sender, 'Invalid message. Say help for a list of valid messages.')
	
	def trivia_TMODE(self, prefix, params):
		if not self.initialized:
			return

		channel = params[1]
		modes = params[2]

		if modes == '-z' and channel in self.channels:
			self.channels.remove(channel)
			self.dbp.execute("DELETE FROM trivia_scores WHERE channel = %s", (self.get_cid(channel),))
			self.dbp.execute("DELETE FROM trivia_chans WHERE name = %s", (channel,))
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)
	
	def trivia_KICK(self, prefix, params):
		if not self.initialized:
			return
		
		if params[1] == self.nick:
			self.parent.sendMessage("JOIN", str(self.parent.mkts()), channel, '+', prefix = self.uid)
	
	def getCommands(self):
		return self.commands_admin
	
	def getHooks(self):
		return (('privmsg', self.trivia_PRIVMSG),
					('notice', self.trivia_NOTICE),
					('tmode', self.trivia_TMODE),
					('kick', self.trivia_KICK)
				)
